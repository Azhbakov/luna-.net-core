import * as React from 'react';
import './App.css';

import logo from './logo.svg';
import { Home } from './components/Home';
import { Layout } from './components/Layout';
import { Route } from 'react-router';
import Counter from './components/Counter';
import Tasks from './components/Tasks';
import Lobbies from './components/Lobbies';
import Lobby from 'src/components/Lobby';
import { FragmentExecutor } from './components/FragmentExecutor';
import { ViewDebug } from './components/ViewDebug';
import TaskView from './components/TaskView';
import { ExecDebug } from './components/ExecDebug';

class App extends React.Component {
	public render() {
    	return this.renderLayout();
	}

	public renderLayout() {
		return (
			<Layout>
				<Route exact={true} path='/' component={Home} />
				<Route path='/Counter' component={Counter} />
				<Route path='/fragmentViewer' component={FragmentExecutor} />
				<Route path='/Lobby' component={Lobby} />
				<Route path='/Lobbies' component={Lobbies} />
				<Route path='/Tasks' component={Tasks} />
				<Route path='/Task/:taskId' component={TaskView} />
				<Route path='/ViewDebug' component={ViewDebug} />
				<Route path='/ExecDebug' component={ExecDebug} />
			</Layout>
		);
	}

	public renderLoader() {
		return (
			<div className="App">
				<header className="App-header">
					<img src={logo} className="App-logo" alt="logo" />
					<h1 className="App-title">Welcome to React</h1>
				</header>
				<p className="App-intro">
					To get started, edit <code>src/App.tsx</code> and save to reload.
				</p>
			</div>
		);
	}
}

export default App;
