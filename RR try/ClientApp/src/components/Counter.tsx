import * as React from "react";
import { actionCreators } from './../store/Counter/index';
import { connect } from 'react-redux';
import { AppState } from "../store/configureStore";

interface CounterProps {
    count: number;
}

interface CounterConnectedProps {
    dispatch: any;
}


class Counter extends React.Component<CounterProps & CounterConnectedProps, any> {
    
    public render() {
        return (
            <div>
                <h1>Counter</h1>

                <p>This is a simple example of a React component.</p>

                <p>Current count: <strong>{this.props.count}</strong></p>

                <button onClick={this.onIncrementClick}>Increment</button>
                <button onClick={this.onDecrementClick}>Decrement</button>
            </div>
        );
    }

    private onIncrementClick = () => {
        this.props.dispatch(actionCreators.increment())
    }

    private onDecrementClick = () => {
        this.props.dispatch(actionCreators.decrement())
    }
}

export default connect(
    (state: AppState): CounterProps => ({
        count: state.counter.counter
    })
)(Counter);