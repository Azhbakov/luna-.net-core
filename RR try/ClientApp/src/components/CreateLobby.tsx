import { Task } from "src/store/Tasks";
import * as React from "react";
import { lobbyCreateUrl, LobbyCreateModel } from "../store/Lobbies";
import { FormGroup, ControlLabel, FormControl } from "react-bootstrap";

interface CreateLobbyProps {
    tasks: Task[]
    onSuccess: () => void;
}

interface CreateLobbyLocalState {
    taskId: string | null;
    lobbyName: string | null;
    maxUsersCount: number | null;
    error: Error | null;
    isSending: boolean;
}

export default class CreateLobby extends React.Component<CreateLobbyProps, CreateLobbyLocalState> {
    constructor(props: CreateLobbyProps) {
        super(props);
        this.state = {
            taskId: null,
            lobbyName: null,
            maxUsersCount: null,
            error: null,
            isSending: false
        };
    }
    
    public render () {
        const state = this.state;

        const bindTaskId = (e: any): void => {
            this.setState({...state, taskId: e.target.value})
        }
        const bindLobbyName = (e: React.ChangeEvent<HTMLInputElement>): void => {
            this.setState({...state, lobbyName: e.target.value})
        }
        const bindMaxUserCount = (e: React.ChangeEvent<HTMLInputElement>): void => {
            this.setState({...state, maxUsersCount: +e.target.value})
        }

        const submit = (e: React.FormEvent) => {
            if (!state.lobbyName || !state.taskId) throw new Error('All values must be provided before submit.')
            fetch(lobbyCreateUrl, {
                method: "POST", 
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(state as LobbyCreateModel)
            }).then(response => {
                if (response.ok) this.props.onSuccess();
            }).catch(reason => {
                this.setState({...state, error: reason});
            });

            e.preventDefault();
        }

        const createTaskOptions = () => {
            return (
                this.props.tasks.map(t => <option key={t.id} value={t.id}>{t.name}</option>)
            );
        }

        return (
            <div>
                <form onSubmit={submit} >
                    <div className="form-group">
                        <label htmlFor="lobbyName">{"Lobby name"}</label>
                        <input className="form-control" id="lobbyName" placeholder="Enter lobby name" required onChange={bindLobbyName}/>
                    </div>
                    <FormGroup>
                        <ControlLabel htmlFor="taskId">Task</ControlLabel>
                        <FormControl componentClass="select" id="taskId" onChange={ bindTaskId }>
                            <option disabled selected>Select task...</option>
                            { createTaskOptions() }
                        </FormControl>
                    </FormGroup>
                    <div className="form-group">
                        <label htmlFor="maxUserCount">{"Max users"}</label>
                        <input type="number" className="form-control" id="maxUserCount" onChange={ bindMaxUserCount } />
                    </div>
                    {state.error && <label>{state.error.message}</label>}
                    <button type="submit" className="btn btn-primary" disabled={state.isSending}>{state.isSending? "Creating..." : "Create"}</button>
                </form> 
            </div>
        );
    };
};