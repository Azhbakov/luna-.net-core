import { Grid } from "react-bootstrap";
import * as React from "react";
import { IExecutionResult, Atoms } from "../domain/engine/lang_processing/cf";
import { lunaLang } from "../domain/engine/lang_processing/lunaLang";
import { ExecutionView } from "./ExecutionView";
import { while1, while1Atoms } from "../domain/engine/tests/tasks/while1";
import TimeLogger from "./TimeLogger";
//import threadWorker from '../domain/engine/runtime/webworker/threadWorker';
import logo from '../../src/logo.svg';
import { ThreadWorkerProxy } from "src/domain/engine/runtime/webworker/threadWorkerProxy";
import { WebworkerChannel } from "src/domain/engine/runtime/webworker/webworkerChannel";
import { ProgramView } from "./LobbyComponents/DomainView/ProgramView";
import { IFragmentStorage } from "src/domain/engine/runtime/distributedStorage";
import { ThreadStorageProxy } from "src/domain/engine/runtime/webworker/threadStorageProxy";
import { IWorker } from "src/domain/engine/runtime/worker";
import { poi1d, poi1dAtoms } from "src/domain/engine/tests/tasks/poi1d";
import { InitPathfinder, InitWebworker } from "src/domain/engine/runtime/webworker/messageTypes";
import { IPathfinderContext, SimplePathfinderCreator } from "src/domain/engine/runtime/simplePathfinder";
import { ITopology, IPeer } from "src/domain/Lobby";
import { Poi1dPathfinderCreator } from "src/domain/engine/runtime/poi1dPathfinder";
//import MyWorker from "src/domain/engine/runtime/webworker";

interface State {
	taskData: { atoms: Atoms, program: lunaLang.Program },
	res: IExecutionResult | null, 
	storage: ThreadStorageProxy,
	worker: IWorker
}

export class ExecDebug extends React.Component<{}, State> {
	public constructor(props: any) {
		super(props);
		
		const pathfinderContext = { 
				leaderId: "1", 
				selfId: "1", 
				topology: { 
					edges: [], 
					key: 'LineTopology', // for ballStop 
					peers: [{ id: '1', isReady: true, name: "test" } as IPeer]
				} as ITopology 
			} as IPathfinderContext;
		const webworker = new WebworkerChannel();

		function serializeAtoms(atoms: any): any {
			const serializedAtoms = {};
			for (const property of Object.keys(atoms)) {
				serializedAtoms[property] = typeof atoms[property] === 'function' ?
					atoms[property].toString()
					: atoms[property];
			}
			return serializedAtoms;
		}
		
		const { storage, initMessage: storageMessage } = ThreadStorageProxy.init(webworker, "1");
		const { worker, initMessage: workerMessage } = ThreadWorkerProxy.init(webworker, poi1d, poi1dAtoms, true);

		webworker.postMessage({
			type: 'initWebworker',
			storage: storageMessage,
			worker: workerMessage,
			pathfinder: {
				pathfinderCreator: serializeAtoms({creator: Poi1dPathfinderCreator}).creator,
				pathfinderContext
			} as InitPathfinder
		} as InitWebworker);

		this.state = {
			taskData: { atoms: while1Atoms, program: while1 },
			res: null,
			storage,
			worker
		};
	}  
	
	public render() {
		const webWorker = this.state.worker;

		const step = () => {
			webWorker.step();
		}
		const loadMainThread = () => {
			const users = [];

			const userDetails = {
				name: 'Jane Doe',
				email: 'jane.doe@gmail.com',
				dateJoined: new Date(),
				id: 1
			};

			for (let i = 0; i < 10000000; i++) {

				userDetails.id = i;
				userDetails.dateJoined = new Date()
				users.push(userDetails);
			}
			console.log(users.length)
		}

		return (
			<Grid fluid>
				<h1>WebWorker Debug</h1>
				<img src={logo} className="App-logo" alt="logo" />
				<button onClick={step}>WebWorker step</button>
				<button onClick={loadMainThread}>Struggle self</button>
				{this.state.taskData && <ProgramView value={this.state.taskData.program}/>}
				<ExecutionView program={this.state.taskData.program} atoms={this.state.taskData.atoms}
					storage={this.state.storage} worker={this.state.worker}/>
				<TimeLogger/>
			</Grid>
		)
	}
}