import * as React from "react";
import { lunaLang } from "../domain/engine/lang_processing/lunaLang";
import { ProgramView as ProgramView } from "./LobbyComponents/DomainView/ProgramView";
import { Grid, Col, Row, Button } from "react-bootstrap";
import { StorageView } from "./StorageView";
import { IWorker } from "../domain/engine/runtime/worker";
import { Atoms } from "../domain/engine/lang_processing/cf";
import { IObservableStorage } from "src/domain/engine/runtime/distributedStorage";
import { IObservableBufferedStorage } from "src/domain/engine/runtime/webworker/bufferedStorageView";

export type Props = {
	program: lunaLang.Program;
	atoms: Atoms;
	storage: IObservableStorage | IObservableBufferedStorage;
	worker: IWorker;
}

export class ExecutionView extends React.Component<Props> {
	constructor(props: Props) {
		super(props);
	}

	public render(): JSX.Element {

		const doStep = () => this.props.worker.step();
		const doWork = () => this.props.worker.work();

		return (
			<Grid fluid>
				<Row>
					<h3>Worker</h3>
					<Button onClick={doStep}>Execute CF</Button>
					<Button onClick={doWork}>Execute program</Button>
				</Row>
				<Row>
					<Col xs={12} lg={6} lgPush={6}>
						<StorageView storage={this.props.storage}/>
					</Col>
					<Col xs={12} lg={6} lgPull={6}>
						<h3>Source</h3>
						<ProgramView value={this.props.program}/>
					</Col>
				</Row>
			</Grid>
		)
	}
}