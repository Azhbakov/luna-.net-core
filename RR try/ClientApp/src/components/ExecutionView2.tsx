import * as React from "react";
import { ProgramView as ProgramView } from "./LobbyComponents/DomainView/ProgramView";
import { Grid, Col, Row, Button } from "react-bootstrap";
import { ExecutionState, executionActions } from "src/store/Execution";
import { AppState } from "src/store/configureStore";
import { DispatchConnectedProps, mapDispatchToProps } from "src/store";
import { connect } from "react-redux";
import { StorageView2 } from "./StorageView2";


class ExecutionView2 extends React.Component<ExecutionState & DispatchConnectedProps> {
	constructor(props: any) {
		super(props);
	}

	public render(): JSX.Element {

		const doStep = () => this.props.dispatch(executionActions.step());

		return (
			<Grid fluid>
				<Row>
					<h3>Worker</h3>
					<Button onClick={doStep}>Execute CF</Button>
				</Row>
				<Row>
					<Col xs={12} lg={6} lgPush={6}>
						<StorageView2 cfs={this.props.cfs} dfs={this.props.dfs}/>
					</Col>
					<Col xs={12} lg={6} lgPull={6}>
						<h3>Source</h3>
						{this.props.program && <ProgramView value={this.props.program}/>}
					</Col>
				</Row>
			</Grid>
		)
	}
}

const mapStateToProps = (state: AppState) => state.execution;

export default connect<ExecutionState, DispatchConnectedProps>(mapStateToProps, mapDispatchToProps)(ExecutionView2);