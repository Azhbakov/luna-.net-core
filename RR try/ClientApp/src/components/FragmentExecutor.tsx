import * as React from "react";
import { Grid, Row, FormControl, ControlLabel, Button, Col, FormGroup, Panel } from "react-bootstrap";
import { lunaLang } from '../domain/engine/lang_processing/lunaLang';
import { interpretCfDecl } from "../domain/engine/lang_processing/factories";
import { IDf, IContextVariable, ICf, IExecutionContext } from "../domain/engine/lang_processing/cf";
import { IExecCf } from "../domain/engine/lang_processing/cf_impl/execCf";
import { ExecCf } from "./LobbyComponents/DomainView/ExecCf";

interface IFragmentExecutorState {
	prototypesInput: string;
	dfsInput: string;
	scopeVarsInput: string;
	cfInput: string;
	program: lunaLang.Program | null;
	dfs: IDf[] | null;
	scopeVars: IContextVariable[] | null;
	cf: ICf | null;
	error: string | null
}

export class FragmentExecutor extends React.Component<{}, IFragmentExecutorState> {
	public constructor(props: any) {
		super(props);

		this.state = {
			prototypesInput: "", dfsInput: "", scopeVarsInput: "", cfInput: "",
			program: null,
			dfs: null,
			scopeVars: null,
			cf: null,
			error: null
		}
	}
	
	private parseInput(prototypesInput: string, dfsInput: string, scopeVarsInput: string, cfInput: string) {
		// Parse prototypes
		const prototypesJson = JSON.parse(prototypesInput);
		const program: lunaLang.Program = {};
		Object.keys(prototypesJson).forEach((p: any) => program[p] = lunaLang.parseDeclarationStatement(prototypesJson[p]));
		
		// Parse dfs
		const dfsJson = JSON.parse("{ \"dfs\": [" + dfsInput + "]}");
		const dfs: IDf[] = dfsJson.dfs.map((df: any) => df as IDf);

		// Parse scoped vars
		const scopeVarsJson = JSON.parse("{ \"vars\": [" + scopeVarsInput + "]}");
		const scopeVars: IContextVariable[] = scopeVarsJson.vars.map((scopeVar: any) => scopeVar as IContextVariable);

		// Parse operator
		const cfJson = JSON.parse(cfInput);
		if (!lunaLang.IsOperator(JSON.parse(cfInput))) {
			this.setState({...this.state, cf: null, error: "Failed to parse fragment."});
		}
		const operator = cfJson as lunaLang.IExec;

		const initialContext: IExecutionContext = { variables: scopeVars, parentCfId: { main: "", indicies: [] },/*, tryGetDf: (id: string) => dfs[id]? dfs[id] : 'WAIT'*/ };

		const cf = interpretCfDecl(operator, initialContext, { source: program, atoms: [] });
		this.setState({
			...this.state, program, dfs, scopeVars, cf, error: null
		});
	}

	public render(): JSX.Element {
		const bindPrototypesInput = (event: any) => this.setState({...this.state, prototypesInput: event.target.value});
		const bindDfsInput = (event: any) => this.setState({...this.state, dfsInput: event.target.value});
		const bindScopeVarsInput = (event: any) => this.setState({...this.state, scopeVarsInput: event.target.value});
		const bindCfInput = (event: any) => this.setState({...this.state, cfInput: event.target.value});

		const submit = (e: React.FormEvent) => {
			this.parseInput(this.state.prototypesInput, this.state.dfsInput, 
				this.state.scopeVarsInput, this.state.cfInput);
            e.preventDefault();
		}

		const inputArea = (label: string, placeholder: string, onChange: (event: any) => void) => 
			<FormGroup>
				<ControlLabel>{label}</ControlLabel>
				<FormControl componentClass="textarea" placeholder={placeholder} 
					onChange={onChange}/>
			</FormGroup>
		
		const cfInput = (
			<form onSubmit={submit}>
				{inputArea("Prototypes", "Enter subroutines/atom declarations...", bindPrototypesInput)}
				{inputArea("DF Storage", "Enter pre-loaded data fragment declarations...", bindDfsInput)}
				{inputArea("Scope variables", "Enter arguments which are visible in the current scope...", bindScopeVarsInput)}
				{inputArea("Fragment", "Enter CF declaration...", bindCfInput)}
				<Button type="submit">Execute</Button>
			</form>
		);

		return (
			<div>
				<h1>Fragment Executor</h1>
				<Grid fluid>
					<Row><Col>{cfInput}</Col></Row>
				</Grid>
				{this.state.cf && <ExecCf value={this.state.cf as IExecCf}/>}
			</div>
		);
	}
}