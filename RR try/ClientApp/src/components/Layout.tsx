import * as React from "react";
import { NavMenu } from "./NavMenu";
import * as Grid  from "react-bootstrap/lib/Grid";
import * as Row  from "react-bootstrap/lib/Row";
import * as Col  from "react-bootstrap/lib/Col";

export const Layout: React.StatelessComponent = props => {
    return (
        <Grid fluid={true}>
            <Row>
                <Col sm={3}>
                    <NavMenu />
                </Col>
                <Col sm={9}>
                    { props.children }
                </Col>
            </Row>
        </Grid>
    );
}