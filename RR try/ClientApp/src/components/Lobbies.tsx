import { LobbiesState, actionCreators as lobbiesActionCreators} from "../store/Lobbies";
import { DispatchConnectedProps, mapDispatchToProps } from "../store";
import * as React from "react";
import { AppState } from "src/store/configureStore";
import { connect } from "react-redux";
import { BootstrapTable, TableHeaderColumn, Options, SelectRow } from "react-bootstrap-table";
import { TasksState, actionCreators as tasksActionCreators} from "../store/Tasks";
import { Glyphicon, Button, Modal } from "react-bootstrap";
import CreateLobby from "./CreateLobby";
import { actionCreators } from "../store/Lobby";
import { Link } from "react-router-dom";

interface LobbiesComponentState {
    selectedLobbyId: string | null;
    createLobbyShown: boolean;
}

class LobbiesBase extends React.Component<LobbiesState & TasksState & DispatchConnectedProps, LobbiesComponentState> {
    public constructor(props: any) {
        super(props);
        this.state = {
            selectedLobbyId: null,
            createLobbyShown: false
        }
    }

    public componentDidMount() {
        this.props.dispatch(tasksActionCreators.taskFetchStart());
        this.props.dispatch(lobbiesActionCreators.lobbiesFetchStart());
    }

    public render(): JSX.Element {
        const lobbiesData = this.props.lobbies.map(lobby => {
            const task = this.props.tasks.find(t => t.id === lobby.taskId);
            const taskName = task? task.name : "*MISSING*";
            return ({id: lobby.id, lobbyName: lobby.lobbyName, taskName, users: `${lobby.userCount}/${lobby.maxUsersCount}` });
        })

        const showCreateLobby = () => { this.setState({...this.state, createLobbyShown: true }); }
        const hideCreateLobby = () => { this.setState({...this.state, createLobbyShown: false }); }
        
        const refreshList = () => {
            this.props.dispatch(tasksActionCreators.taskFetchStart());
            this.props.dispatch(lobbiesActionCreators.lobbiesFetchStart());
        }

        const onDelete = (row: any, rowIndex: number) => {
            this.props.dispatch(lobbiesActionCreators.lobbyRemoveStart(row.id));
        }
        const createDeleteButton = (cell: any, row: any, formatExtraData: any, rowIndex: number) => {
            const deleteAction = () => onDelete(row, rowIndex);
            return (
                <Button className="btn-xs btn-icon" onClick={deleteAction}>
                    <Glyphicon glyph="glyph glyphicon-trash" />
                </Button>
            );
        }

        const onLobbyCreated = () => {
            hideCreateLobby();
            this.props.dispatch(lobbiesActionCreators.lobbiesFetchStart());
        }

        const CreateTaskModal = (
            <Modal show={this.state.createLobbyShown} onHide={hideCreateLobby}>
                <Modal.Header closeButton>
                    <Modal.Title>Upload task</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <CreateLobby tasks={this.props.tasks} onSuccess={onLobbyCreated}/>
                </Modal.Body>
            </Modal>
        );

        const rowSelectionOptions: SelectRow<any> = {
            mode: "radio",
            bgColor: "yellow",
            hideSelectColumn: true,
            clickToSelect: true,
            clickToExpand: true,
		}
		
		const createJoinHandler = (lobbyId: string) => {
			return () => { this.props.dispatch(actionCreators.lobbyJoin(lobbyId)); }
		}
	
        const tableOptions: Options = {
            noDataText: this.props.isLoading ? 'Loading...' : 'Press "Create" to create new lobby',
        }
        const isExpandable = (row: any) => true;
        const onRowExpand = (row: any): JSX.Element => {
            return (
				<Link className="btn btn-default" to={'/lobby'} onClick={ createJoinHandler(row.id) }>
					Join
				</Link>
			)
        }
        const lobbiesTable = (
            <div>
                <Button className="btn btn-primary" onClick={showCreateLobby}>Create lobby</Button>
                <Button className="btn-icon" onClick={refreshList}>
                    <Glyphicon glyph="glyph glyphicon-refresh" />
                </Button>
                <BootstrapTable 
                    data={lobbiesData} 
                    options={tableOptions} 
                    selectRow={rowSelectionOptions}
                    expandableRow={isExpandable}
                    expandComponent={onRowExpand}>
                <TableHeaderColumn dataField="id" dataAlign="center" isKey>ID</TableHeaderColumn>
                <TableHeaderColumn dataField="lobbyName" dataAlign="center">Title</TableHeaderColumn>
                <TableHeaderColumn dataField="taskName" dataAlign="center">Task</TableHeaderColumn>
                <TableHeaderColumn dataField="users" dataAlign="center">Users</TableHeaderColumn>
                <TableHeaderColumn dataField="button" dataAlign="center" dataFormat={createDeleteButton}>Remove</TableHeaderColumn>
                </BootstrapTable>
            </div>
        );
        const detailsView = (
            <div>
                <span>Selected Details</span>
            </div>
        );

        return (
            <div>
                <h1>Lobbies</h1>
                {CreateTaskModal}
                {lobbiesTable}
            </div>
        );
    }
}

const mapStateToProps = (state: AppState) => ({ ...state.lobbies, ...state.tasks });

export default connect<LobbiesState & TasksState, DispatchConnectedProps, LobbiesComponentState>(mapStateToProps, mapDispatchToProps)(LobbiesBase);