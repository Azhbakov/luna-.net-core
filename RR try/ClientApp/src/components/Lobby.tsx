import { DispatchConnectedProps, mapDispatchToProps } from "../store";
import * as React from "react";
import { Row, Col, Grid, Button } from "react-bootstrap/lib";
import { connect } from "react-redux";
import { LobbyState, actionCreators } from "src/store/Lobby";
import { AppState } from "../store/configureStore";
import { BootstrapTable, TableHeaderColumn, SelectRow, Options } from "react-bootstrap-table";
import { ServerConnectionState } from "../store/ServerConnection";
import { WebRtcState } from "../store/PeerConnection";
import { ServerConnection } from "./LobbyComponents/ServerConnection";
import { PeerConnectionsGallery, PeerConnectionData } from "./LobbyComponents/PeerConnectionGallery";
import { ILobby } from "src/domain/Lobby";
import { LobbyDetails } from "./LobbyComponents/LobbyDetails";
import { ProgramView } from "./LobbyComponents/DomainView/ProgramView";
import { lunaLang } from "src/domain/engine/lang_processing/lunaLang";
import { Atoms } from "src/domain/engine/lang_processing/cf";
import ExecutionView2 from "./ExecutionView2";
import { isLeader, getLobby } from "src/store/Lobby/selectors";
import TimeLogger from "./TimeLogger";
import { ExecutionSettings } from "src/domain/ExecutionSettings";

interface LobbyViewState {
	lobbyState: LobbyState,
	taskData: { atoms: Atoms, program: lunaLang.Program } | null
	serverConnection: ServerConnectionState,
	webRtc: WebRtcState
}

class LobbyBase extends React.Component<LobbyViewState & DispatchConnectedProps> {
	public render(): JSX.Element {		
		// PEERS
		const lobby = this.props.lobbyState.lobby;
		const onPeerExpand = (row: any): JSX.Element => {
			if (!lobby|| lobby.topology.peers.length === 0) return (<span>No neighbors.</span>);
			const expandedPeerRows = lobby.topology.edges.filter(e => e.fromId === row.id || e.toId === row.id).map(e => {
				const { targetPeerId, targetPosKey } = e.fromId === row.id ? { targetPeerId: e.toId, targetPosKey: e.fromKey } : { targetPeerId: e.fromId, targetPosKey: e.toKey };
				return (
					<Row key={targetPosKey}>
						<Col xs={1}><strong>{targetPosKey}</strong></Col><Col xs={1}><span>{targetPeerId}</span></Col>
					</Row>
				);
			});
			return <Grid fluid children={expandedPeerRows}/>
		}
		const isExpandable = (row: any) => true;
		const rowSelectionOptions: SelectRow<any> = {
            mode: "radio",
            hideSelectColumn: true,
            clickToSelect: true,
            clickToExpand: true,
		}
		const tableOptions: Options = {
			
		}
		const rowStyle = (row: any) => {
			return row.isReady? 'peerReady' : 'peerNotReady';
		}
		const peers = (
			<div>
				<BootstrapTable 
					hover
					data={lobby? lobby.topology.peers.map(p => ({ id: p.id, name: p.name, isReady: p.isReady })) : []}
					trClassName={rowStyle} 
					options={tableOptions}
					selectRow={rowSelectionOptions}
					expandableRow={isExpandable}
					expandComponent={onPeerExpand}
					>
					<TableHeaderColumn isKey={true} dataAlign="center" dataField='id'>ID</TableHeaderColumn>
					<TableHeaderColumn dataAlign="center" dataField='name'>Name</TableHeaderColumn>
				</BootstrapTable>
			</div>
		);

		const leaveLobby = () => {
			this.props.dispatch(actionCreators.lobbyLeave());
		}

		return (
			<div>
				<h1>Lobby</h1>
				{this.props.lobbyState.lobbyError && <div className="lobbyError">{this.props.lobbyState.lobbyError.message}</div>}
				{!this.props.lobbyState.lobby && <h3>Please, join a lobby in "Lobbies" menu.</h3>}
				{this.props.lobbyState.lobby && 
					<div>
						<Grid fluid>
							<Row>
								<Col md={6}><ServerConnection serverConnection={this.props.serverConnection}/></Col>
								<Col md={6}>
									<LobbyDetails lobby={this.props.lobbyState.lobby}/>
									<div><Button className='btn btn-danger' onClick={leaveLobby}>Leave lobby</Button></div>
								</Col>
							</Row>
						</Grid>
						<PeerConnectionsGallery peerConnections={this.getNeighborConnectionData(this.props.lobbyState.lobby)}/>
						{peers}
						<div>{isLeader(this.props.lobbyState) && this.getStartButtons()}</div>
						{this.props.taskData && <ProgramView value={this.props.taskData.program}/>}
						<ExecutionView2/>
						<TimeLogger/>
					</div>
				}
			</div>
		);
	}

	private getNeighborConnectionData(lobby: ILobby) {
		const peerConnectionStates: PeerConnectionData[] = [];
		const selfId = this.props.lobbyState.selfId;
		const relatedEdges = lobby.topology.edges.filter(e => e.fromId === selfId || e.toId === this.props.lobbyState.selfId);

		relatedEdges.forEach(e => {
			const { targetPeerId, targetPosKey } = e.fromId === selfId ? { targetPeerId: e.toId, targetPosKey: e.fromKey } : { targetPeerId: e.fromId, targetPosKey: e.toKey };
			const peerWebRtcState = this.props.webRtc.peerConnectionStates.find(p => p.peerId === targetPeerId);
			if (!peerWebRtcState) return; // possible when offer haven't come from peer yet
			peerConnectionStates.push({ peerPosition: targetPosKey, state: peerWebRtcState });
		}, this);

		return peerConnectionStates;
	}

	private getStartButtons(): JSX.Element {
		const lobby = getLobby(this.props.lobbyState);
		const onClickStep = () => {
			this.props.dispatch(actionCreators.lobbyStart({updateUI: true, mode: 'STEP'} as ExecutionSettings));
		}
		const onClickContinuous = () => {
			this.props.dispatch(actionCreators.lobbyStart({updateUI: true, mode: 'RUN'} as ExecutionSettings));
		}

		return <span>
			<Button active={lobby.status === 'READY'} onClick={onClickStep}>Start</Button>;
			<Button active={lobby.status === 'READY'} onClick={onClickContinuous}>Start</Button>;
		</span>
	}
}

const mapStateToProps = (state: AppState): LobbyViewState => {
	const task = state.lobby.lobby ? 
		state.tasks.tasks.find(t => t.id === (state.lobby.lobby as ILobby).task.id) : null;
	const taskData = task && task.atoms && task.program? { atoms: task.atoms, program: task.program } : null;
	return ({
		lobbyState: state.lobby,
		taskData,
		serverConnection: state.serverConnection,
		webRtc: state.webRtc
	});
}

export default connect<LobbyViewState, DispatchConnectedProps>(mapStateToProps, mapDispatchToProps)(LobbyBase);