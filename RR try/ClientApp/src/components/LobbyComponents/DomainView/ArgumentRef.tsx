import { IArgument, IConstArgument, IDfArgument, isIDfArgument, IExpressionArgument, isIExpressionArgument, isICastArgument, ICastArgument } from "../../../domain/engine/lang_processing/arguments";
import * as React from "react";
import { lunaLang } from "../../../domain/engine/lang_processing/lunaLang";
import { OverlayTrigger, Tooltip, Popover } from "react-bootstrap";
import { DfArgument } from "./DfArgument";
import { ExpressionArgument } from "./ExpressionArgument";
import { CastArgument } from "./CastArgument";

export interface Props {
	value: IArgument
}

export class ArgumentRef extends React.Component<Props> {
	public render(): JSX.Element {
		const value = this.props.value;
		
		const isConst = lunaLang.IsIConstArgument(value);
		const constArg = value as IConstArgument;

		const isDf = isIDfArgument(value);
		const dfArg = value as IDfArgument;

		const isExpr = isIExpressionArgument(value);
		const exprArg = value as IExpressionArgument;

		const isCast = isICastArgument(value);
		const castArg = value as ICastArgument;

		if (![isConst, isDf, isExpr, isCast].some(b => b)) return <span>NOT IMPLEMENTED ARG REF</span>

		return (
			<span>
				{isConst && <ConstArgRef value={constArg}/>}
				{isDf && <DfArgRef value={dfArg}/>}
				{isExpr && <ExprArgRef value={exprArg}/>}
				{isCast && <CastArgRef value={castArg}/>}
			</span>
		) // TODO: tooltip with DfArgument component

	}
}

const ConstArgRef: React.StatelessComponent<{value: IConstArgument}> = props => {
	return (
		<span className="argRef constArgumentRef">{props.value.value.toString()}</span>
	)
}

const DfArgRef: React.StatelessComponent<{value: IDfArgument}> = props => {
	const dfArg = props.value;
	const referenceText = dfArg.value === undefined ? 
		"DF"
		: dfArg.value === null ? "null" 
		: ['number', 'boolean', 'string'].findIndex(t => t === typeof dfArg.value) !== -1 ? dfArg.value.toString()
		: '[Obj]';

	return (
		<OverlayTrigger trigger="click" overlay={
			<Popover title="Data Fragment" id={dfArg.id.main}>
				<DfArgument value={dfArg}/>
			</Popover>
		}>
			<span className="argRef dfArgumentRef">{referenceText}</span>
		</OverlayTrigger>
	)
}

const ExprArgRef: React.StatelessComponent<{value: IExpressionArgument}> = props => {
	const exprArg = props.value;
	const referenceText = exprArg.value !== undefined ? exprArg.value.toString() : exprArg.type;

	return (
		<OverlayTrigger trigger="click" overlay={
			<Popover title="Expression" id={exprArg.type}>
				<ExpressionArgument value={exprArg}/>
			</Popover>
		}>
		<span className="argRef exprArgumentRef">{referenceText}</span>
		</OverlayTrigger>
	)
}

const CastArgRef: React.StatelessComponent<{value: ICastArgument}> = props => {
	const castArg = props.value;

	let targetTypeView;
	switch (castArg.type) {
		case 'icast': targetTypeView = "int"; break;
		default: throw new Error(`Not implemented cast type: ${castArg.type}`)
	}

	const referenceText = castArg.value !== undefined ? castArg.value.toString() : targetTypeView;

	return (
		<OverlayTrigger trigger="click" overlay={
			<Popover title="Type Cast" id={castArg.type}>
				<CastArgument value={castArg}/>
			</Popover>
		}>
		<span className="argRef castArgumentRef">{referenceText}</span>
		</OverlayTrigger>
	)
}