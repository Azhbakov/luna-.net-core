import * as React from "react";
import { ArgumentRef } from "./ArgumentRef";
import { ICastArgument } from "src/domain/engine/lang_processing/arguments";

export const CastArgument: React.StatelessComponent<{value: ICastArgument}> = props => {
	const castArg = props.value;
	
	let type;
	switch (castArg.type) {
		case 'icast': type = "int"; break;
		default: throw new Error(`Not implemented cast type: ${castArg.type}`)
	}

	return (
		<div className="castArgument">
			<div>
				<span className="argumentPropertyName">Cast:</span>
				<ArgumentRef value={castArg.expr} />
				<span className="operation">to</span>
				<span className="castTo">{type}</span>
			</div>
			<div>
				<span className="argumentPropertyName">Value:</span>
				{castArg.value && <span className="exprArgumentValue">{castArg.value}</span>}
				{castArg.value === undefined && <span className="dfArgumentNoValue">*not ready*</span>}
			</div>
		</div>
	)
}