import { ICf } from "../../../domain/engine/lang_processing/cf";
import { ExecCf } from "./ExecCf";
import * as React from "react";
import { IExecCf } from "src/domain/engine/lang_processing/cf_impl/execCf";
import { ForCf } from "./ForCf";
import { IForCf } from "../../../domain/engine/lang_processing/cf_impl/forCf";
import { IWhileCf } from "../../../domain/engine/lang_processing/cf_impl/whileCf";
import { WhileCf } from "src/components/LobbyComponents/DomainView/WhileCf";

export const CfView: React.StatelessComponent<{value: ICf}> = props => {
	const cf = props.value;

	switch (cf.type) {
		case 'exec':
			return <ExecCf value={cf as IExecCf}/>
		case 'for':
			return <ForCf value={cf as IForCf}/>
		case 'while':
			return <WhileCf value={cf as IWhileCf}/>
		default:
			throw new Error("Not implemented CF view: " + cf.type);
	}
}