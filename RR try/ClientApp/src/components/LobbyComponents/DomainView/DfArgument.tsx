import * as React from "react";
import { IDfArgument } from "../../../domain/engine/lang_processing/arguments";
import { FragmentId } from "./FragmentId";

export interface Props {
	value: IDfArgument
}

export class DfArgument extends React.Component<Props> {
	public render(): JSX.Element {
		const dfArg = this.props.value;
		
		const valueText = dfArg.value === undefined ? 
			"DF"
			: dfArg.value === null ? "null" 
			: ['number', 'boolean', 'string'].findIndex(t => t === typeof dfArg.value) !== -1 ? dfArg.value.toString()
			: '[Obj]';

		const printNeeded = valueText === '[Obj]';
		const printValue = () => {
			console.log(dfArg.value);
		}

		return (
			<div className="dfArgument">
				<div>
					<span className="argumentPropertyName">ID:</span>
					<FragmentId value={dfArg.id}/>
				</div>
				<div>
					<span className="argumentPropertyName">Value:</span>
					{dfArg.value && !printNeeded && <span className="dfArgumentValue">{valueText}</span>}
					{dfArg.value && printNeeded && <span className="dfArgumentValue" onClick={printValue}>{valueText}</span>}
					{dfArg.value === undefined && <span className="dfArgumentNoValue">*not ready*</span>}
				</div>
			</div>
		)
	}
}