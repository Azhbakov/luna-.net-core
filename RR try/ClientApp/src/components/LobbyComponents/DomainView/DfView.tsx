import { IDf } from "../../../domain/engine/lang_processing/cf";
import * as React from "react";
import { FragmentId } from "./FragmentId";
import { Panel } from "react-bootstrap";

export const DfView: React.StatelessComponent<{value: IDf}> = props => {
	const df = props.value;


	return (
		<Panel className="dfCard">
		<div className="df">
			<FragmentId value={df.id}/>
			<span className="colon">:</span>
			<DfValueView value={df.value}/>
		</div>
		</Panel>
	)
}

const DfValueView: React.StatelessComponent<{value: any}> = props => {
	const value = props.value;
	const isPrimitive = ['number', 'string', 'boolean'].findIndex(t => t === typeof value) !== -1;

	const onClick = () => {
		console.log(value);
	}
	return isPrimitive ?
		(<span className="dfValue" >{value.toString()}</span>)
		: (<span className="dfValue" onClick={onClick}>[Obj]</span>)
}
