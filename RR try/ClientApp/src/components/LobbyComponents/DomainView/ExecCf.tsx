import * as React from "react";
import { ArgumentRef } from "./ArgumentRef";
import { FragmentId } from "./FragmentId";
import { IArgument } from "../../../domain/engine/lang_processing/arguments";
import { IExecCf } from "../../../domain/engine/lang_processing/cf_impl/execCf";
import { Panel } from "react-bootstrap";

export interface Props {
	value: IExecCf
}

export class ExecCf extends React.Component<Props> {
	public render(): JSX.Element {
		const cf = this.props.value
		const prototype = cf.prototype;

		return (
			<Panel className="cfCard">
			<span className="cf">
				<span className="cfTitle">{cf.prototypeName}</span>
				<span className="parenthesis">(</span>

				{cf.args.map((arg, i) => (
					<span className="cfArguments" key={i}>
						<CfArgument value={arg}/>
	
						{/* ARG DETAILS */}
						{ prototype.type === 'extern' && 
							<span>
								<span className="as">:</span>
								<span className="cfParameterType">{prototype.args[i].type}</span>
							</span>
						}
						{ prototype.type === 'struct' && 
							<span>
								<span className="as">:</span>
								<span className="cfParameterName">{prototype.args[i].id}</span>
								<span className="colon">:</span>
								<span className="cfParameterType">{prototype.args[i].type}</span>
							</span>
						}
						
						{(i !== cf.args.length-1) && <span className="comma">,</span>}
					</span>
				))}

				<span className="parenthesis">)</span>
				<span className="arrow">⇨</span>
				<FragmentId value={cf.id}/>
			</span>
			</Panel>
		)
	}
}

const CfArgument: React.StatelessComponent<{value: IArgument}> = props => {
	const arg = props.value;
	return (
		<span className="cfArgument">
			<ArgumentRef value={arg}/>
		</span>
	)
}