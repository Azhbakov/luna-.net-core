import * as React from "react";
import { ArgumentRef } from "./ArgumentRef";
import { IExpressionArgument } from "src/domain/engine/lang_processing/arguments";

export const ExpressionArgument: React.StatelessComponent<{value: IExpressionArgument}> = props => {
	const exprArg = props.value;
	
	return (
		<div className="exprArgument">
			<div>
				<span className="argumentPropertyName">Action:</span>
				<ArgumentRef value={exprArg.operands[0]} />
				<span className="operation">{exprArg.type}</span>
				<ArgumentRef value={exprArg.operands[1]} />
			</div>
			<div>
				<span className="argumentPropertyName">Value:</span>
				{exprArg.value && <span className="exprArgumentValue">{exprArg.value}</span>}
				{exprArg.value === undefined && <span className="dfArgumentNoValue">*not ready*</span>}
			</div>
		</div>
	)
}