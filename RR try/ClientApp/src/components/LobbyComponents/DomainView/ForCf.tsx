import * as React from "react";
import { ArgumentRef } from "./ArgumentRef";
import { FragmentId } from "./FragmentId";
import { IArgument } from "../../../domain/engine/lang_processing/arguments";
import { Panel } from "react-bootstrap";
import { IForCf } from "src/domain/engine/lang_processing/cf_impl/forCf";

export interface Props {
	value: IForCf
}

export class ForCf extends React.Component<Props> {
	public render(): JSX.Element {
		const forCf = this.props.value

		return (
			<Panel className="cfCard">
			<span className="for">
				<span className="forTitle">for </span>
				<span className="forVar">{forCf.var}</span>
				<span className="forIn"> in </span>
				<span className="parenthesis">(</span>
				<ForArgument value={forCf.first}/>
				<span className="forDots">...</span>
				<ForArgument value={forCf.last}/>
				<span className="parenthesis">)</span>

				<span className="arrow">⇨</span>
				<FragmentId value={forCf.id}/>
			</span>
			</Panel>
		)
	}
}

const ForArgument: React.StatelessComponent<{value: IArgument}> = props => {
	const arg = props.value;
	return (
		<span className="cfArgument">
			<ArgumentRef value={arg}/>
		</span>
	)
}