import { IFragmentId } from "../../../domain/engine/lang_processing/cf";
import * as React from "react";
import { ArgumentRef } from "./ArgumentRef";
import { IArgument } from "../../../domain/engine/lang_processing/arguments";

const FragmentIdPart: React.StatelessComponent<{value: IArgument}> = props => {
	const idPart = props.value;
	return (
		<span className="fragmentIdPart">
			<span className="bracket">[</span>
				<ArgumentRef value={idPart}/>
			<span className="bracket">]</span>
		</span>
	)
}

export const FragmentId: React.StatelessComponent<{value: IFragmentId}> = props => {
	const id = props.value;
	return (
		<span className="fragmentId">
			<span className="fragmentIdMain">{id.main}</span>
			{id.indicies.map((idPart, i) => <FragmentIdPart value={idPart} key={i}/>)}
		</span>
	)
}