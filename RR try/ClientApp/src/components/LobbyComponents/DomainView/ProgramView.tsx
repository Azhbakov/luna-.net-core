
import * as React from "react";
import { IArgument, ICastArgument } from "../../../domain/engine/lang_processing/arguments";
import { lunaLang } from "../../../domain/engine/lang_processing/lunaLang";

export const ProgramView: React.StatelessComponent<{value: lunaLang.Program}> = props => {
	const program = props.value;
	return (
		<div>
			{Object.keys(program).map((subName, i) => 
				<DeclarationStatement key={i} name={subName} value={program[subName]}/>)
			}
		</div>
	);
}

export const DeclarationStatement: React.StatelessComponent<{name: string, value: lunaLang.DeclarationStatement}> = props => {
	const { name, value: statement } = props;
	if (lunaLang.isAtom(statement)) return <Atom name={name} value={statement as lunaLang.Atom}/>;
	if (lunaLang.IsSubroutine(statement)) return <Subroutine name={name} value={statement as lunaLang.Subroutine}/>;
	return <span>NOT IMPLEMENTED</span>
}

export const Atom: React.StatelessComponent<{name: string, value: lunaLang.Atom}> = props => {
	const { name, value: atom } = props;
	return (
		<div className='declarationStatement atom'>
			<span className='atomTitle'>{name}</span>
			<span className='parenthesis'>(</span>
			{atom.args.map((param, i) => (
				<span className="atomArguments" key={i}>
					<span className="atomParameterType">{param.type}</span>
					{(i !== atom.args.length-1) && <span className="comma">,</span>}
				</span>
			))}
			<span className='parenthesis'>)</span>
		</div>
	)
}

export const Subroutine: React.StatelessComponent<{name: string, value: lunaLang.Subroutine}> = props => {
	const { name, value: subroutine } = props;
	return (
		<div className="declarationStatement subroutine">
			<span className='subroutineTitle'>{name}</span>
			<span className='parenthesis'>(</span>
			{subroutine.args.map((param, i) => (
				<span className="subroutineArguments" key={i}>
					<span className="subroutineParameterName">{param.id}</span>
					<span className="colon">:</span>
					<span className="subroutineParameterType">{param.type}</span>
					{(i !== subroutine.args.length-1) && <span className="comma">,</span>}
				</span>
			))}
			<span className='parenthesis'>)</span>
			<Body value={subroutine.body}/>
		</div>
	)
}

export const Body: React.StatelessComponent<{value: lunaLang.IBody}> = props => {
	const body = props.value;
	return (
		<div className={'body'}>
			<div className="brace">{'{'}</div>
				<div className="bodyStatements">
				{body.map((statement, i) => {
					switch (statement.type) {
						case 'dfs': return <DfDecl key={i} value={statement as lunaLang.IDfDeclaration}/>
						case 'exec': return <Exec key={i} value={statement as lunaLang.IExec}/>
						case 'for': return <For key={i} value={statement as lunaLang.IFor}/>;
						case 'while': return <While key={i} value={statement as lunaLang.IWhile}/>;
						default: return <span>NOT IMPLEMENTED</span>
					}
				})}
				</div>
			<div className="brace">{'}'}</div>
		</div>
	)
}

const DfDecl: React.StatelessComponent<{value: lunaLang.IDfDeclaration}> = props => {
	const dfDecl = props.value;
	return (
		<div className='bodyStatement'>
			{dfDecl.names.length !== 0 ? 'df ' : ''}
			{dfDecl.names.map((dfName, i) => 
			[
				<Argument key={i} value={{type: 'id', ref: [dfName]} as lunaLang.IDfArgument}/>,
				i !== dfDecl.names.length - 1 ? ',' : ''
			])}
		</div>
	)
}

const Exec: React.StatelessComponent<{value: lunaLang.IExec}> = props => {
	const exec = props.value;
	return (
		<div className='bodyStatement exec'>
			<span className='execTitle'>{exec.code}</span>
			<span className='parenthesis'>(</span>
				{exec.args.map((arg, i) => (
					<span className="execArguments" key={i}>
						<Argument value={arg}/>
						{(i !== exec.args.length-1) && <span className="comma">,</span>}
					</span>
				))}
			<span className='parenthesis'>)</span>

			<span className="arrow">⇨</span>
			<span className="fragmentId fragmentIdMain">{exec.id[0]}</span>
			<span className="fragmentId fragmentIdPart">
				{exec.id.filter((arg, i) => i > 0)
					.map((arg, i) => 
						<span className="fragmentIdPart" key={i}>
							<span className='bracket'>[</span>
								<Argument value={arg as lunaLang.Argument}/>
							<span className='bracket'>]</span>
						</span>
					)
				}
			</span>
		</div>
	)
}

export const For: React.StatelessComponent<{value: lunaLang.IFor}> = props => {
	const forDecl = props.value;
	return (
		<div className="declarationStatement subroutine">
			<span className="forTitle">for </span>
			<span className="forVar">{forDecl.var}</span>
			<span className="forIn"> in </span>
			<span className="parenthesis">(</span>
				<Argument value={forDecl.first as lunaLang.Argument}/>
				<span className="forDots">...</span>
				<Argument value={forDecl.last as lunaLang.Argument}/>
			<span className="parenthesis">)</span>
			<Body value={forDecl.body}/>
		</div>
	)
}

export const While: React.StatelessComponent<{value: lunaLang.IWhile}> = props => {
	const whileDecl = props.value;
	return (
		<div className="declarationStatement subroutine">
			<span className="whileTitle">while </span>
				<span className="whileCond"><Argument value={whileDecl.cond as lunaLang.Argument}/></span>
				<span className="whileVar">{whileDecl.var}</span>
				<span className="whileText"> = </span>
				<span className="whileFrom"><Argument value={whileDecl.start as lunaLang.Argument}/></span>
				<span className="whileText"> out </span>
				<span className="whileOut"><Argument value={whileDecl.wout as lunaLang.Argument}/></span>
			<Body value={whileDecl.body}/>
		</div>
	)
}

const getCastTypeShortView = (cast: lunaLang.ICast): string => {
	switch (cast.type) {
		case 'icast': return "int";
		default: throw new Error(`Not implemented cast type: ${cast.type}`)
	}
}

const Argument: React.StatelessComponent<{value: lunaLang.Argument}> = props => {
	const arg = props.value;

	const innerExpr = (a: lunaLang.Argument) => (
		<span className='bodyStatement expression'>
			{lunaLang.IsIExpression(a)
				? <span>
					<span className='parenthesis'>(</span>
						<Argument value={a}/>
					<span className='parenthesis'>)</span>
				</span>
				: (lunaLang.IsICast(a) ? 
					<span className='arg castArg'>
						<span className="castType">{getCastTypeShortView(a)}</span>
						<span className='parenthesis'>(</span>
							<Argument value={a.expr}/>
						<span className='parenthesis'>)</span>
					</span>
				: <Argument value={a}/>)
			}
		</span>
	)

	if (lunaLang.IsIConstArgument(arg)) {
		return <span className='arg constArg'>{(arg as lunaLang.IConstArgument).value}</span>;
	} else if (lunaLang.IsIDfArgument(arg)) {
		return <span className='arg dfArg'>
			{arg.ref[0]}
			{arg.ref.filter((a, i) => i > 0)
				.map((a, i) => 
					<span className='idParts' key={i}>
						<span className='bracket'>[</span>
						<Argument value={a as IArgument}/>
						<span className='bracket'>]</span>
					</span>
				)
			}
		</span>
	} else if (lunaLang.IsIExpression(arg)) {
		return <span className='arg expressionArg'>
			{innerExpr(arg.operands[0])}
			<span className='operation'>{arg.type}</span>
			{innerExpr(arg.operands[1])}
		</span>
	} else if (lunaLang.IsICast(arg)) {
		const targetTypeView = getCastTypeShortView(arg as lunaLang.ICast);
		return <span className='arg castArg'>
			<span className="castType">{targetTypeView}</span>
			{innerExpr(arg.expr)}
		</span>
	} else return <span>NOT IMPLEMENTED</span>
}