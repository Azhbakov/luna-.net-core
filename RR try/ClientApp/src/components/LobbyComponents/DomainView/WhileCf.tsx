import * as React from "react";
import { ArgumentRef } from "./ArgumentRef";
import { FragmentId } from "./FragmentId";
import { IArgument } from "../../../domain/engine/lang_processing/arguments";
import { Panel } from "react-bootstrap";
import { IWhileCf } from "../../../domain/engine/lang_processing/cf_impl/whileCf";

export interface Props {
	value: IWhileCf
}

export class WhileCf extends React.Component<Props> {
	public render(): JSX.Element {
		const whileCf = this.props.value

		return (
			<Panel className="cfCard">
			<span className="while">
				<span className="whileTitle">while </span>
				<span className="whileCond"><WhileArgument value={whileCf.condition}/> </span>
				<span className="whileVar">{whileCf.var}</span>
				<span className="whileText"> = </span>
				<span className="whileFrom"><WhileArgument value={whileCf.start}/></span>
				<span className="whileText"> out </span>
				<span className="whileOut"><WhileArgument value={whileCf.wout}/></span>

				<span className="arrow">⇨</span>
				<FragmentId value={whileCf.id}/>
			</span>
			</Panel>
		)
	}
}

const WhileArgument: React.StatelessComponent<{value: IArgument}> = props => {
	const arg = props.value;
	return (
		<span className="cfArgument">
			<ArgumentRef value={arg}/>
		</span>
	)
}