import * as React from "react";
import { Col, Panel } from "react-bootstrap";

export interface Props {
	heading?: React.ReactNode,
	height?: number
	width?: number
}

export class GalleryCard extends React.Component<Props> {
	public render(): JSX.Element {
		const style: React.CSSProperties = {};
		if (this.props.height) style.height = `${this.props.height}px`;
		if (this.props.width) style.width = `${this.props.width}px`;

		return (
			<Col style={{width: 'auto', display: 'inline-block', paddingRight: '10px', float: 'left'}}>
			<Panel style={{marginBottom: "3px"}}>
				{this.props.heading && <Panel.Heading>{this.props.heading}</Panel.Heading>}
				<Panel.Body style={style}>
					{this.props.children}
				</Panel.Body>
			</Panel>
			</Col>
		)
	}
}