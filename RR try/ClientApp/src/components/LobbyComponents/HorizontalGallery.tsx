import * as React from "react";
import { Grid, Row } from "react-bootstrap";

export class HorizontalGallery extends React.Component {
	public render() {
		return (
			<Grid fluid style={{paddingBottom: "20px"}}>
				<Row children={this.props.children} style={{overflowX: "auto", whiteSpace: "nowrap"}}/>
			</Grid>
		);
	}
}