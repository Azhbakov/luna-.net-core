import { ILobby } from "../../domain/Lobby";
import * as React from "react";
import { Panel, Grid, Row, Col } from "react-bootstrap/lib";

export interface LobbyDetailsProps {
	lobby: ILobby,
}

export class LobbyDetails extends React.Component<LobbyDetailsProps> {
	public render() {
		const lobby = this.props.lobby;
		return (
			<Panel>
				<Panel.Heading>Lobby Details</Panel.Heading>
				<Panel.Body>
					<Grid fluid>
						<Row><Col xs={6}><strong>ID</strong></Col><Col xs={6}><span>{lobby.id}</span></Col></Row>
						<Row><Col xs={6}><strong>Name</strong></Col><Col xs={6}><span>{lobby.name}</span></Col></Row>
						<Row><Col xs={6}><strong>Task</strong></Col><Col xs={6}><span>{lobby.task.name}</span></Col></Row>
						<Row><Col xs={6}><strong>Max Users</strong></Col><Col xs={6}><span>{lobby.maxUsersCount? lobby.maxUsersCount : "-"}</span></Col></Row>
						<Row><Col xs={6}><strong>Leader ID</strong></Col><Col xs={6}><span>{lobby.leaderId? lobby.leaderId : "-" }</span></Col></Row>
						<Row><Col xs={6}><strong>Status</strong></Col><Col xs={6}><span>{lobby.status}</span></Col></Row>
					</Grid>
				</Panel.Body>
			</Panel>
		);
	}
}