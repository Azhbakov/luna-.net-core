import * as React from "react";
import { IPeerConnectionState } from "src/store/PeerConnection";
import { Row, Col, Grid, Panel } from "react-bootstrap";
import { GalleryCard } from "./GalleryCard";

export interface PeerComponentProps {
	peerPosition: string,
	connectionState: IPeerConnectionState | undefined,
}

export class PeerConnection extends React.Component<PeerComponentProps> {
	constructor(props: PeerComponentProps) {
		super(props);
	}

	public render() {
		const { peerPosition, connectionState } = this.props;

		const createRow = (name: string, value: any): JSX.Element => 
			<Row><Col xs={6}><strong>{name}</strong></Col><Col xs={6} style={{ textAlign: "right" }}><span>{value }</span></Col></Row>

		return (
			<GalleryCard heading={`Peer: ${peerPosition}`} height={210} width={280}>
				{ connectionState && <h5 style={{ textAlign: "center" }}>[{connectionState.peerId}]</h5> }
				{ connectionState && (
					<Grid fluid>
						{createRow("Signaling", connectionState.signalingState)}
						{createRow("ICE connection", connectionState.iceConnectionState)}
						{createRow("ICE gathering", connectionState.iceGatheringState)}
						{createRow("ICE candidates", connectionState.iceCandidates)}
						{connectionState.error && createRow("ICE error", connectionState.error)}
						{createRow("Data channel", connectionState.dataChannelState)}
						{createRow("Messages in", connectionState.dataChannelMessagesIn)}
						{createRow("Messages out", connectionState.dataChannelMessagesOut)}
						{connectionState.dataChannelError && createRow("Data channel error", connectionState.dataChannelError)}
					</Grid>
				)}
				{ !connectionState && ("Not connected")}
			</GalleryCard>
		);
	}
}