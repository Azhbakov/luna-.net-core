import { IPeerConnectionState } from "src/store/PeerConnection";
import * as React from "react";
import { PeerConnection } from "./PeerConnection";
import { HorizontalGallery } from "./HorizontalGallery";

export interface PeerConnectionData {
	peerPosition: string,
	state: IPeerConnectionState | undefined,
}

export interface PeerConnectionsGalleryProps {
	peerConnections: PeerConnectionData[] | null,
}

export class PeerConnectionsGallery extends React.Component<PeerConnectionsGalleryProps> {
	public render() {
		const children: JSX.Element[] = [];
		const peerConnections = this.props.peerConnections;
		if (!peerConnections || peerConnections.length === 0) return (<span>No neighbors.</span>);

		peerConnections.forEach(pc => {
			children.push(<PeerConnection key={pc.peerPosition} peerPosition={pc.peerPosition} connectionState={pc.state}/>);
		});
		return (
			<div>
				<h3>Peer connections</h3>
				<HorizontalGallery>{children}</HorizontalGallery>
			</div>
		);
	}
}