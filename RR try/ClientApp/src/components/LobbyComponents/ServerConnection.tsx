import * as React from "react";
import { Panel, Grid, Row, Col } from "react-bootstrap/lib";
import { ServerConnectionState } from "../../store/ServerConnection";

export interface ServerConnectionProps {
	serverConnection: ServerConnectionState
}

export class ServerConnection extends React.Component<ServerConnectionProps> {
	public constructor(props: ServerConnectionProps) {
		super(props);
	}

	public render(): JSX.Element {
		const serverConnection = this.props.serverConnection;
		return (
			<div>
				<Panel>
					<Panel.Heading>Server connection</Panel.Heading>
					<Panel.Body>
						<Grid fluid>
							<Row>
								<Col xs={6}><strong>Status</strong></Col><Col xs={6}><span>{serverConnection.status}</span></Col>
								{serverConnection.error && <div><Col xs={6} color={'red'}><strong>Error</strong></Col><Col xs={6} color={'red'}><span>{serverConnection.error}</span></Col></div>}
							</Row>
						</Grid>
					</Panel.Body>
				</Panel>
			</div>
		);
	}
}