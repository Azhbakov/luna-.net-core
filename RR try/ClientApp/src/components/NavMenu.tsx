import * as React from "react";
import * as Navbar from "react-bootstrap/lib/Navbar";
import * as Nav from "react-bootstrap/lib/Nav";
import * as NavItem from "react-bootstrap/lib/NavItem";
import * as Glyphicon from "react-bootstrap/lib/Glyphicon";
import { Link } from "react-bootstrap/lib/Navbar";
import { LinkContainer } from "react-router-bootstrap";
import './NavMenu.css';

export const  NavMenu: React.StatelessComponent = () => {
    return (
        <Navbar inverse={true} fixedTop={true} fluid={true} collapseOnSelect={true}>
            <Navbar.Header>
                <Navbar.Brand>
                    <Link href={'/'}>LuNA</Link>
                </Navbar.Brand>
                <Navbar.Toggle />
            </Navbar.Header>
            <Navbar.Collapse>
                <Nav>
                    <LinkContainer to={'/'} exact={true}>
                        <NavItem>
                            <Glyphicon glyph='home' /> Home
                        </NavItem>
                        </LinkContainer>
                    <LinkContainer to={'/counter'}>
                        <NavItem>
                            <Glyphicon glyph='education' /> Counter
                        </NavItem>
                    </LinkContainer>
					<LinkContainer to={'/fragmentViewer'}>
                        <NavItem>
                            <Glyphicon glyph='education' /> Fragment Viewer
                        </NavItem>
                    </LinkContainer>
					<LinkContainer to={'/lobby'} exact={true}>
                        <NavItem>
                            <Glyphicon glyph='home' /> Execution
                        </NavItem>
					</LinkContainer>
                    <LinkContainer to={'/lobbies'}>
                        <NavItem>
                            <Glyphicon glyph='th-list' /> Lobbies
                        </NavItem>
                    </LinkContainer>
                    <LinkContainer to={'/tasks'}>
                        <NavItem>
                            <Glyphicon glyph='upload' /> Tasks
                        </NavItem>
                    </LinkContainer>
                    <LinkContainer to={'/viewDebug'}>
                        <NavItem>
                            <Glyphicon glyph='th-list' /> View Debug
                        </NavItem>
                    </LinkContainer>
					<LinkContainer to={'/execDebug'}>
                        <NavItem>
                            <Glyphicon glyph='th-list' /> Exec Debug
                        </NavItem>
                    </LinkContainer>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
}