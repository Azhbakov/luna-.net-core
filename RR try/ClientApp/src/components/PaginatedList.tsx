import { Pagination } from "react-bootstrap/lib";
import * as React from "react";

interface PaginatedListProps {
	elements: any[];
	elementView: (element: any, i: number) => JSX.Element;
	pageCapacity: number;
}

interface State {
	selectedPage: number;
}

type PageGenerator = () => JSX.Element;
type Page = JSX.Element | PageGenerator

export class PaginatedList extends React.Component<PaginatedListProps, State> {
	public constructor(props: any) {
		super(props);
		this.state = {
			selectedPage: 0
		};
	}

	private paginate(): Page[] {
		const pages: PageGenerator[] = [];
		const { pageCapacity, elements, elementView } = this.props;

		const pageCount = Math.ceil(elements.length / pageCapacity);
		while (pages.length !== pageCount) {
			const pageStart = pageCapacity * pages.length;
			const pageGenerator = () => {
				const pageElements = [];
				for (let i = pageStart; i < Math.min(pageStart + pageCapacity, elements.length); i++) {
					pageElements.push(elementView(elements[i], i));
				}
				return (<div>
					{pageElements}
				</div>);
			}
			pages.push(pageGenerator);
		}
		return pages;
	} 

	public render(): JSX.Element {
		const { pageCapacity, elements, elementView } = this.props;
		const pageCount = Math.ceil(elements.length / pageCapacity);
		const selectedPage = Math.min(this.state.selectedPage, pageCount === 0 ? 0 : pageCount - 1);

		// Current page content
		const fromInd = selectedPage * pageCapacity
		const pageElements = [];
		for (let i = fromInd; i < Math.min(fromInd + pageCapacity, elements.length); i++) {
			pageElements.push(elementView(elements[i], i));
		}

		// Page labels
		const th = this;
		const selectPageHandler = (n: number) => () => { th.setState({selectedPage: n}); }
		const pageItems = [];
		if (pageCount <= 10) {
			for (let i = 0; i < pageCount; i++) {
				pageItems.push(<Pagination.Item onClick={selectPageHandler(i)} key={i} active={i === selectedPage}>{i+1}</Pagination.Item>);
			}
		} else {
			for (let i = 0; i <= 2; i++) {
				pageItems.push(<Pagination.Item onClick={selectPageHandler(i)} key={i} active={i === selectedPage}>{i+1}</Pagination.Item>);
			}
			if (3 <= selectedPage - 2) {
				pageItems.push(<Pagination.Ellipsis key={"ellipsis1"}></Pagination.Ellipsis>)
			}
			if (2 <= selectedPage && selectedPage <= pageCount-3) {
				for (let i = selectedPage-1; i <= selectedPage+1; i++) {
					if (i <= 2 || pageCount - 3 <= i) continue;
					pageItems.push(<Pagination.Item onClick={selectPageHandler(i)} key={i} active={i === selectedPage}>{i+1}</Pagination.Item>);
				}	
			}
			if (selectedPage + 2 <= pageCount - 4) {
				pageItems.push(<Pagination.Ellipsis key={"ellipsis2"}></Pagination.Ellipsis>)
			}
			for (let i = pageCount-3; i < pageCount; i++) {
				pageItems.push(<Pagination.Item onClick={selectPageHandler(i)} key={i} active={i === selectedPage}>{i+1}</Pagination.Item>);
			}
		}

		return (
			<div>
				<div>
					{pageCount === 0 ? <span>Empty</span> : pageElements}
				</div>
				{pageItems.length > 1 && 
					<Pagination>
						{pageItems}
					</Pagination>
				}
			</div>
		)
	}
}
