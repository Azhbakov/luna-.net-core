import * as React from "react";
import { ICf, IDf, fragmentIdMethods } from "../domain/engine/lang_processing/cf";
import update from "immutability-helper";
import { CfView } from "./LobbyComponents/DomainView/CfView";
import { DfView } from "./LobbyComponents/DomainView/DfView";
import { IObservableStorage } from "src/domain/engine/runtime/distributedStorage";
import { IObservableBufferedStorage } from "src/domain/engine/runtime/webworker/bufferedStorageView";
import { Pagination } from "react-bootstrap";
import { PaginatedList } from "./PaginatedList";

export type Props = {
	storage: IObservableStorage | IObservableBufferedStorage;
}

export type State = {
	selectedPage: number;
	cfs: ICf[];
	dfs: IDf[];
}

export class StorageView extends React.Component<Props, State> {
	constructor(props: Props) {
		super(props);
		const storage = this.props.storage;
		
		const here = this;

		this.state = {
			selectedPage: 0,
			cfs: storage.getCfs().slice(), // slice is copy
			dfs: storage.getDfs().slice()
		};

		const removeCf = (cf: ICf) => {
			const ind = here.state.cfs.findIndex(c => fragmentIdMethods.equals(cf.id, c.id));
			if (ind === -1) return;
			here.setState(update(here.state, { cfs: { $splice: [[ind, 1]]}}));
		};
		
		storage.onCfAdded.addCallback(cf => here.setState(update(here.state, { cfs: { $push: [cf] } })));
		storage.onDfAdded.addCallback(df => here.setState(update(here.state, { dfs: { $push: [df] } })));
		storage.onCfRemoved.addCallback(removeCf);
		if ((storage as IObservableBufferedStorage).onBatchUpdate) {
			(storage as IObservableBufferedStorage).onBatchUpdate.addCallback(b => {
				this.state.cfs.push(...b.cfs);
				this.state.dfs.push(...b.dfs);
				here.setState({
					cfs: this.state.cfs.filter(cf => b.removedCfs.findIndex(c => fragmentIdMethods.equals(cf.id, c.id)) === -1),
					dfs: this.state.dfs
				});
			});
		}
	}
	
	public render(): JSX.Element {
		const cfView = (cf: ICf, i: number) => <CfView key={`cfView_${i}`} value={cf}/>;
		const dfView = (df: IDf, i: number) => <DfView key={`dfView_${i}`} value={df}/>;

		return (
			<div>
				<h3>CF storage</h3>
				<PaginatedList elements={this.state.cfs} elementView={cfView} pageCapacity={10}/>
				<h3>DF storage</h3>
				<PaginatedList elements={this.state.dfs} elementView={dfView} pageCapacity={10}/>
			</div>
		);
	}
}