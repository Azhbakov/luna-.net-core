import * as React from "react";
import { ICf, IDf } from "../domain/engine/lang_processing/cf";
import { CfView } from "./LobbyComponents/DomainView/CfView";
import { DfView } from "./LobbyComponents/DomainView/DfView";
import { PaginatedList } from "./PaginatedList";

export type Props = {
	cfs: ICf[];
	dfs: IDf[];
}

export class StorageView2 extends React.Component<Props> {
	constructor(props: Props) {
		super(props);
	}
	
	public render(): JSX.Element {
		const cfView = (cf: ICf, i: number) => <CfView key={`cfView_${i}`} value={cf}/>;
		const dfView = (df: IDf, i: number) => <DfView key={`dfView_${i}`} value={df}/>;

		return (
			<div>
				<h3>CF storage</h3>
				<PaginatedList elements={this.props.cfs} elementView={cfView} pageCapacity={10}/>
				<h3>DF storage</h3>
				<PaginatedList elements={this.props.dfs} elementView={dfView} pageCapacity={10}/>
			</div>
		);
	}
}