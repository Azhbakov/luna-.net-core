import * as React from "react";
import { TasksState, taskAtomsFetchUrl, taskProgramFetchUrl } from "../store/Tasks";
import { connect } from "react-redux";
import { AppState } from "src/store/configureStore";
import { DispatchConnectedProps, mapDispatchToProps } from "../store";
import { lunaLang } from "../domain/engine/lang_processing/lunaLang";
import { Atoms } from "../domain/engine/lang_processing/cf";
import { parseAtoms, parseProgram } from "../domain/engine/runtime/parser";
import { ProgramView } from "./LobbyComponents/DomainView/ProgramView";

interface Props {
	tasks: TasksState,
	match: { params: { taskId: string } },
}

interface State {
	programRaw: any;
	atomsRaw: any;
	program: lunaLang.Program | null;
	atoms: Atoms | null,
	atomsError: string | null,
	programError: string | null
}

class TaskViewBase extends React.Component<Props & DispatchConnectedProps, State> {
	constructor(props: any) {
        super(props);
        this.state = {
            programRaw: null,
			atomsRaw: null,
			program: null,
			atoms: null,
			atomsError: null,
			programError: null
        };
	}

	private fetchText(url: string): Promise<string> {
		return fetch(url)
			.then(res => {
				if (res.status !== 200 || !res.body) {
					throw res.statusText;
				} else {
					return res.text();
				}
			}).then(atomsText => {
				return atomsText;
			});
	}

	public componentDidMount() {
		this.fetchText(taskAtomsFetchUrl(this.props.match.params.taskId))
		.then(atomsText => {
			const atoms = parseAtoms(atomsText);
			this.setState({...this.state,
				atomsRaw: atomsText, atoms, atomsError: null
			})
		}).catch(err => this.setState({...this.state,
			atomsRaw: null, atoms: null, atomsError: err
		}));

		this.fetchText(taskProgramFetchUrl(this.props.match.params.taskId))
		.then(programText => {
			const program = parseProgram(programText);
			this.setState({...this.state,
				programRaw: programText, program, programError: null
			})
		}).catch(err => this.setState({...this.state,
			programRaw: null, program: null, programError: err
		}));
	}
	
	public render(): JSX.Element {
		return (
			<div>
				<h1>Task</h1>
				{!this.state.programRaw && <div>loading</div>}
				{!this.state.atomsRaw && <div>loading</div>}
				{this.state.atomsError && <div>{this.state.atomsError}</div>}
				{this.state.programError && <div>{this.state.programError}</div>}
				{this.state.program && <ProgramView value={this.state.program} />}
			</div>
		);
	}
}

const mapStateToProps = (state: AppState) => state.tasks;

export default connect<TasksState, DispatchConnectedProps, Props>(mapStateToProps, mapDispatchToProps)(TaskViewBase);