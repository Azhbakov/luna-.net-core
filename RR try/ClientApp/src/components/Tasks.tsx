import UploadTask from "./UploadTask";
import * as React from "react";
import { TasksState, Task } from "../store/Tasks";
import { DispatchConnectedProps, mapDispatchToProps } from "../store";
import { AppState } from "../store/configureStore";
import { connect } from 'react-redux';
import { BootstrapTable, TableHeaderColumn, Options } from "react-bootstrap-table";
import { actionCreators } from "../store/Tasks";
import { Modal, Button, Glyphicon, Tooltip, OverlayTrigger } from "react-bootstrap";
import { Link } from "react-router-dom";

interface TasksComponentState {
    uploadShown: boolean
}

class TasksBase extends React.Component<TasksState & DispatchConnectedProps, TasksComponentState> {
    constructor(props: any) {
        super(props);
        this.state = {
            uploadShown: false
        }
    }

    public componentDidMount() {
        this.props.dispatch(actionCreators.taskFetchStart());
    }

    public render () : JSX.Element {
        const handleShow = () => { this.setState({uploadShown: true})};
        const handleHide = () => { this.setState({uploadShown: false})};
        const onTaskCreated =() => {
            handleHide();
            this.props.dispatch(actionCreators.taskFetchStart());
        }

        const onDelete = (row: Task, rowIndex: number) => {
            this.props.dispatch(actionCreators.taskRemoveStart(row.id));
        }

        const createDeleteButton = (cell: any, row: Task, formatExtraData: any, rowIndex: number) => {
            const deleteAction = () => onDelete(row, rowIndex);
            return (
                <Button className="btn-xs btn-icon" onClick={deleteAction}>
                    <Glyphicon glyph="glyph glyphicon-trash" />
                </Button>
            );
		}
		const createPreviewButton = (cell: any, row: Task, formatExtraData: any, rowIndex: number) => {
            return (
                <Link className="btn-xs btn-icon" to={'/Task/' + row.id}>
                    <Glyphicon glyph="glyph glyphicon-play" />
                </Link>
            );
        }

        const withTooltip = (cell: any, row: Task, formatExtraData: any, rowIndex: number) => {
            return (
                <OverlayTrigger 
                    overlay={<Tooltip id={cell}><span>{cell}</span></Tooltip>} 
                    placement="top">
                    <span>{cell}</span>
                </OverlayTrigger>
            )
        }

        const onRefresh = () => {
            this.props.dispatch(actionCreators.taskFetchStart());
        }

        const tableOptions: Options = {
            noDataText: 
                this.props.isLoading ? 'Loading...' : 'Press "Upload" to create new task'
        }

        return (
            <div>
                <Modal show={this.state.uploadShown} onHide={handleHide}>
                    <Modal.Header closeButton>
                        <Modal.Title>Upload task</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <UploadTask onSuccess={onTaskCreated}/>
                    </Modal.Body>
                </Modal>
                
                <h1>
                    Tasks
                </h1>
                <Button className="btn btn-primary" onClick={handleShow}>Upload</Button>
                <Button className="btn-icon" onClick={onRefresh}>
                    <Glyphicon glyph="glyph glyphicon-refresh" />
                </Button>
                <BootstrapTable data={this.props.tasks} options={tableOptions} striped bordered={false} >
                <TableHeaderColumn dataField="id" dataAlign="center" dataFormat={withTooltip} isKey>ID</TableHeaderColumn>
                <TableHeaderColumn dataField="name" dataAlign="center"  dataFormat={withTooltip}>Name</TableHeaderColumn>
                <TableHeaderColumn dataField="button" dataAlign="center" dataFormat={createDeleteButton}>Remove</TableHeaderColumn>
				<TableHeaderColumn dataField="button" dataAlign="center" dataFormat={createPreviewButton}>Preview</TableHeaderColumn>
                </BootstrapTable>
            </div>
        );
    }
}

const mapStateToProps = (state: AppState) => state.tasks;

export default connect<TasksState, DispatchConnectedProps>(mapStateToProps, mapDispatchToProps)(TasksBase);