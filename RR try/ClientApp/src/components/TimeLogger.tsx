import * as React from "react";
import { ExecutionPeriod, IdlePeriod } from "src/domain/engine/runtime/worker";
import { DispatchConnectedProps, mapDispatchToProps } from "src/store";
import { ExecutionState } from "src/store/Execution";
import { AppState } from "src/store/configureStore";
import { connect } from "react-redux";
import { Panel } from "react-bootstrap";
import { FragmentId } from "./LobbyComponents/DomainView/FragmentId";

export type State = {
	execPeriods: ExecutionPeriod[];
	idlePeriods: IdlePeriod[];
}

export const ExecPeriodEntry: React.StatelessComponent<{value: ExecutionPeriod}> = props => {
	return (
		<div>
			<span className="periodTitle"><FragmentId value={props.value.cfId}/>: </span>
			<span className="periodTime">{props.value.startTime.toLocaleTimeString()}</span> - 
			<span className="periodTime">{props.value.finishTime && props.value.finishTime.toLocaleTimeString()}</span>
		</div>
	)
}
export const IdlePeriodEntry: React.StatelessComponent<{value: IdlePeriod}> = props => {
	return (
		<div>
			<span className="periodTitle">Idle: </span>
			<span className="periodTime">{props.value.startTime.toLocaleTimeString()}</span> - 
			<span className="periodTime">{props.value.finishTime && props.value.finishTime.toLocaleTimeString()}</span>
		</div>
	)
}


export const TimeLine: React.StatelessComponent<State> = props => {
	const totalHeight = 400;
	return (
		<div style={{height: totalHeight}}>
			
		</div>
	)
}

export const TimeLogPanel: React.StatelessComponent<State> = props => {
	const periods = props.execPeriods.map(p => ({ type: 'exec', value: p })).concat(
		props.idlePeriods.map(p => ({ type: 'idle', value: p }) as any)
	);
	periods.sort((a, b) => ((a.value.startTime as Date) < (b.value.startTime as Date)) ? -1 : 1);
	
	return (
		<Panel>
			<Panel.Heading>Execution Log</Panel.Heading>
			<Panel.Body>
				{periods.map((period, i) => {
					return period.type === 'exec' ? 
						<ExecPeriodEntry key={i} value={period.value}/> 
						: <IdlePeriodEntry key={i} value={period.value as any}/>;
				})}
			</Panel.Body>
		</Panel>
	)
}

class TimeLogger extends React.Component<ExecutionState & DispatchConnectedProps> {
	public render(): JSX.Element {
		return <TimeLogPanel execPeriods={this.props.execPeriods} idlePeriods={this.props.idlePeriods}/>
	}
}

const mapStateToProps = (state: AppState) => state.execution;

export default connect<ExecutionState, DispatchConnectedProps>(mapStateToProps, mapDispatchToProps)(TimeLogger);