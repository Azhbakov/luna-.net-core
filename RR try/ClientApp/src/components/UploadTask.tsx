import * as React from "react";
import { AppState } from "../store/configureStore";
import { connect } from 'react-redux';
import { actionCreators, FileType, TaskUploadState } from "../store/Upload";
import update from "immutability-helper";
import { DispatchConnectedProps, mapDispatchToProps } from "../store";

interface UploadTaskState {
    taskName: string | null;
    faFile: File | null;
    atomsFile: File | null;
}

interface UploadTaskProps {
    onSuccess?: () => void;
}

class UploadTaskBase extends React.Component<TaskUploadState & DispatchConnectedProps & UploadTaskProps, UploadTaskState> {
    constructor(props: TaskUploadState & DispatchConnectedProps) {
        super(props);
        this.state = {
            taskName: null,
            faFile: null,
            atomsFile: null
        };
    }

    public componentDidUpdate(prevProps: TaskUploadState) {
        const { status, onSuccess } = this.props;
        if (prevProps.status !== "SUCCESS" && status === 'SUCCESS') {
            if (onSuccess) onSuccess();
        }
    }
    
    public render () {
        const bindFile = (e: React.ChangeEvent<HTMLInputElement>, type: FileType): void => {
            if (!e.target || !e.target.files) { return; }
            const file = e.target.files[0];
            switch (type)
            {
                case 'fa':
                    this.setState(update(this.state, { faFile: { $set: file } }));
                    break;
                case 'atoms':
                    this.setState(update(this.state, { atomsFile: { $set: file } }));
                    break;
                default:
                    throw new Error('Unhandled file type: ' + type);
            }
        };

        const bindName = (e: React.ChangeEvent<HTMLInputElement>): void => {
            this.setState(update(this.state, { taskName: { $set: e.target.value } }));
        }

        const bindFaFile = (e: React.ChangeEvent<HTMLInputElement>): void => {
            return bindFile(e, 'fa');
        }
    
        const bindAtomsFile = (e: React.ChangeEvent<HTMLInputElement>): void => {
            return bindFile(e, 'atoms');
        }
        
        const state = this.state;

        const submit = (e: React.FormEvent) => {
            if (!state.taskName || !state.faFile || !state.atomsFile) throw new Error('All files must be provided before submit.')
            this.props.dispatch(actionCreators.uploadStart(state.taskName, state.faFile, state.atomsFile));
            e.preventDefault();
        }

        const isLoading = this.props.status === 'UPLOADING';

        return (
            <div>
                <form onSubmit={submit} >
                    <div className="form-group">
                        <label htmlFor="taskName">{"Task name"}</label>
                        <input className="form-control" id="taskName" placeholder="Enter task name" required onChange={bindName}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="faFile">{"Program file"}</label>
                        <input type="file" className="form-control-file" id="faFile" required onChange={ bindFaFile} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="atomsFile">{"Atoms file"}</label>
                        <input type="file" className="form-control-file" id="atomsFile" required onChange={ bindAtomsFile } />
                    </div>
                    {this.props.error && <label>{this.props.error.message}</label>}
                    <button type="submit" className="btn btn-primary" disabled={isLoading}>{isLoading? "Loading..." : "Upload"}</button>
                </form> 
            </div>
        );
    };
};

const mapStateToProps = (state: AppState) => state.upload;

export default connect<TaskUploadState, DispatchConnectedProps, UploadTaskProps>(mapStateToProps, mapDispatchToProps)(UploadTaskBase);