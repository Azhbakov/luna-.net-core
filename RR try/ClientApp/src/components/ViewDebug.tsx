import { Grid, Button } from "react-bootstrap";
import * as React from "react";
import { IExecCf } from "../domain/engine//lang_processing/cf_impl/execCf";
import { IConstArgument, IDfArgument } from "../domain/engine//lang_processing/arguments";
import { IExecutionContext, IExecutionResult, IProgram, IFragmentId, Atoms } from "../domain/engine//lang_processing/cf";
import { ExecCf } from "./LobbyComponents/DomainView/ExecCf";
import { lunaLang } from "../domain/engine//lang_processing/lunaLang";
import { executeCf } from "../domain/engine//lang_processing/factories";
import { ExecutionView } from "./ExecutionView";
import { DfView } from "./LobbyComponents/DomainView/DfView";
import { while1, while1Atoms } from "../domain/engine/tests/tasks/while1";
import { TimeLogPanel } from "./TimeLogger";
import { ExecutionPeriod, IdlePeriod, IWorker, Worker } from "src/domain/engine/runtime/worker";
import { LocalStorage } from "src/domain/engine/runtime/localStorage";

interface State {
	res: IExecutionResult | null, 
	program: lunaLang.Program,
	atoms: Atoms,
	storage: LocalStorage,
	worker: IWorker
}

export class ViewDebug extends React.Component<{}, State> {
	public constructor(props: any) {
		super(props);
		
		const distributedStorage = new LocalStorage();

		const atoms = while1Atoms;
		const program = while1;

		this.state = { 
		 	res: null,
			program,
			atoms,
			storage: distributedStorage,
			worker: new Worker(program, atoms, distributedStorage)
		};
	}  
	
	public render() {
		const innerAtomDecl: lunaLang.Atom = {
			type: "extern",
			args: [ { type: 'int' }],
			code: 'atom'
		}

		const execInnerSub: lunaLang.IExec = {
			type: 'exec',
			id: [
				"innerId",
				{
					"ref": [
						"firstArg"
					],
					type: "id"
				}
			],
			args: [],
			code: "innerSub"
		} as lunaLang.IExec;

		const subroutineDecl: lunaLang.Subroutine = {
			type: "struct",
			args: [
				{
					id: "firstArg",
					type: "int"
				},
				{
					id: "secondArg",
					type: "value"
				}
			] as lunaLang.NamedParameter[],
			body:  [
				execInnerSub
			]
		} as lunaLang.Subroutine;
		
		const program: lunaLang.Program = {
			"main": subroutineDecl,
			"innerSub": innerAtomDecl
		}
		
		const execCf: IExecCf = {
			type: 'exec',
			id: {
				main: "_l9",
				indicies: [
					{ 
						type: 'iconst', 
						value: 42 
					} as IConstArgument,
					{ 
						type: 'id',
						id: { 
							main: "_df",
							indicies: [
								{ 
									type: 'id', 
									id: {
										main: "_dfIdPart",
										indicies: [],
									},
									value: undefined
								} as IDfArgument,
							],
						},
						value: 42 
					} as IDfArgument,
				]
			},
			args: [
				{ 
					type: 'id', 
					id: {
						main: "_dfArg",
						indicies: [
							{ 
								type: 'iconst', 
								value: 15 
							} as IConstArgument,
						]
					},
					value: undefined
				} as IDfArgument,
				{ 
					type: 'iconst', 
					value: 11 
				} as IConstArgument,
			],
			prototypeName: "main",
			prototype: subroutineDecl,
			context: {
				variables: [],
				parentCfId: { main: "", indicies: [] }
			} as IExecutionContext
		} as IExecCf;

		const execute = (): void => {
			this.setState({res: executeCf(execCf, { source: program, atoms: [] } as IProgram )});
		}

		const createPeriods: () => { execPeriods: ExecutionPeriod[], idlePeriods: IdlePeriod[] } = () =>{
			const time = new Date();
			const prevTime = time;

			const execPeriods: ExecutionPeriod[] = [];
			const idlePeriods: IdlePeriod[] = [];
			for (let i = 0; i < 5; i++) {
				const newStart = new Date(prevTime.getTime());
				prevTime.setSeconds(prevTime.getSeconds() + 5);
				const newFinish = new Date(prevTime.getTime());
				const idleStart = new Date(prevTime.getTime());
				prevTime.setSeconds(prevTime.getSeconds() + 1);
				const idleFinish = new Date(prevTime.getTime());

				execPeriods.push({ cfId: { main: i.toString(), indicies: [] } as IFragmentId, startTime: newStart, finishTime: newFinish } as ExecutionPeriod)
				idlePeriods.push({ startTime: idleStart, finishTime: idleFinish } as IdlePeriod);
			}
			return { execPeriods, idlePeriods };
		}
		const periods = createPeriods();

		return (
			<Grid fluid>
				<h1>Components View Debug</h1>
				<h2>Exec CF</h2>
				<ExecCf value={execCf}/>
				<div>
					<Button onClick={execute}>Execute</Button>
				</div>
				<DfView value={{id: { main: "testDf", indicies: []}, value: 42 }}/>
				<div>
				{this.state.res && this.state.res.cfs.map((cf, i) => {
					return (
						<div key={i}>
							{cf.type === 'exec' ? <ExecCf value={cf as IExecCf}/> : "Not implemented"}
						</div>
					)
				})}
				</div>
				<h2>Subroutine</h2>
				<ExecutionView program={while1} atoms={while1Atoms} storage={this.state.storage} worker={this.state.worker}/>
				<TimeLogPanel execPeriods={periods.execPeriods} idlePeriods={periods.idlePeriods}/>
			</Grid>
		)
	}
}