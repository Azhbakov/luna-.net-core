export interface ExecutionSettings {
	updateUI: boolean,
	mode: 'RUN' | 'STEP',
	isLeader: boolean
}