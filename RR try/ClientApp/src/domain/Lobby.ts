import { Atoms } from "./engine/lang_processing/cf";
import { lunaLang } from "./engine/lang_processing/lunaLang";

export interface IPeer {
	id: string;
	name: string;
	isReady: boolean;
	// neighbors: { [id: string]: string }; // <position key, peerId>
}

export interface ITask {
	id: string;
	name: string;
	atomsName: string;
	programName: string;
	atoms: Atoms;
	program: lunaLang.Program;
}

export interface IEdge {
	fromId: string; 
	fromKey: string;
	toId: string;
	toKey: string;
}

export interface ITopology {
	key: string;
	peers: IPeer[];
	edges: IEdge[];
}

export function getPeerAtKey(topology: ITopology, peerId: string, key: string): IPeer | undefined {
	const peer = topology.peers.find(p => p.id === peerId);
	if (!peer) throw new Error("Peer not found");
	const edge = topology.edges.find(e => 
		e.fromId === peerId && e.fromKey === key
		|| e.toId === peerId && e.toKey === key);
	if (!edge) return undefined;
	const peerAtKeyId = edge.fromId === peerId ? edge.toId : edge.fromId;
	const res = topology.peers.find(p => p.id === peerAtKeyId);
	if (!res) throw new Error("Has edge to peer but no peer found itself!");
	return res;
}

export type LobbyStatus = 'NOTREADY' | 'READY' |'RUNNING' | 'FINISHED'

export interface ILobby {
	id: string;
	changeIndex: number;
    name: string;
	maxUsersCount: number | null;

	userCount: number;
	leaderId: string | null;

	task: ITask;
	topology: ITopology;
	status: LobbyStatus
}