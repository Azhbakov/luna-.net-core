import {lunaLang} from "./lunaLang";
import { IDf, IFragmentId, isIFragmentId } from "./cf";
import { DfStorageAccessor } from "../runtime/worker";

export interface IArgument {
	type: 'iconst' | 'rconst' | 'sconst' | 'id' | lunaLang.ExpressionType | lunaLang.CastType
	value: any
}
export function isIArgument(o: any): o is IArgument {
	return ['iconst', 'rconst', 'sconst', 'id'].find(t => t === o.type) !== undefined;
}

export interface IConstArgument {
	type: 'iconst' | 'rconst' | 'sconst'
	value: any
}
export function isIConstArgument(o: any): o is IArgument {
	return ['iconst', 'rconst', 'sconst'].find(t => t === o.type) !== undefined;
}

export interface IIntConst extends IArgument {
	type: 'iconst'
	value: number
}
export function isIIntConst(o: any): o is IIntConst {
	return o.type === 'iconst' && typeof o.value === 'number'
}

export interface IRealConst extends IArgument {
	type: 'rconst', value: number
}
export function isIRealConst(o: any): o is IRealConst {
	return o.type === 'rconst' && typeof o.value === 'number'
}

export interface IStringConst extends IArgument {
	type: 'sconst', value: string
}
export function isIStringConst(o: any): o is IStringConst {
	return o.type === 'sconst' && typeof o.value === 'string'
}

export interface IDfArgument extends IArgument { // TODO methods in Executor
	type: 'id'
	id: IFragmentId
	value: any
}
export function isIDfArgument(o: any): o is IDfArgument {
	return o.type === 'id' && o.id && isIFragmentId(o.id);
}

export type ArgumentValue = lunaLang.ConstArgumentValue | IDf

export interface IExpressionArgument extends IArgument {
	type: lunaLang.ExpressionType
	operands: IArgument[]
	value: any
}

export function isIExpressionArgument(o: any): o is IExpressionArgument {
	return o.type &&
		['+', '-', '*', '/', '%', '<', '>', '&&'].find(t => t === o.type) !== undefined &&
		o.operands;
}

export interface ICastArgument extends IArgument {
	type: lunaLang.CastType
	expr: IArgument
	value: any
}

export function isICastArgument(o: any): o is ICastArgument {
	return o.type &&
		['icast'].find(t => t === o.type) !== undefined && o.expr;
}

export const argumentMethods = {
	checkResolved: (arg: IArgument, parameter: lunaLang.Parameter, dfStorageAccessor: DfStorageAccessor): boolean => {
		switch (arg.type) {
			case 'iconst':
			case 'rconst':
			case 'sconst':
				return true;
			case 'id':
				const dfArg = arg as IDfArgument;

				// If DF has unresolved indicies it is definetely not resolved.
				if (dfArg.id.indicies.some(indArg => !argumentMethods.checkResolved(indArg, { type: 'int' }, dfStorageAccessor))) {
					return false;
				}

				switch (parameter.type) {
					case 'int':
					case 'real':
					case 'string':
					case 'value':
						if (dfArg.value !== undefined) return true;
						// If DF argument has no value written, try to get it from DF storage by ID
						const nullableValue = dfStorageAccessor(dfArg.id);
						dfArg.value = nullableValue !== null ? nullableValue.value : undefined;
						return dfArg.value !== undefined;
					case 'name':
						//dfArg.value = fragmentIdMethods.toString(dfArg.id);
						return true;
					default:
						throw new Error(`Parameter type not implemented: ${parameter.type}`);
				}
			case "*": case "/": case "+": case "-": case "%": case "<": case ">": case "&&":
				const exprArg = arg as IExpressionArgument;
				if (exprArg.operands.some(operandArg => !argumentMethods.checkResolved(operandArg, { type: "value" }, dfStorageAccessor))) {
					return false;
				}

				if (exprArg.value !== undefined) return true;
				const op1 = argumentMethods.getValue(exprArg.operands[0]);
				const op2 = argumentMethods.getValue(exprArg.operands[1]);
				switch (exprArg.type) {
					case "*": exprArg.value = (op1 as number) * (op2 as number); break;
					case "/": exprArg.value = (op1 as number) / (op2 as number); break;
					case "+": exprArg.value = (op1 as number) + (op2 as number); break;
					case "-": exprArg.value = (op1 as number) - (op2 as number); break;
					case "%": exprArg.value = (op1 as number) % (op2 as number); break;
					case "<": exprArg.value = (op1 as number) < (op2 as number); break;
					case ">": exprArg.value = (op1 as number) > (op2 as number); break;
					case "&&": exprArg.value = (op1 as boolean) && (op2 as boolean); break;
					default:
						throw new Error(`Operation not implemented: ${arg.type}`);
				}
				return true;

			case 'icast':
				const castArg = arg as ICastArgument;
				if (castArg.value !== undefined) return true;

				if (!argumentMethods.checkResolved(castArg.expr, { type: "int" }, dfStorageAccessor)) 
					return false;
				const val = argumentMethods.getValue(castArg.expr);
				let asNumber;
				switch (typeof val) {
					case 'number': asNumber = val; break;
					case 'string': asNumber = parseInt(val as string, 10); break;
					default: asNumber = val ? 1 : 0; break;
				}
				castArg.value = asNumber;
				return true;

			default:
				throw new Error(`Argument type not implemented: ${arg.type}`);
		}
	},

	getValue: (arg: IArgument): ArgumentValue => {
		switch (arg.type) {
			case 'iconst': return (arg as IIntConst).value;
			case 'rconst': return (arg as IRealConst).value;
			case 'sconst': return (arg as IStringConst).value;
			case 'id': 
			case "*": case "/": case "+": case "-": case "%": case "<": case ">": case "&&":
				if (arg.value === undefined) throw new Error("Trying to access argument value which haven't been produced yet.");
				return arg.value;
			case 'icast': return (arg as ICastArgument).value;
			default:
				throw new Error(`Argument type not implemented: ${arg.type}`);
		}
	}
}