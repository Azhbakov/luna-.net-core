import { IArgument, argumentMethods } from "./arguments";
import { DfStorageAccessor } from "../runtime/worker";
import { lunaLang } from "./lunaLang";

export interface IFragmentId {
	main: string
	indicies: IArgument[]
}
export function isIFragmentId(o: any): o is IFragmentId {
	return o.main && o.indicies
}
export const fragmentIdMethods = {
	toString: (id: IFragmentId): string => {
		return [id.main].concat(id.indicies.map(ind => {
			return argumentMethods.getValue(ind) as string;
		})).join('.');
	},
	equals: (first: IFragmentId, second: IFragmentId): boolean => {
		return fragmentIdMethods.toString(first) === fragmentIdMethods.toString(second);
	}
}


export interface IDf {
	id: IFragmentId
	value: any
}

export interface IExecutionResult {
	cfs: ICf[]
	dfs: IDf[]
}

export interface IContextVariable {
	localName: string
	value: IArgument
}
export function IsContextVariable(o: any): o is IContextVariable {
	return o.localName && o.value
}

export interface IExecutionContext {
	variables: IContextVariable[],
	parentCfId: IFragmentId
}

export type Atoms = {[name: string]: any};

export interface IProgram {
	source: lunaLang.Program,
	atoms: Atoms
}

// All dependency trees start with cf and end with df and have args in the middle.

// Cf execution is it's body execution - generating all of it's child fragments.
// We can start body execution only when all the dependencies of current cf have been evaluated.
// Cf creation means passing all the dependencies to new cf's context. 
// IMPORTANT: cf creation is always possible because since we are executing body all of current dependecies have been resolved
// and values can be passed to new context.

// Workflow:
// 1. Get main subroutine, empty context
// 2. Execute main subroutine's body:
// 	2.1 Get operator, give him current context, let him fork it and produce cf

// In operator's creator:
// You have extern context.
// 1. Add your parameters (inner names and extern values (it must be evaluated in extern context)) to your context.
// 2. Look at your arguments (and id index arguments), ask every argument what it needs from current extern context
// and let them save it in their contexts. They will be evaluated later, when resulting cf will be executed.

// In operator's executor:
// 1. Look at your arguments and at your parameters (in prototype).
// 2. Depending on corresponding parameter (?) try to evaluate each argument.
export interface ICf {
	type: 'exec' | 'for' | 'while'
	id: IFragmentId
}

export interface ICfProcessor<T extends ICf, D extends lunaLang.Operator> {
	// Instantiate new CF using parent context to locate dependencies
	interpretDeclaration: (declaration: D, parentContext: IExecutionContext, program: IProgram) => T
	
	// Check if CF has all dependencies calculated (they are already known as CF exists)
	checkResolved: (cf: T, dfStorageAccessor: DfStorageAccessor) => boolean

	// Create new context and execute body/atom
	execute: (cf: T, program: IProgram) => IExecutionResult // no context parameter is needed to execute cf, if it exists it already includes all dependencies
}