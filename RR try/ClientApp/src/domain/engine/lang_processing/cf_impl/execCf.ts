import { lunaLang } from "../lunaLang";
import { interpretArgumentDecl, executeBody } from "../factories";
import { IArgument, argumentMethods, IDfArgument } from "../arguments";
import { ICf, ICfProcessor, IExecutionContext, IExecutionResult, IFragmentId, IProgram, fragmentIdMethods } from "../cf";
import { DfStorageAccessor } from "../../runtime/worker";


/// EXEC
export interface IExecCf extends ICf {
	type: 'exec'
	prototypeName: string
	prototype: lunaLang.Subroutine | lunaLang.Atom
	args: IArgument[]
}

export const execCfProcessor: ICfProcessor<IExecCf, lunaLang.IExec> = {

	interpretDeclaration: (declaration: lunaLang.IExec, parentContext: IExecutionContext, program: IProgram): IExecCf => {
		/// Iterate over all arguments (id and args), ask them to create their contexts using parent context
		///	Create own context and fill it with parameter names from prototype.
		/// ARGUMENTS DEAL WITH THEIR OWN CONTEXT AND DONT AFFECT CONTEXT OF NEW CF
		const idMain = declaration.id[0] as string;
		const idParts = declaration.id.filter((idPart, i) => i > 0).map(idPartDecl => {
			return interpretArgumentDecl(idPartDecl as lunaLang.Argument, parentContext);
		});
		const id = { 
			main: fragmentIdMethods.toString(parentContext.parentCfId) + '.' + idMain, 
			indicies: idParts
		} as IFragmentId;

		const args = declaration.args.map(argDecl => {
			return interpretArgumentDecl(argDecl, parentContext);
		});
	
		const prototype = program.source[declaration.code] as lunaLang.Subroutine;
		if (!prototype) throw new Error(`Prototype for call to \"${declaration.code}\" not resolved.`);
	
		return { 
			type: 'exec',
			id,
			args,
			prototypeName: declaration.code,
			prototype
		} as IExecCf
	},

	checkResolved: (exec: IExecCf, dfStorageAccessor: DfStorageAccessor): boolean => {
		return !exec.args
			.some((arg, i) => !argumentMethods.checkResolved(arg, exec.prototype.args[i], dfStorageAccessor)) 
			&& 
			!exec.id.indicies.some(arg => !argumentMethods.checkResolved(arg, { type: 'int' }, dfStorageAccessor));
	},

	execute: (cf: IExecCf, program: IProgram): IExecutionResult => {
		if (cf.prototype.type === 'extern') {
			const atom = program.atoms[cf.prototype.code];
			if (!atom) throw new Error(`Atom ${cf.prototype.code} not found.`);

			const args = cf.args.map((arg, i) => cf.prototype.args[i].type === 'name' ? arg : argumentMethods.getValue(arg));
			const res = atom(...args);
			const resDfs = cf.args.filter((arg, i) => cf.prototype.args[i].type === 'name');
			if (!resDfs.every(arg => arg.type === 'id')) throw new Error("All 'name' args are expected to be DFs.")

			return { cfs: [], dfs: resDfs as IDfArgument[] };
		}

		const newContextVars = cf.prototype.args.map((param, i) => {
			return { localName: param.id, value: cf.args[i] };
		});
		const currentContext = { variables: newContextVars, parentCfId: cf.id };
		
		return executeBody(cf.prototype.body, currentContext, program);
	}

}