import { ICf, ICfProcessor, IExecutionContext, IProgram, IExecutionResult, fragmentIdMethods, IFragmentId, IContextVariable, IsContextVariable } from "../cf";
import { lunaLang } from "../lunaLang";
import { argumentMethods, IConstArgument, IDfArgument, IIntConst } from "../arguments";
import { DfStorageAccessor } from "../../runtime/worker";
import { interpretArgumentDecl, executeBody, checkBodyResolved } from "../factories";

export interface IForCf extends ICf {
	type: 'for'
	first: IConstArgument | IDfArgument,
	last: IConstArgument | IDfArgument,
	var: string,
	body: lunaLang.IBody,
	parentScopeVariables: IContextVariable[]
}

export const forCfProcessor: ICfProcessor<IForCf, lunaLang.IFor> = {
	// Instantiate new CF using parent context to locate dependencies
	interpretDeclaration: (declaration: lunaLang.IFor, parentContext: IExecutionContext, program: IProgram): IForCf => {
		const id = { 
			main: fragmentIdMethods.toString(parentContext.parentCfId) + '.' + 'for', 
			indicies: []
		} as IFragmentId;

		return {
			type: 'for',
			id,
			first: interpretArgumentDecl(declaration.first, parentContext),
			last: interpretArgumentDecl(declaration.last, parentContext),
			var: declaration.var,
			body: declaration.body,
			parentScopeVariables: parentContext.variables
		} as IForCf
	},
	
	// Check if CF has all dependencies calculated (they are already known as CF exists)
	checkResolved: (forCf: IForCf, dfStorageAccessor: DfStorageAccessor): boolean => {
		// FIX FOR MISSING VALUES FOR DESERIALIZED FOR CFS
		// forCf.parentScopeVariables.forEach(v => 
		// 	argumentMethods.checkResolved(v.value, { type: 'value' }, dfStorageAccessor));

		return argumentMethods.checkResolved(forCf.first, { type: 'int' }, dfStorageAccessor) 
			&& argumentMethods.checkResolved(forCf.last, { type: 'int' }, dfStorageAccessor)
			&& checkBodyResolved(forCf.body, { variables: contextVarsWithCounter(forCf, 0) } as IExecutionContext, dfStorageAccessor); // counter value is not important for resolve checking
	},

	// Create new context and execute body/atom
	execute: (forCf: IForCf, program: IProgram): IExecutionResult => { // no context parameter is needed to execute cf, if it exists it already includes all dependencies
		const res: IExecutionResult = { cfs: [], dfs: [] };
		for (let i = forCf.first.value; i <= forCf.last.value; i++) {
			const newContextVars = contextVarsWithCounter(forCf, i);
			const currentContext = { 
				variables: newContextVars, 
				parentCfId: { main: `${forCf.id.main}.${i}`, indicies: forCf.id.indicies }
			};

			const cycleRes = executeBody(forCf.body, currentContext, program);
			res.cfs.push(...cycleRes.cfs);
			res.dfs.push(...cycleRes.dfs);
		}

		return res;
	}
}

function contextVarsWithCounter(forCf: IForCf, counterValue: number): IContextVariable[] {
	return [ ...forCf.parentScopeVariables, { localName: forCf.var, value: { type: 'iconst', value: counterValue } as IIntConst } ];
}