import { ICf, ICfProcessor, IExecutionContext, IProgram, IExecutionResult, fragmentIdMethods, IFragmentId, IContextVariable, IDf } from "../cf";
import { lunaLang } from "../lunaLang";
import { argumentMethods, IConstArgument, IDfArgument, IIntConst, IExpressionArgument } from "../arguments";
import { DfStorageAccessor } from "../../runtime/worker";
import { interpretArgumentDecl, executeBody, checkBodyResolved } from "../factories";

export interface IWhileCf extends ICf {
	type: 'while'
	start: IConstArgument | IDfArgument,
	condition: IExpressionArgument,
	wout: IDfArgument,

	var: string,
	body: lunaLang.IBody,
	// Interpretation of declaration embeds counter as value in condition and wout, it cannot be found later. Bring original declarations to later create next While CF with new counter.
	originalCondition: lunaLang.Argument,
	originalWout: lunaLang.IDfArgument,
	parentContextWithCounter: IExecutionContext
}

export const whileCfProcessor: ICfProcessor<IWhileCf, lunaLang.IWhile> = {
	// Instantiate new CF using parent context to locate dependencies
	interpretDeclaration: (declaration: lunaLang.IWhile, parentContext: IExecutionContext, program: IProgram): IWhileCf => {
		const id = { 
			main: fragmentIdMethods.toString(parentContext.parentCfId) + '.' + 'while', 
			indicies: []
		} as IFragmentId;

		const counterDfArg = interpretArgumentDecl(declaration.start, parentContext)

		const parentContextWithCounter = {
			...parentContext,
			variables: [
				...parentContext.variables, { // counter must be in the context for checkResolved to be able to resolve counter references between execution
					localName: declaration.var,
					value: counterDfArg
				} as IContextVariable
			]
		};

		return {
			type: 'while',
			id,
			start: interpretArgumentDecl(declaration.start, parentContext),
			condition: interpretArgumentDecl(declaration.cond, parentContextWithCounter) as IExpressionArgument,
			wout: interpretArgumentDecl(declaration.wout, parentContextWithCounter) as IDfArgument,

			var: declaration.var,
			body: declaration.body,
			originalCondition: declaration.cond,
			originalWout: declaration.wout,
			parentContextWithCounter
		} as IWhileCf
	},
	
	// Check if CF has all dependencies calculated (they are already known as CF exists)
	// TODO: CF id indicies in while body should also be resolved?!
	checkResolved: (whileCf: IWhileCf, dfStorageAccessor: DfStorageAccessor): boolean => {
		// FIX FOR MISSING VALUES FOR DESERIALIZED WHILE CFS
		// whileCf.parentContextWithCounter.variables.forEach(v => 
		// 	argumentMethods.checkResolved(v.value, { type: 'value' }, dfStorageAccessor));
		return argumentMethods.checkResolved(whileCf.start, { type: 'int' }, dfStorageAccessor) 
			&& argumentMethods.checkResolved(whileCf.wout, { type: 'name' }, dfStorageAccessor)
			&& argumentMethods.checkResolved(whileCf.condition, { type: 'value' }, dfStorageAccessor)
			&& checkBodyResolved(whileCf.body, whileCf.parentContextWithCounter, dfStorageAccessor);
	},

	// Create new context and execute body/atom
	execute: (whileCf: IWhileCf, program: IProgram): IExecutionResult => { // no context parameter is needed to execute cf, if it exists it already includes all dependencies
		const counterVar = whileCf.parentContextWithCounter.variables.find(v => v.localName === whileCf.var);
		if (counterVar === undefined) throw new Error('Expected to have counter DF in while context.');

		const counterValue = argumentMethods.getValue(counterVar.value) as number;
		
		if (!argumentMethods.getValue(whileCf.condition)) {
			return { cfs: [], dfs: [{
				id: whileCf.wout.id,
				value: counterValue
			} as IDf]}
		}

		const res: IExecutionResult = { cfs: [], dfs: [] };

		// Execute current loop body
		const loopRes = executeBody(whileCf.body, whileCf.parentContextWithCounter, program);
		res.cfs.push(...loopRes.cfs);
		res.dfs.push(...loopRes.dfs);

		// Create new While CF with incremented counter
		const newCounterValue = counterValue + 1;

		const contextWithIncrementedCounter = {
			...whileCf.parentContextWithCounter,
			variables: [
				...whileCf.parentContextWithCounter.variables.filter(v => v.localName !== whileCf.var), 
				{ 
					localName: whileCf.var, 
					value: {
						type: 'iconst',
						value: newCounterValue
					} as IIntConst
				} as IContextVariable
			]
		}
		
		const newWhileCf = {
			...whileCf,			
			id: {
				main: whileCf.id.main,
				indicies: [ { type: 'iconst', value: newCounterValue } as IIntConst]
			} as IFragmentId,
	
			condition: interpretArgumentDecl(whileCf.originalCondition, contextWithIncrementedCounter) as IExpressionArgument,
			wout: interpretArgumentDecl(whileCf.originalWout, contextWithIncrementedCounter) as IDfArgument,
			parentContextWithCounter: contextWithIncrementedCounter,
		} as IWhileCf

		res.cfs.push(newWhileCf);

		return res;
	}
}