import { execCfProcessor, IExecCf } from "./cf_impl/execCf";
import { lunaLang } from "./lunaLang";
import { IArgument, IIntConst, IRealConst, IStringConst, IDfArgument, IExpressionArgument, ICastArgument, argumentMethods } from "./arguments";
import { IExecutionResult, IExecutionContext, ICf, fragmentIdMethods, IProgram } from "./cf";
import { DfStorageAccessor } from "./../runtime/worker";
import { forCfProcessor, IForCf } from "./cf_impl/forCf";
import { whileCfProcessor, IWhileCf } from "./cf_impl/whileCf";

export function interpretArgumentDecl(argDecl: lunaLang.Argument, parentContext: IExecutionContext): IArgument {
	switch (argDecl.type) {
		case "iconst":
			return { type: 'iconst', value: (argDecl as lunaLang.IIntConst).value } as IIntConst;
		case "rconst":
			return { type: 'rconst', value: (argDecl as lunaLang.IRealConst).value } as IRealConst
		case "sconst":
			return { type: 'sconst', value: (argDecl as lunaLang.IStringConst).value } as IStringConst
		case "id":
			const prototype = argDecl as lunaLang.IDfArgument;
			const id = {
				main: prototype.ref[0] as string,
				indicies: prototype.ref.filter((idPart, i) => i > 0).map(idPart => interpretArgumentDecl(idPart as lunaLang.Argument, parentContext))
			};
			const matchingVariable = parentContext.variables.find(variable => variable.localName === id.main); // indicies always mean creating new DF, doesn't affect value 
			if (!matchingVariable) throw new Error(`Failed to resolve DF reference ${id.main} with current parent context. Make sure you are executing resolved CF.`);
			if (id.indicies.length !== 0 && matchingVariable.value.type !== 'id') throw new Error("Argument is passed with additional indicies but it is not DF");

			const resultingArg = id.indicies.length === 0 ? matchingVariable.value 
				: { // append new indicies 
					... matchingVariable.value,
					id: {
						main: (matchingVariable.value as IDfArgument).id.main,
						indicies: (matchingVariable.value as IDfArgument).id.indicies.concat(id.indicies)
					},
					value: undefined
				} as IDfArgument

			return resultingArg;
		case "*": case "/": case "+": case "-": case "%": case "<": case ">": case "&&":
			return {
				type: argDecl.type,
				operands: (argDecl as lunaLang.Expression).operands.map(arg => interpretArgumentDecl(arg, parentContext)),
				value: undefined
			} as IExpressionArgument
		case 'icast':
			const cast = argDecl as lunaLang.ICast;
			return {
				type: argDecl.type,
				expr: interpretArgumentDecl(cast.expr, parentContext)
			} as ICastArgument
		default:
			throw new Error(`Argument processing not implemented for ${argDecl.type}`);
	}
}

export function interpretCfDecl(cfDecl: lunaLang.Operator, parentContext: IExecutionContext, program: IProgram): ICf {
	switch (cfDecl.type) {
		case "exec":
			return execCfProcessor.interpretDeclaration(cfDecl as lunaLang.IExec, parentContext, program);
		case "for":
			return forCfProcessor.interpretDeclaration(cfDecl as lunaLang.IFor, parentContext, program);
		case "while":
			return whileCfProcessor.interpretDeclaration(cfDecl as lunaLang.IWhile, parentContext, program);
		default:
			throw new Error(`Operator not implemented ${cfDecl.type}`);
	}
}

export function executeCf(cf: ICf, program: IProgram): IExecutionResult {
	switch (cf.type) {
		case "exec":
			return execCfProcessor.execute(cf as IExecCf, program);
		case "for":
			return forCfProcessor.execute(cf as IForCf, program);
		case "while":
			return whileCfProcessor.execute(cf as IWhileCf, program);
		default:
			throw new Error(`Operator not implemented ${cf.type}`);
	}
}

export function checkCfResolved(cf: ICf, dfStorageAccessor: DfStorageAccessor): boolean {
	switch (cf.type) {
		case "exec":
			return execCfProcessor.checkResolved(cf as IExecCf, dfStorageAccessor);
		case "for":
			return forCfProcessor.checkResolved(cf as IForCf, dfStorageAccessor);
		case "while":
			return whileCfProcessor.checkResolved(cf as IWhileCf, dfStorageAccessor);
		default:
			throw new Error(`Operator not implemented ${cf.type}`);
	}
}

export function executeBody(body: lunaLang.IBody, context: IExecutionContext, program: IProgram): IExecutionResult {
	const decl = body[0].type === 'dfs' ? body[0] : null;

	if (decl) {
		(decl as lunaLang.IDfDeclaration).names.forEach(name => {
			const newDfArg = { 
				type: 'id',
				id: {
					main: fragmentIdMethods.toString(context.parentCfId) + '.' + name,
					indicies: []
				},
				value: undefined
			} as IDfArgument
			context.variables.push({ localName: name, value: newDfArg });
		})
	}

	const newCfs = body.filter((_, i) => !decl || i > 0).map(operator => {
		return interpretCfDecl(operator, context, program);
	});

	newCfs.filter(cf => cf.type ==='for').forEach((cf, i) => cf.id.main += `.${i}`)
	newCfs.filter(cf => cf.type ==='while').forEach((cf, i) => cf.id.main += `.${i}`)  

	return { 
		dfs: [],
		cfs: newCfs
	};
}

// To make sure cf id's in bodies of For and While are resolved
export function checkBodyResolved(body: lunaLang.IBody, context: IExecutionContext, dfStorageAccessor: DfStorageAccessor): boolean {
	return body.filter(o => o.type === 'exec').every(e => 
		(e as lunaLang.IExec).id.filter((idPart, i) => i > 0)
			.every(idPart => argumentMethods.checkResolved(
				interpretArgumentDecl(idPart as lunaLang.Argument, context),
				{ type: 'value' } as lunaLang.Parameter, 
				dfStorageAccessor)));
}