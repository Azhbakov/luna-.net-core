export namespace lunaLang {

export interface Parameter {
	type: 'int' | 'real' | 'string' | 'value' | 'name'
}
export function IsParameter(o: any): o is Parameter {
	return o.type && 
		(o.type === 'int' || o.type === 'real' || o.type === 'string' || o.type === 'value' || o.type === 'name');
}

export interface NamedParameter extends Parameter {
	id: string
}
export function IsNamedParameter(o: any): o is NamedParameter {
	return o.id && IsParameter(o);
}

// exec == argument passing
// for, while, if = declaration & parameter passing

export type DeclarationStatement = Atom | Subroutine
export function IsDeclarationStatement(o: any): o is DeclarationStatement {
	return isAtom(o) || IsSubroutine(o);
}

export type Program = {[prototypeName: string]: DeclarationStatement};
export function IsProgram(o: any): o is Program {
	return !o.some((key: string) => !IsDeclarationStatement(o[key]));
}

export interface Subroutine  {
	type: 'struct'
	args: NamedParameter[]
	body: IBody
}
export function IsSubroutine(o: any): o is Subroutine {
	return o.type && o.type === 'struct' &&
		o.args && !o.args.some((a: any) => !IsNamedParameter(a)) &&
		o.body && IsBody(o.body);
}

export interface Atom {
	type: 'extern'
	args: Parameter[]
	code: string
}
export function isAtom(o: any): o is Atom {
	return o.type && o.type === 'extern'
		&& o.args && o.code
}

export type IBody = Operator[];
export function IsBody(o: any): o is IBody {
	return !o.some((oo: any) => !IsOperator(oo));
}

export interface Operator {
	type: 'dfs' | 'exec' | 'for' | 'while' | 'if'
}
export function IsOperator(o: any): o is Operator {
	return o.type && ['dfs', 'exec', 'for', 'while', 'if'].find(t => t === o.type) !== undefined;
}

export interface IDfDeclaration extends Operator {
	type: 'dfs'
	names: string[]
}
export function IsIDfs(o: any): o is IDfDeclaration {
	return o.type && o.type ==='dfs' && o.names;
}

export interface IExec extends Operator {
	type: 'exec'
	code: string
	args: Argument[]
	id: (string | Argument)[]
}
export function isIExec(o: any): o is IExec {
	return o.type && o.type === 'exec'
		&& o.code && o.args && o.id
}

export interface IFor extends Operator {
	type: 'for'
	var: string
	first: Argument
	last: Argument
	body: IBody
}
export function IsIFor(o: any): o is IFor {
	return o.type ==='for' && o.var && 
		o.first && IsArgument(o.first) &&  o.last && IsArgument(o.last) && 
		o.body && IsBody(o.body);
}

export interface IWhile extends Operator {
	type: 'while'
	var: string
	start: Argument
	cond: Argument
	wout: IDfArgument
	body: IBody
}
export function IsIWhile(o: any): o is IWhile {
	return o.type ==='while' && o.var && 
		o.start && IsArgument(o.start) &&  o.cond && IsArgument(o.cond) && 
		o.wout && IsIDfArgument(o.wout);
}

export interface IIf extends Operator {
	type: 'if'
	// cond: 
	body: IBody
}
export function IsIIf(o: any): o is IIf {
	return o.type ==='if' && o.body && IsBody(o.body);
}

export interface Argument {
	type: IConstArgumentType | ExpressionType | 'id' | CastType
};
export function IsArgument(o: any): o is Argument {
	return IsIDfArgument(o) || IsIConstArgument(o) || IsIExpression(o);
}

export type IConstArgumentType = 'iconst' | 'rconst' | 'sconst'

export interface IConstArgument {
	type: IConstArgumentType
	value: any
}
export function IsIConstArgument(o: any): o is IConstArgument {
	return o.type && ['iconst', 'rconst', 'sconst'].find(t => t === o.type) !== undefined;
}

export type ConstArgumentValue = number | string | boolean;

interface ConstArgument<T extends ConstArgumentValue> extends IConstArgument {
	value: T
}

export interface IIntConst extends ConstArgument<number> {
	type: 'iconst'
}
export function IsIIntConst(o: any): o is IIntConst { return o.type === 'iconst';}

export interface IRealConst extends ConstArgument<number> {
	type: 'rconst'
}
export function IsIRealConst(o: any): o is IRealConst { return o.type === 'rconst'; }

export interface IStringConst extends ConstArgument<string> {
	type: 'sconst'
}
export function IsIStringConst(o: any): o is IStringConst { return o.type === 'sconst'; }

export interface IDfArgument {
	type: 'id'
	ref: (string | Argument)[] 
}
export function IsIDfArgument(o: any): o is IDfArgument { 
	return o.type === 'id' && o.ref; 
}

export type ExpressionType = '+' | '-' | '*' | '/' | '%' | '<' | '>' | '&&'

export interface Expression {
	type: ExpressionType
	operands: Argument[]
}
export function IsIExpression(o: any): o is Expression {
	return o.type &&
		['+', '-', '*', '/', '%', '<', '>', '&&'].find(t => t === o.type) !== undefined &&
		o.operands;
}

export type CastType = 'icast';

export interface ICast {
	type: CastType
	expr: Argument
}

export function IsICast(o: any): o is ICast {
	return o.type &&
		['icast'].find(t => t === o.type) !== undefined &&
		o.expr;
}

export function parseDeclarationStatement(o: any): DeclarationStatement {
	if (!IsDeclarationStatement(o)) throw new Error("Failed to parse declaration statement.");
	const statement = o as DeclarationStatement;
	switch (statement.type) {
		case 'struct':
			return o as Subroutine;
		case 'extern':
			return o as Atom;
		default:
			throw new Error("Failed to parse declaration statement");
	}
}

export function parseProgram(o: any): Program {
	if (!IsProgram(o)) throw new Error("Failed to parse program.");
	const program = o as Program;
	const parsedProgram: Program = {};

	Object.keys(program).forEach((declName: string) => {
		const statement = program[declName] as DeclarationStatement;
		parsedProgram[declName] = parseDeclarationStatement(statement);
	});
	return program;
}
}