import { IWorker } from "./worker";
import { ICommModule, IMessagePayload } from "./commModule";
import { CallbackControl, CallbackStore } from "src/utils/callbackControl";
import { ITopology } from "src/domain/Lobby";
import { IDirection } from "./simplePathfinder";

export interface FinishDetails {
	time: number
}

export interface BallStopContext {
	selfId: string;
	topology: ITopology;
}

export const BallMessageTypes = {
	CHECK: 'CHECK BALL',
	STOP: 'STOP BALL',
}

class CheckBallMessage implements IMessagePayload {
	public type: string = BallMessageTypes.CHECK;
	public idleHops: number;
	public lastFinishTime: number;
}

class StopBallMessage implements IMessagePayload {
	public type: string = BallMessageTypes.STOP;
	public lastFinishTime: number;
}

export class BallStop {
	private _commModule: ICommModule;
	private _context: BallStopContext;

	private _status: 'IDLE' | 'BUSY' | 'STOPPED';
	private _ballLaunched = false;

	private _finishCallback: CallbackControl<FinishDetails> = new CallbackControl<FinishDetails>();
	public get onFinish(): CallbackStore<FinishDetails> { return this._finishCallback; }

	private _releaseHoldBall: () => void;

	public constructor(worker: IWorker, commModule: ICommModule, context: BallStopContext) {
		this._commModule = commModule;
		if (context.topology.key !== 'LineTopology') throw new Error("Only line topology is supported by BallStop.")
		this._context = context;

		const th = this;

		const isDegenerateLine = !this.hasPeerInDirection(this._context, 'left') && !this.hasPeerInDirection(this._context, 'right');

		if (isDegenerateLine) {
			worker.onIdleStart.addCallback(idleDetails => {
				if (idleDetails.type !== 'WAITING CFS') return;
				th._finishCallback.fire({ time: performance.now() });
			});
			return;
		}

		worker.onIdleStart.addCallback(idleDetails => {
			if (idleDetails.type !== 'WAITING CFS') return;
			th._status = 'IDLE';

			if (th._releaseHoldBall) th._releaseHoldBall();
			
			// Launch ball
			if (th.isFirstInLine(context) && !th._ballLaunched) {
				th._ballLaunched = true;
				const ball = new CheckBallMessage();
				ball.lastFinishTime = performance.now();
				th._commModule.send(ball, { type: 'DIRECTION', direction: 'right' } as IDirection);
			}
		});

		worker.onIdleFinish.addCallback(idleDetails => {
			th._status = 'BUSY';
		});

		// On check ball received
		this._commModule.addReceiveCallback((peerId, message) => {
			if (message.type !== BallMessageTypes.CHECK) return;
			const ball = (message as CheckBallMessage);
			console.log(ball.lastFinishTime);

			if (th._status === 'IDLE' && th.isFirstInLine(th._context) && (message as CheckBallMessage).idleHops >= 2 * th._context.topology.peers.length) {
				th._finishCallback.fire({ time: performance.now() });
				const stopBall = new StopBallMessage();
				stopBall.lastFinishTime = ball.lastFinishTime;
				th._commModule.send(stopBall, { type: 'DIRECTION', direction: 'right' } as IDirection);
			} else {
				if (th._status === 'IDLE') {
					th.passBall(ball, peerId);
				} else {
					new Promise(resolve => { // hold ball until idle
						console.log("holding")
						th._releaseHoldBall = resolve;
					}).then(() => {
						console.log("releasing")
						ball.idleHops = 0;
						ball.lastFinishTime = performance.now();
						th.passBall(ball, peerId);
					});
				}
			}
		});

		// Process STOP BALL
		this._commModule.addReceiveCallback((peerId, message) => {
			if (message.type !== BallMessageTypes.STOP) return;
			th._finishCallback.fire({ time: (message as StopBallMessage).lastFinishTime });

			if (!th.isLastInLine(th._context)) th._commModule.send(message, { type: 'DIRECTION', direction: 'right' } as IDirection)
		});
	}

	private hasPeerInDirection(context: BallStopContext, direction: string): boolean {
		return context.topology.edges.some(e => e.toId === context.selfId && e.toKey === direction 
			|| e.fromId === context.selfId && e.fromKey === direction);
	}

	private isFirstInLine(context: BallStopContext): boolean {
		return !this.hasPeerInDirection(context, 'left');
	}

	private isLastInLine(context: BallStopContext): boolean {
		return !this.hasPeerInDirection(context, 'right');
	}

	private getPeerDirection(peerId: string, context: BallStopContext): string {
		for (const e of context.topology.edges) {
			if (e.fromId === peerId && e.toId === context.selfId) return e.toKey;
			if (e.fromId === context.selfId && e.toId === peerId) return e.fromKey;
		}
		throw new Error(`Received ball from not neighbor ${peerId}.`);
	}

	private inverseDirection(direction: string): string {
		switch (direction) {
			case 'left': return 'right';
			case 'right': return 'left';
			default: throw new Error(`Unknown direction ${direction}.`);
		}
	}

	private passBall(ball: CheckBallMessage, fromPeerId: string): void {
		ball.idleHops++;
		const nextDirection = this.isLastInLine(this._context) ? 'left' 
			: this.isFirstInLine(this._context) ? 'right' 
			: this.inverseDirection(this.getPeerDirection(fromPeerId, this._context));

		this._commModule.send(ball, { type: 'DIRECTION', direction: nextDirection } as IDirection);
	}
}