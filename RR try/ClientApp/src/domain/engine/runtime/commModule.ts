import { IRemotePath, IPathfinder, IDirection, IPeerPath } from "./simplePathfinder";
import { CallbackStore, CallbackControl } from "src/utils/callbackControl";

export interface IMessage {
	payload: any;
	targetPeerId: string | null;
	sourcePeerId: string;
}

export interface IPeerComm {
	peerId: string;
	direction: string;
	send(message: IMessage): Promise<void>;
	onReceive(callback: (message: IMessage) => void): void;
}

export interface ICommContext {
	selfId: string;
	peerConnections: IPeerComm[];
}

export interface IMessagePayload {
	type: string
}

export type CommMessageHandler = (peerId: string, payload: IMessagePayload) => void;

export interface ICommModule {
	send(payload: IMessagePayload, path: IRemotePath): Promise<void>
	addReceiveCallback(callback: CommMessageHandler): void;
}

export interface IObservalbleCommModule {
	onSend: CallbackStore<{payload: IMessagePayload, path: IRemotePath}>;
	onRecieve: CallbackStore<{peerId: string, payload: IMessagePayload}>;
}

export class CommModule implements ICommModule, IObservalbleCommModule {
	private _context: ICommContext;
	private _callbacks: CommMessageHandler[] = [];
	private _pathfinder: IPathfinder;

	// Observable
	private _sendCallbacks: CallbackControl<{payload: IMessagePayload, path: IRemotePath}> = new CallbackControl<{payload: IMessagePayload, path: IRemotePath}>();
	get onSend(): CallbackStore<{payload: IMessagePayload, path: IRemotePath}> { return this._sendCallbacks; }
	private _recieveCallbacks: CallbackControl<{peerId: string, payload: IMessagePayload}> = new CallbackControl<{peerId: string, payload: IMessagePayload}>();
	get onRecieve(): CallbackStore<{peerId: string, payload: IMessagePayload}> { return this._recieveCallbacks; }

	public constructor(context: ICommContext, pathfinder: IPathfinder) {
		this._context = context;
		this._pathfinder = pathfinder;

		context.peerConnections.forEach(pc => {
			pc.onReceive(message => this.processRecieving(pc.peerId, message));
		}, this);
	}

	private processRecieving(neighborId: string, message: IMessage): void {
		// console.log(`Recieved from ${neighborId}:`);
		// console.log(message);
		if (message.targetPeerId === this._context.selfId) {
			this._callbacks.forEach(cb => cb(message.sourcePeerId, message.payload));
			return;
		}
		const newPath = message.targetPeerId ? 
			this._pathfinder.calculatePath(message.payload, message.targetPeerId) 
			: this._pathfinder.whereTo(message.payload);
		if (newPath.type === 'STAY') {
			this._callbacks.forEach(cb => cb(message.sourcePeerId, message.payload));
			return;
		}
		this.sendImpl(message, newPath as IRemotePath);
	}

	public send(payload: IMessagePayload, path: IRemotePath): Promise<void> {
		const message = { 
			payload, 
			sourcePeerId: this._context.selfId, 
			targetPeerId: path.type === 'PEER' ? (path as IPeerPath).targetPeerId : null 
		} as IMessage;
		return this.sendImpl(message, path);
	}

	private sendImpl(message: IMessage, path: IRemotePath): Promise<void> {
		const direction = (path as IDirection).direction;
		const targetPc = this._context.peerConnections.find(p => p.direction === direction);
		if (!targetPc) throw new Error("No peer in such direction, bad pathfinder.")
		// console.log(`Sending to ${targetPc.peerId} (${targetPc.direction}):`);
		// console.log(message);
		return targetPc.send(message);
	}

	public addReceiveCallback(callback: (peerId: string, payload: IMessagePayload) => void): void {
		this._callbacks.push(callback);
	}
}