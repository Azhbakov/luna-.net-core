import { ICf, IDf, fragmentIdMethods, IFragmentId } from "../lang_processing/cf";
import { IPathfinder, IRemotePath, Payload, IPeerPath } from "./simplePathfinder";
import { ICommModule, IMessagePayload } from "./commModule";
import { LocalStorage } from "./localStorage";
import { CallbackStore } from "src/utils/callbackControl";

export interface IObservableStorage {
	onDfAdded: CallbackStore<IDf>; // called only for CFs which stay on this node
	onCfAdded: CallbackStore<ICf>;
	onCfRemoved: CallbackStore<ICf>

	getCfs(): ICf[];
	getDfs(): IDf[];
}

export interface IFragmentStorage {
	popCf(id: IFragmentId): ICf | null;
	getDf(id: IFragmentId): IDf | null;
	putDf(df: IDf): void;
	putCf(cf: ICf): void;

	onDfAdded: CallbackStore<IDf>; // called only for CFs which stay on this node
	onCfAdded: CallbackStore<ICf>;
}

export class FragmentRequestPayload implements IMessagePayload {
	public type: string = 'DF REQUEST';
	public fragmentId: IFragmentId;
}

export type FragmentPayload = DfPayload | CfPayload;

export class DfPayload implements IMessagePayload {
	public type: string = 'DF';
	public df: IDf;
}
export class CfPayload implements IMessagePayload {
	public type: string = 'CF';
	public cf: ICf;
}

export class DistributedStorage extends LocalStorage {
	private _selfPeerId: string;
	private _pathfinder: IPathfinder;
	private _commModule: ICommModule;
	
	private _dfRequestsFromRemote: { dfId: IFragmentId, awaitingPeerId: string }[] = [];
	private _sentDfRequests: IFragmentId[] = [];

	public constructor(selfPeerId: string, pathfinder: IPathfinder, commModule: ICommModule) {
		super();
		this._selfPeerId = selfPeerId;
		this._pathfinder = pathfinder;
		this._commModule = commModule;

		this._commModule.addReceiveCallback(this.processMessage.bind(this))
	}

	private processMessage(fromPeerId: string, payload: Payload) {
		switch (payload.type) {
			// TODO PROCESS MESSAGES
			case 'DF':
				// console.log(`Recieved ${fragmentIdMethods.toString((payload as IDfPayload).df.id)} from ${fromPeerId}`)
				this.putDf((payload as DfPayload).df);
				break;
			case 'CF':
				this.putCf((payload as CfPayload).cf);
				break;
			case 'DF REQUEST':
				const requestedDfId = (payload as FragmentRequestPayload).fragmentId;
				
				// Check if already have such request
				if (this._dfRequestsFromRemote.find(dfReq => fragmentIdMethods.equals(requestedDfId, dfReq.dfId) && dfReq.awaitingPeerId === fromPeerId)) 
					throw new Error(`DF request for "${requestedDfId}" have already been registered from peer ${fromPeerId}`);
				
				// console.log(`Request for ${fragmentIdMethods.toString(requestedDfId)} from ${fromPeerId}`)

				// Check if DF already exists
				const storedDf = super.getDf(requestedDfId);
				if (storedDf) {
					const dfPayload = { type: 'DF', df: storedDf } as DfPayload;
					// console.log(`Sending ${fragmentIdMethods.toString(requestedDfId)} to ${fromPeerId}`)
					this._commModule.send(
						dfPayload, 
						this._pathfinder.calculatePath(dfPayload, fromPeerId) as IRemotePath);
					break;
				}
				
				// Register request
				this._dfRequestsFromRemote.push({ dfId: requestedDfId, awaitingPeerId: fromPeerId });
				break;
		}
	}

	public putDf(df: IDf): void {
		const dfPayload = { type: 'DF', df } as DfPayload;
		const path = this._pathfinder.whereTo(dfPayload);
		
		if (path.type !== 'STAY') {
			super.putDf(df); // store locally anyway for optimization
			// console.log(`Sending ${fragmentIdMethods.toString(df.id)} to ${(path as IPeerPath).targetPeerId}`)
			this._commModule.send(dfPayload, path as IRemotePath);
			return;
		}
		// If should be stored at this peer
		// console.log(`Saving ${fragmentIdMethods.toString(df.id)}`)
		super.putDf(df);
		// Also check pending requests for this DF from other peers
		let reqInd;
		while ((reqInd = this._dfRequestsFromRemote.findIndex(dfReq => fragmentIdMethods.equals(df.id, dfReq.dfId))) !== -1) {
			const dfReq = this._dfRequestsFromRemote[reqInd];
			// console.log(`Sending ${fragmentIdMethods.toString(df.id)} to ${dfReq.awaitingPeerId}`)
			this._commModule.send(dfPayload, this._pathfinder.calculatePath(dfPayload, dfReq.awaitingPeerId) as IRemotePath);
			this._dfRequestsFromRemote.splice(reqInd, 1);
		}
	}
	public putCf(cf: ICf): void {
		const cfPayload = { type: 'CF', cf } as CfPayload;
		const path = this._pathfinder.whereTo(cfPayload);
		if (path.type !== 'STAY') {
			this._commModule.send(cfPayload, path as IRemotePath);
		} else {
			super.putCf(cf);
		}
	}

	public getDf(id: IFragmentId): IDf | null {
		const res = super.getDf(id);
		if (res) return res;

		const request = { type: 'DF REQUEST', fragmentId: id } as FragmentRequestPayload;
		const path = this._pathfinder.whereTo(request);
		if (path.type === 'STAY') return null;

		if (!this._sentDfRequests.find(dfId => fragmentIdMethods.equals(dfId, id))) {
			// console.log(`Request for ${fragmentIdMethods.toString(id)} to ${(path as IPeerPath).targetPeerId}`)
			this._commModule.send(request, path as IRemotePath);
			this._sentDfRequests.push(id);
		}
		
		return null;
	}
}