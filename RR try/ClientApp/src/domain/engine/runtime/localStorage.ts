import { ICf, IDf, fragmentIdMethods, IFragmentId } from "../lang_processing/cf";
import { IFragmentStorage, IObservableStorage } from "./distributedStorage";
import { CallbackControl, CallbackStore } from "../../../utils/callbackControl";


export class LocalStorage implements IFragmentStorage, IObservableStorage {
	protected _localCfStorage: ICf[] = [];
	protected _localDfStorage: IDf[] = [];

	protected _addCfCallbacks: CallbackControl<ICf> = new CallbackControl<ICf>(); // callback returns true if fragment meets it requirements and callback is no longer needed?
	public get onCfAdded(): CallbackStore<ICf> { return this._addCfCallbacks; }

	protected _addDfCallbacks: CallbackControl<IDf> = new CallbackControl<IDf>();
	public get onDfAdded(): CallbackStore<IDf> { return this._addDfCallbacks; }

	protected _removeCfCallbacks: CallbackControl<ICf> = new CallbackControl<ICf>();
	public get onCfRemoved(): CallbackStore<ICf> { return this._removeCfCallbacks; }


	public putDf(df: IDf): void {
		this._localDfStorage.push(df);
		this._addDfCallbacks.fire(df);
	}
	public putCf(cf: ICf): void {
		this._localCfStorage.push(cf);
		this._addCfCallbacks.fire(cf);
	}

	public popCf(id: IFragmentId): ICf | null {
		const ind = this._localCfStorage.findIndex(cf => fragmentIdMethods.equals(cf.id, id));
		if (ind === -1) return null;
		const res = this._localCfStorage.splice(ind, 1)[0];

		this._removeCfCallbacks.fire(res);
		return res;
	}
	public getDf(id: IFragmentId): IDf | null {
		const res =  this._localDfStorage.find(df => fragmentIdMethods.equals(df.id, id));
		return res ? res : null;
	}

	public getCfs(): ICf[] { return this._localCfStorage; }
	public getDfs(): IDf[] { return this._localDfStorage; }
}