import { lunaLang} from "../lang_processing/lunaLang";
import { Atoms } from "../lang_processing/cf";

export function parseAtoms(text: string): Atoms {
	const o = eval(text);
	if (Object.keys(o).length === 0) throw new Error('Failed to parse atoms.');
	return o as Atoms;
}

export function parseProgram(text: string): lunaLang.Program {
	const o = JSON.parse(text);
	if (Object.keys(o).some(k => !lunaLang.IsDeclarationStatement(o[k]))) throw new Error('Failed to parse atoms.');
	return o as lunaLang.Program;
}