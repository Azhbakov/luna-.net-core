import { DfPayload, CfPayload, FragmentRequestPayload } from "./distributedStorage";
import { Payload, IPathfinderContext, IPathfinder, SimplePathfinderCreator, IPeerPath, IPath } from "./simplePathfinder";
import { ITopology } from "src/domain/Lobby";
import { IFragmentId } from "../lang_processing/cf";


export const Poi1dPathfinderCreator: (context: IPathfinderContext) => IPathfinder = context => {
	const pathfinder: any = {};
	pathfinder.context = context;

	pathfinder.stringHashCode = (value: string) => {
		let hash = 0;
		if (value.length === 0) return hash;
		for (let i = 0; i < value.length; i++) {
			const chr   = value.charCodeAt(i);
			hash = ((hash << 5) - hash) + chr;
			hash |= 0; // Convert to 32bit integer
		}
		return hash;
	};

	pathfinder.iterateOverLine = eval(`(function*(topology) {
		var zeroPeer = topology.peers.find(p => !pathfinder.getPeerAtKey(topology, p.id, "left"));
		if (!zeroPeer) throw new Error("Unexpected: Zero peer not found in line.");
		var peer = zeroPeer;
		var count = 0;
		while (peer) {
			yield { peer, linePosition: count };
			count++;
			peer = pathfinder.getPeerAtKey(topology, peer.id, "right");
		}
	})`);

	pathfinder.getPeerAtKey = (topology: ITopology, peerId: string, key: string) => {
		const peer = topology.peers.find(p => p.id === peerId);
		if (!peer) throw new Error("Peer not found");
		const edge = topology.edges.find(e => 
			e.fromId === peerId && e.fromKey === key
			|| e.toId === peerId && e.toKey === key);
		if (!edge) return undefined;
		const peerAtKeyId = edge.fromId === peerId ? edge.toId : edge.fromId;
		const res = topology.peers.find(p => p.id === peerAtKeyId);
		if (!res) throw new Error("Has edge to peer but no peer found itself!");
		return res;
	}

	pathfinder.getLineNumber = (peerId: string) => {
		const line = pathfinder.iterateOverLine(pathfinder.context.topology);
		while (true) {
			const { value, done } = line.next();
			if (done) throw new Error("Unexpected: line ended but peer is not found.");
			if (value.peer.id === peerId) return value.linePosition;
		}
	}

	pathfinder.getPeerAtLineNumber = (lineNumber: number) => {
		const line = pathfinder.iterateOverLine(pathfinder.context.topology);
		while (true) {
			const { value, done } = line.next();
			if (done) throw new Error("Unexpected: line ended but target position is not reached.");
			if (value.linePosition === lineNumber) return value.peer;
		}
	}

	pathfinder.linePosition = pathfinder.getLineNumber(pathfinder.context.selfId);

	pathfinder.calculatePath = (payload: Payload, targetPeerId: string) => {
		const targetLinePos = pathfinder.getLineNumber(targetPeerId);

		return pathfinder.calculatePathForLinePosition(targetLinePos, targetPeerId);
	}

	pathfinder.calculatePathForLinePosition = (linePosition: number, targetPeerId: string) => {
		if (linePosition === pathfinder.linePosition) return { type: 'STAY' };
		const targetDirection = linePosition > pathfinder.linePosition ? 'right' : 'left'

		return { type: 'PEER', direction: targetDirection, targetPeerId } as IPeerPath;
	}

	pathfinder.idToString = (id: IFragmentId): string => {
		return [id.main].concat(id.indicies.map(ind => {
			return ind.value as string;
		})).join('.');
	}
	
	pathfinder.whereTo = (payload: Payload) => {
		const id = payload.type === 'DF REQUEST' ? (payload as FragmentRequestPayload).fragmentId
			: payload.type === 'DF' ? (payload as DfPayload).df.id
			: payload.type === 'CF' ? (payload as CfPayload).cf.id
			: null;
		if (!id) return { type: 'STAY' } as IPath;
		
		const lastInd = id.indicies.length !== 0 ? id.indicies[id.indicies.length-1].value : undefined;
		const targetLinePos = lastInd ? lastInd % pathfinder.context.topology.peers.length : 0;
		const targetPeerId = pathfinder.getPeerAtLineNumber(targetLinePos).id;
		
		return pathfinder.calculatePathForLinePosition(targetLinePos, targetPeerId);
	}

	return pathfinder;
}