import { ExecutionPeriod, IdlePeriod, Worker } from "./worker";

export class TimeLogger {
	private _executionPeriods: ExecutionPeriod[];
	public get ExecutionPeriods() { return this._executionPeriods; }

	private _idlePeriods: IdlePeriod[];
	public get IdlePeriods() { return this._idlePeriods; }
	
	public constructor(worker: Worker) {
		worker.onCfExecutionStart.addCallback(execPeriod => {
			this._executionPeriods.push(execPeriod);
		});
		worker.onCfExecutionFinish.addCallback(execPeriod => {
			const startEntry = this._executionPeriods.find(p => p === execPeriod);
			if (!startEntry) throw new Error("Trying to log time but start entry not found.");
		});
		worker.onIdleStart.addCallback(idlePeriod => {
			this._idlePeriods.push(idlePeriod);
		});
		worker.onIdleFinish.addCallback(idlePeriod => {
			const startEntry = this._idlePeriods.find(p => p === idlePeriod);
			if (!startEntry) throw new Error("Trying to log time but start entry not found.");
		});
	}
}