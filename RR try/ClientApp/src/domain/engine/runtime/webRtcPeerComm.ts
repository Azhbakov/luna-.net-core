import { IPeerComm, IMessage } from "src/domain/engine/runtime/commModule";

export class WebRtcPeerComm implements IPeerComm {
	private _peerId: string;
	public get peerId(): string { return this._peerId; }

	private _direction: string;
	public get direction(): string { return this._direction; }

	private _dataChannel: RTCDataChannel;

	private _receiveCallbacks: ((message: IMessage) => void)[] = [];

	public constructor(peerId: string, direction: string, dataChannel: RTCDataChannel) {
		this._peerId = peerId;
		this._direction = direction;
		this._dataChannel = dataChannel;

		const prevHandler = this._dataChannel.onmessage;
		const bindedHandler = prevHandler ? prevHandler.bind(dataChannel) : null;
		const cbs = this._receiveCallbacks;
		this._dataChannel.onmessage = (ev: MessageEvent) => {
			if (bindedHandler) bindedHandler(ev);
			const data = JSON.parse(ev.data) as IMessage;
			cbs.forEach(cb => cb(data));
		}
	}

	public send(message: IMessage): Promise<void> {
		this._dataChannel.send(JSON.stringify(message));
		return Promise.resolve();
	}
	
	public onReceive(callback: (message: IMessage) => void): void {
		this._receiveCallbacks.push(callback);
	}


}