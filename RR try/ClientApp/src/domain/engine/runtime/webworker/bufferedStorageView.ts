import { ICf, IDf } from "../../lang_processing/cf";
import { IObservableStorage } from "../distributedStorage";
import { CallbackStore, CallbackControl } from "src/utils/callbackControl";

export interface IBatchUpdate {
	cfs: ICf[]
	dfs: IDf[]
	removedCfs: ICf[]
}

export interface IObservableBufferedStorage extends IObservableStorage {
	onBatchUpdate: CallbackStore<IBatchUpdate>;
}

export class BufferedStorageView implements IObservableBufferedStorage {
	private _storage: IObservableStorage;

	// Callbacks
	protected _addCfCallbacks: CallbackControl<ICf> = new CallbackControl<ICf>(); // callback returns true if fragment meets it requirements and callback is no longer needed?
	public get onCfAdded(): CallbackStore<ICf> { return this._addCfCallbacks; }

	protected _addDfCallbacks: CallbackControl<IDf> = new CallbackControl<IDf>();
	public get onDfAdded(): CallbackStore<IDf> { return this._addDfCallbacks; }

	protected _removeCfCallbacks: CallbackControl<ICf> = new CallbackControl<ICf>();
	public get onCfRemoved(): CallbackStore<ICf> { return this._removeCfCallbacks; }
	
	protected _batchUpdateCallbacks: CallbackControl<IBatchUpdate> = new CallbackControl<IBatchUpdate>();
	public get onBatchUpdate(): CallbackStore<IBatchUpdate> { return this._batchUpdateCallbacks; }

	// Batch collecting
	private _cfBuffer: ICf[] = [];
	private _dfBuffer: IDf[] = [];
	private _removedCfBuffer: ICf[] = [];

	private _lastAddTime: Date = new Date();
	private _timer: any;
	private _addCount: number = 0;
	private readonly TimeStepMs = 2000;
	private readonly SaturationCount = 10;

	public constructor(storage: IObservableStorage) {
		this._storage = storage;
		storage.onCfAdded.addCallback(cf => this.notifyOrBufferize({ type: 'cf', cf }));
		storage.onDfAdded.addCallback(df => this.notifyOrBufferize({ type: 'df', df }));
		storage.onCfRemoved.addCallback(cf => this.notifyOrBufferize({ type: 'removeCf', cf }));
	}

	private notifyOrBufferize(update: 
		{ type: 'cf', cf: ICf } 
		| { type: 'df', df: IDf }
		| { type: 'removeCf', cf: ICf }) {

		const currentTime = new Date();
		
		// Check time and buffers and batch notify
		if (!this._timer) {
			const th = this;
			this._timer = setTimeout(() => {
				if (th.isSaturated()) {
					// send batch and clean buffers
					th._batchUpdateCallbacks.fire({ 
						cfs: th._cfBuffer, 
						dfs: th._dfBuffer,
						removedCfs: th._removedCfBuffer
					} as IBatchUpdate);
					th._cfBuffer = []; th._dfBuffer = []; th._removedCfBuffer = [];
				}
				th._addCount = 0;
				th._lastAddTime = currentTime;
				th._timer = null;
			}, this.TimeStepMs);
		}

		// Bufferize or immediately notify
		this._addCount++;
		if (this.isSaturated()) {
			switch (update.type) {
				case 'cf': this._cfBuffer.push(update.cf); break;
				case 'df': this._dfBuffer.push(update.df); break;
				case 'removeCf': this._removedCfBuffer.push(update.cf); break;
				default: throw new Error(`Update type not implemented`);
			}
		} else {
			switch (update.type) {
				case 'cf': this._addCfCallbacks.fire(update.cf); break;
				case 'df': this._addDfCallbacks.fire(update.df); break;
				case 'removeCf': this._removeCfCallbacks.fire(update.cf); break;
				default: throw new Error(`Update type not implemented`);
			}
		}
	}

	private isSaturated(): boolean {
		return this._addCount > this.SaturationCount;
	}

	public getCfs(): ICf[] { return this._storage.getCfs(); }
	public getDfs(): IDf[] { return this._storage.getDfs(); }
}