import { ICf, IDf } from "../../lang_processing/cf";
import { lunaLang } from "../../lang_processing/lunaLang";
import { Payload, IRemotePath, IPathfinderContext, IPathfinder } from "../simplePathfinder";

export interface ControlMessage {
	type: 'initWebworker'
		| 'workerStep' | 'workerStepFinished' 
		| 'workerRun' | 'workerRunFinished'
		| 'workerCfExecuted'
}

export interface InitWebworker extends ControlMessage {
	type: 'initWebworker'
	worker: InitWorker,
	storage: InitStorage,
	pathfinder: InitPathfinder
}

export interface InitWorker {
	atoms: any, //{ [names: string]: string }, // deserialize functions manually 
	program: lunaLang.Program
	isLeader: boolean
}
export interface InitStorage {
	selfPeerId: string
}
export interface InitPathfinder {
	pathfinderCreator: (pathfinderContext: IPathfinderContext) => IPathfinder,
	pathfinderContext: IPathfinderContext
}

export interface WorkerStep extends ControlMessage {
	type: 'workerStep'
}
export interface WorkerStepFinished extends ControlMessage {
	type: 'workerStepFinished',
	value: 'continue' | 'end'
}

export interface WorkerRun extends ControlMessage {
	type: 'workerRun'
}
export interface WorkerRunFinished extends ControlMessage {
	type: 'workerRunFinished',
}

export interface WorkerCfExecuted extends ControlMessage {
	type: 'workerCfExecuted',
	cf: ICf
}


export interface CommMessage {
	type: 'commSend' | 'commRecieve'
}

export interface CommSend extends CommMessage {
	type: 'commSend',
	payload: Payload,
	path: IRemotePath
}

export interface CommRecieve extends CommMessage {
	type: 'commRecieve',
	peerId: string,
	payload: Payload
}


export interface UIMessage {
	type: 'storageCf' | 'storageDf' | 'storageCfRemoved' | 'storageBatchUpdate'
		| 'workerExec' | 'workerIdle'
}

export interface StorageCf extends UIMessage {
	type: 'storageCf',
	cf: ICf, 
}
export interface StorageDf extends UIMessage {
	type: 'storageDf',
	df: IDf, 
}
export interface StorageCfRemoved extends UIMessage {
	type: 'storageCfRemoved',
	cf: ICf 
}
export interface StorageBatchUpdate {
	type: 'storageBatchUpdate',
	cfs: ICf[],
	dfs: IDf[],
	removedCfs: ICf[]
}