import { ICommModule, CommMessageHandler } from "../commModule";
import { IRemotePath, Payload } from "../simplePathfinder";
import { WebworkerChannel } from "./webworkerChannel";
import { CommRecieve, CommSend } from "./messageTypes";
import { CallbackControl, CallbackStore } from "src/utils/callbackControl";

export class ThreadCommModule implements ICommModule {
	private _commModule: ICommModule;
	private _webworkerChannel: WebworkerChannel;

	public constructor(commModule: ICommModule, webworkerChannel: WebworkerChannel) {
		this._commModule = commModule;
		this._webworkerChannel = webworkerChannel;
		
		const th = this;
		this._webworkerChannel.addMessageHandler('commSend', ev => {
			const send = (ev.data as CommSend);
			th._commModule.send(send.payload, send.path);
		});

		this._commModule.addReceiveCallback((peerId, payload) => th._webworkerChannel.postMessage({ type: "commRecieve", peerId, payload } as CommRecieve));
	}
	
	public send(payload: Payload, path: IRemotePath): Promise<void> {
		this._webworkerChannel.postMessage({ type: "commSend", payload, path} as CommSend)
		return Promise.resolve();
	}	
	
	public addReceiveCallback(callback: (peerId: string, payload: Payload) => void): void {
		throw new Error("Expected all callbacks to be registered at worker side or on underlying ICommModule");
	}
}

export class ThreadCommModuleProxy implements ICommModule {
	private _callbacks: CommMessageHandler[] = [];
	
	private _sendCallbacks: CallbackControl<{payload: Payload, path: IRemotePath}> = new CallbackControl<{payload: Payload, path: IRemotePath}>();
	get onSend(): CallbackStore<{payload: Payload, path: IRemotePath}> { return this._sendCallbacks; }

	public send(payload: Payload, path: IRemotePath): Promise<void> {
		this._sendCallbacks.fire({ payload, path });
		return Promise.resolve();
	}
	
	public recieve(peerId: string, payload: Payload) {
		this._callbacks.forEach(cb => cb(peerId, payload));
	}

	public addReceiveCallback(callback: (peerId: string, payload: Payload) => void): void {
		this._callbacks.push(callback);
	}
}