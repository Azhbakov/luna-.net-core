import { CallbackControl, CallbackStore } from "src/utils/callbackControl";
import { ICf, IDf } from "../../lang_processing/cf";
import { WebworkerChannel } from "./webworkerChannel";
import { StorageCf, StorageDf, StorageCfRemoved, StorageBatchUpdate, InitStorage } from "./messageTypes";
import { IObservableBufferedStorage, IBatchUpdate } from "./bufferedStorageView";

export class ThreadStorageProxy implements IObservableBufferedStorage {
	private _webworker: WebworkerChannel;
	
	protected _addCfCallbacks: CallbackControl<ICf> = new CallbackControl<ICf>(); // callback returns true if fragment meets it requirements and callback is no longer needed?
	public get onCfAdded(): CallbackStore<ICf> { return this._addCfCallbacks; }

	protected _addDfCallbacks: CallbackControl<IDf> = new CallbackControl<IDf>();
	public get onDfAdded(): CallbackStore<IDf> { return this._addDfCallbacks; }

	protected _removeCfCallbacks: CallbackControl<ICf> = new CallbackControl<ICf>();
	public get onCfRemoved(): CallbackStore<ICf> { return this._removeCfCallbacks; }

	protected _batchUpdateCallbacks: CallbackControl<IBatchUpdate> = new CallbackControl<IBatchUpdate>();
	public get onBatchUpdate(): CallbackStore<IBatchUpdate> { return this._batchUpdateCallbacks; }

	private constructor() {}

	public static init(webworker: WebworkerChannel, selfPeerId: string): { storage: ThreadStorageProxy, initMessage: InitStorage } {
		const storage = new ThreadStorageProxy();
		webworker.addMessageHandler('storageCf', ev => storage._addCfCallbacks.fire((ev.data as StorageCf).cf));
		webworker.addMessageHandler('storageDf', ev => storage._addDfCallbacks.fire((ev.data as StorageDf).df));
		webworker.addMessageHandler('storageCfRemoved', ev => storage._removeCfCallbacks.fire((ev.data as StorageCfRemoved).cf));
		webworker.addMessageHandler('storageBatchUpdate', ev => storage._batchUpdateCallbacks.fire((ev.data as StorageBatchUpdate)));

		return { storage, initMessage: {
			selfPeerId
		} as InitStorage };
	}

	public getCfs(): ICf[] {
		return []
	}
	public getDfs(): IDf[] {
		return []
	}


} 