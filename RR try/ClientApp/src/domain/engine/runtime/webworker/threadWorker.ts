import { InitWorker, WorkerStepFinished, WorkerRunFinished, StorageCf, StorageDf, StorageCfRemoved, StorageBatchUpdate, CommSend, CommRecieve, InitPathfinder, InitStorage, InitWebworker } from "./messageTypes";
import { DistributedStorage } from "../distributedStorage";
import { Worker } from "../worker";
import { BufferedStorageView, IObservableBufferedStorage } from "./bufferedStorageView";
import { ThreadCommModuleProxy } from "./threadCommModule";
import { IPathfinder, IPathfinderContext } from "../simplePathfinder";
import { BallStop, BallStopContext } from "../ballStop";

let pathfinder: IPathfinder | null = null;
const commModule: ThreadCommModuleProxy = new ThreadCommModuleProxy()
let worker: Worker | null = null;
let storage: DistributedStorage | null = null;
let bufferedStorageView: IObservableBufferedStorage | null = null;
let ballStop: BallStop | null = null;
let pathfinderContext: IPathfinderContext | null; // TODO REMOVE

commModule.onSend.addCallback(send => postMessage({ type: "commSend", payload: send.payload, path: send.path} as CommSend));

self.addEventListener('message', e => { // eslint-disable-line no-restricted-globals
	const m = e.data;//JSON.parse(e.data);
	switch (m.type) {
		// Initialization
		case 'initWebworker':
			const initWebworkerMessage = m as InitWebworker;
			
			// Pathfinder
			const initPathfinderMessage = (initWebworkerMessage.pathfinder as InitPathfinder);
			initPathfinderMessage.pathfinderCreator = deserializeAtoms({creator: initPathfinderMessage.pathfinderCreator}).creator;
			pathfinder = initPathfinderMessage.pathfinderCreator(initPathfinderMessage.pathfinderContext);

			pathfinderContext = initPathfinderMessage.pathfinderContext; // TODO REMOVE

			// Storage
			storage = new DistributedStorage(initWebworkerMessage.storage.selfPeerId, pathfinder, commModule);
			bufferedStorageView = new BufferedStorageView(storage);
			bufferedStorageView.onCfAdded.addCallback(cf => postMessage({ type: 'storageCf', cf } as StorageCf));
			bufferedStorageView.onDfAdded.addCallback(df => postMessage({ type: 'storageDf', df } as StorageDf));
			bufferedStorageView.onCfRemoved.addCallback(cf => postMessage({ type: 'storageCfRemoved', cf } as StorageCfRemoved));
			bufferedStorageView.onBatchUpdate.addCallback(b => postMessage({ type: 'storageBatchUpdate', cfs: b.cfs, dfs: b.dfs, removedCfs: b.removedCfs } as StorageBatchUpdate));
			
			// Worker
			if (!storage) throw new Error("Storage must be initialized first!");
			const initWorkerMessage = initWebworkerMessage.worker;
			initWorkerMessage.atoms = deserializeAtoms(initWorkerMessage.atoms);
			worker = new Worker(initWorkerMessage.program, initWorkerMessage.atoms, storage, initWorkerMessage.isLeader);	
			
			ballStop = new BallStop(worker, commModule, pathfinderContext as any); // TODO REMOVE
			const start = performance.now();
			ballStop.onFinish.addCallback(finishDetails => {
				console.log(`Exec time ms: ${finishDetails.time - start}`);
			})
			break;
		
		// Exec
		case 'workerStep':
			if (!worker) throw new Error("Worker is not initialized.")
			worker.step().then(res=>{
				postMessage({type: 'workerStepFinished', value: res} as WorkerStepFinished);
			});
			break;
		case 'workerRun':
			if (!worker) throw new Error("Worker is not initialized.");
			worker.work().then(() => {
				postMessage({type: 'workerRunFinished'} as WorkerRunFinished);
			});
			break;
		case 'storageCf':
			if (!storage) throw new Error('Storage is not initialized.');
			storage.putCf((m as StorageCf).cf);
			break;
		case 'commRecieve':
			if (!commModule) throw new Error("CommModule is not initialized.");
			const recieve = m as CommRecieve
			commModule.recieve(recieve.peerId, recieve.payload);
			break;
		case undefined: break; // random system message
		default: 
			throw new Error(`Unknown message type: ${m.type}`);
	}
})

function deserializeAtoms(atoms: any) {
	for (const property in atoms) {
		if (atoms.hasOwnProperty(property)) {
			atoms[property] = typeof atoms[property] === 'string' && 
				(atoms[property].lastIndexOf('function', 0) === 0 || atoms[property].lastIndexOf('(', 0) === 0) ? 
				eval('(' + atoms[property] + ')')
				: atoms[property];
		}
	}
	return atoms;
}
	
function postMessage(message: any, transfer?: any[]): void {
	(self.postMessage as any)(message, transfer);
};