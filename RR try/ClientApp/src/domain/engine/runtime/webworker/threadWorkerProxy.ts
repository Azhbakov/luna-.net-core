import { IWorker, ExecutionPeriod, IdlePeriod } from "../worker";
import { CallbackStore, CallbackControl } from "src/utils/callbackControl";
import { lunaLang } from "../../lang_processing/lunaLang";
import { ICf } from "../../lang_processing/cf";
import { WorkerCfExecuted, InitWorker, WorkerStep, WorkerStepFinished, WorkerRun } from "./messageTypes";
import { WebworkerChannel } from "./webworkerChannel";

export class ThreadWorkerProxy implements IWorker {
	private readonly _webworker: WebworkerChannel;
	private _resolveStep: (res: 'continue' | 'end') => void;
	private _resolveRun: () => void;

	// Callbacks
	private _cfExecutedCallbacks: CallbackControl<ICf> = new CallbackControl<ICf>();
	public get onCfExecuted(): CallbackStore<ICf> { return this._cfExecutedCallbacks; }

	// Logging callbacks
	private _onCfExecutionStart: CallbackControl<ExecutionPeriod> = new CallbackControl<ExecutionPeriod>();
	public get onCfExecutionStart(): CallbackStore<ExecutionPeriod> { return this._onCfExecutionStart; }
	private _onCfExecutionFinish: CallbackControl<ExecutionPeriod> = new CallbackControl<ExecutionPeriod>();
	public get onCfExecutionFinish(): CallbackStore<ExecutionPeriod> { return this._onCfExecutionFinish; }

	private _onIdleStart: CallbackControl<IdlePeriod> = new CallbackControl<IdlePeriod>();
	public get onIdleStart(): CallbackStore<IdlePeriod> { return this._onIdleStart; }
	private _onIdleFinish: CallbackControl<IdlePeriod> = new CallbackControl<IdlePeriod>();
	public get onIdleFinish(): CallbackStore<IdlePeriod> { return this._onIdleFinish; }

	private constructor(webworker: WebworkerChannel) {
		this._webworker = webworker;
	}

	public static init(webworker: WebworkerChannel, program: lunaLang.Program, atoms: {[name: string]: any}, isLeader: boolean = true): { worker: ThreadWorkerProxy, initMessage: InitWorker } {
		const worker = new ThreadWorkerProxy(webworker);
		webworker.addMessageHandler('workerStepFinished', ev => worker._resolveStep((ev.data as WorkerStepFinished).value));
		webworker.addMessageHandler('workerRunFinished', ev => worker._resolveRun());
		webworker.addMessageHandler('workerCfExecuted', ev => worker._cfExecutedCallbacks.fire((ev.data as WorkerCfExecuted).cf));
		
		return { worker, initMessage: {
			program, 
			atoms: this.serializeAtoms(atoms),
			isLeader
		} as InitWorker }
	}

	public step(): Promise<"continue" | "end"> {
		this._webworker.postMessage({type: 'workerStep'} as WorkerStep);
		const th = this;
		return new Promise<"continue" | "end">(resolve => {
			th._resolveStep = resolve;
		});
	}
	
	public async work(): Promise<void> {
		this._webworker.postMessage({type: 'workerRun'} as WorkerRun);
		const th = this;
		return new Promise<void>(resolve => {
			th._resolveRun = resolve;
		});
	}

	private static serializeAtoms(atoms: any): any {
		const serializedAtoms = {};
		for (const property of Object.keys(atoms)) {
			serializedAtoms[property] = typeof atoms[property] === 'function' ?
				atoms[property].toString()
				: atoms[property];
		}
		return serializedAtoms;
	}
}