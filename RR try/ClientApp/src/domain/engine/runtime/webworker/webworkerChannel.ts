export class WebworkerChannel {
	private readonly _worker = new Worker("static/js/threadWorker.js");
	private _messageHandlers: {messageType: string, handler: (ev: any) => void}[] = [];

	public constructor() {
		const th = this;
		this._worker.onmessage = (ev: MessageEvent) => {
			let isHandled = false;
			th._messageHandlers.forEach(mh => {
				if (mh.messageType === ev.data.type) {
					mh.handler(ev);
					isHandled = true;
				}
			})
			if (!isHandled) throw new Error(`Unknown message type ${ev.data.type}`);
		}
	}

	public close() {
		this._worker.terminate();
	}

	public postMessage(message: any) {
		this._worker.postMessage(message);
	}
	
	public addMessageHandler(messageType: string, handler: (ev: any) => void): void {
		this._messageHandlers.push({messageType, handler});
	}
}