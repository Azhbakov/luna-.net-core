import { IFragmentStorage } from "./distributedStorage";
import { lunaLang } from "../lang_processing/lunaLang";
import { executeCf, interpretCfDecl, checkCfResolved, } from "../lang_processing/factories";
import { IExecutionContext, IDf, IProgram, ICf, IFragmentId, fragmentIdMethods } from "../lang_processing/cf";
import { CallbackControl, CallbackStore } from "../../../utils/callbackControl";

export interface ExecutionPeriod {
	cfId: IFragmentId;
	startTime: Date;
	finishTime: Date | null;
}
export interface IdlePeriod {
	type: 'WAITING CFS' | 'WAITING DFS';
	startTime: Date;
	finishTime: Date | null;
}

export interface IWorker {
	step(): Promise<'continue' | 'end'>
	work(): Promise<void>
	onCfExecuted: CallbackStore<ICf>
	
	onCfExecutionStart: CallbackStore<ExecutionPeriod>
	onCfExecutionFinish: CallbackStore<ExecutionPeriod>
	onIdleStart: CallbackStore<IdlePeriod>
	onIdleFinish: CallbackStore<IdlePeriod>
}

export class Worker implements IWorker {
	private _program: IProgram;
	private _fragmentStorage: IFragmentStorage;
	private _cfQueue: ICf[] = [];
	private _dfsRequested: { requestedDfId: IFragmentId, pendingCf: ICf }[] = [];
	
	private _cfWaitingPromiseResolver: any = null;

	public onError: (message: string) => void;

	// Callbacks
	private _cfExecutedCallbacks: CallbackControl<ICf> = new CallbackControl<ICf>();
	public get onCfExecuted(): CallbackStore<ICf> { return this._cfExecutedCallbacks; }


	// Logging callbacks
	private _onCfExecutionStart: CallbackControl<ExecutionPeriod> = new CallbackControl<ExecutionPeriod>();
	public get onCfExecutionStart(): CallbackStore<ExecutionPeriod> { return this._onCfExecutionStart; }
	private _onCfExecutionFinish: CallbackControl<ExecutionPeriod> = new CallbackControl<ExecutionPeriod>();
	public get onCfExecutionFinish(): CallbackStore<ExecutionPeriod> { return this._onCfExecutionFinish; }

	private _onIdleStart: CallbackControl<IdlePeriod> = new CallbackControl<IdlePeriod>();
	public get onIdleStart(): CallbackStore<IdlePeriod> { return this._onIdleStart; }
	private _onIdleFinish: CallbackControl<IdlePeriod> = new CallbackControl<IdlePeriod>();
	public get onIdleFinish(): CallbackStore<IdlePeriod> { return this._onIdleFinish; }


	public constructor(program: lunaLang.Program, atoms: {[name: string]: any}, fragmentStorage: IFragmentStorage, isLeader: boolean = true) {
		this._program = { source: program, atoms };
		this._fragmentStorage = fragmentStorage;

		// Init queue
		const worker = this;
		this._fragmentStorage.onCfAdded.addCallback(cf => worker.acceptForExecution(cf));
		// Get new DF? Try to execute all CFs which depend on this fragment
		this._fragmentStorage.onDfAdded.addCallback(df => {
			let ind = -1;
			while ((ind = worker._dfsRequested.findIndex(r => fragmentIdMethods.equals(r.requestedDfId, df.id))) !== -1) {
				const cf = worker._dfsRequested[ind].pendingCf;
				worker._dfsRequested.splice(ind, 1);
				worker.acceptForExecution(cf);
			}
		});

		// Extract main TODO MOVE OUTSIDE
		if (isLeader) {
			const mainCf: lunaLang.IExec = { type: 'exec', code: 'main', args: [], id: ['main'] };
			const initialContext: IExecutionContext = { variables: [], parentCfId: { main: "0.0", indicies: [] }/*, tryGetDf: (id: string) => this._fragmentStorage.getDf(id) */};
			this._fragmentStorage.putCf(interpretCfDecl(mainCf, initialContext, this._program));
		}
	}

	private waitForCf(): Promise<ICf | 'end'> {
		// Check if already have CF
		const cf = this._cfQueue.shift();
		if (cf) return Promise.resolve(cf);

		// if (this._fragmentStorage.getCfs().length === 0) 
		// 	return Promise.resolve('end' as 'end');

		// Log idle start
		const idlePeriod: IdlePeriod = { 
			type: this._dfsRequested.length === 0 ? 'WAITING CFS' : 'WAITING DFS', 
			startTime: new Date(), 
			finishTime: null
		};
		this._onIdleStart.fire(idlePeriod);
		
		const worker = this;
		return new Promise(resolve => {
			// Start awaiting
			worker._cfWaitingPromiseResolver = () => {
				// Log idle finish
				idlePeriod.finishTime = new Date();
				worker._onIdleFinish.fire(idlePeriod);

				resolve(worker._cfQueue.pop()); // Still using CF from queue! Do not pass CF as argument.
			}
		});
	}

	private acceptForExecution(cf: ICf): void {
		this._cfQueue.push(cf);

		// Resolve promise and reset it. CF will be obtained from queue, not from here.
		const waitingPromiseResolver = this._cfWaitingPromiseResolver;
		this._cfWaitingPromiseResolver = null;
		if (waitingPromiseResolver) waitingPromiseResolver();
	}

	public step(): Promise<'continue' | 'end'> {
		const worker = this;
		return this.waitForCf().then(cf => {
			if (cf === 'end') return 'end';

			// Log start
			const cfExecution: ExecutionPeriod = { cfId: cf.id, startTime: new Date(), finishTime: null };
			this._onCfExecutionStart.fire(cfExecution);

			// Get CF
			if (!checkCfResolved(cf, (dfId: IFragmentId) => { // DfStorageAccessor
				const df = this._fragmentStorage.getDf(dfId);
				if (df) return df;

				worker._dfsRequested.push({ requestedDfId: dfId, pendingCf: cf });
				return null;
			})) {
				return 'continue';
			}

			const res = executeCf(cf, worker._program);

			// Store results
			res.dfs.forEach(resDf => worker._fragmentStorage.putDf(resDf)) 
			res.cfs.forEach(resCf => worker._fragmentStorage.putCf(resCf));
			this._fragmentStorage.popCf(cf.id);

			this._cfExecutedCallbacks.fire(cf);

			// Log finish
			cfExecution.finishTime = new Date();
			this._onCfExecutionFinish.fire(cfExecution);
			return 'continue';
		});
	}

	public async work(): Promise<void> {
		const worker = this;
		while (true) {
			const res = await worker.step();
			if (res === 'end') return;
		};
	}
}

export type DfStorageAccessor = (dfId: IFragmentId) => IDf | null;