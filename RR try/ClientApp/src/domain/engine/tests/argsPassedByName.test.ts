import { lunaLang } from "../lang_processing/lunaLang";
import { IExecCf } from "../lang_processing/cf_impl/execCf";
import { IProgram } from "../lang_processing/cf";
import { executeCf } from "../lang_processing/factories";
import { createSubroutine, createExecCf, createExecDecl } from "./utils";
import { IArgument, IDfArgument } from "../lang_processing/arguments";

/// def mainSub(firstArg: int, secondArg: id)
/// 	innerExec1(secondArg)
/// 	innerExec2(firstArg)
///
/// def innerSub1(innerArgName: name)
///
/// def innerSub2(innerArgName: int)
///
/// call mainExec(int, df)


const program: IProgram = {
	atoms: [],
	source: {

		"execPrototype": createSubroutine(sub => {
			sub.args = [
				{
					id: "firstArg",
					type: "int"
				},
				{
					id: "secondArg",
					type: "name"
				}
			];
			sub.body = [
				createExecDecl(execDecl => {
					execDecl.code = "innerSub2";	
					execDecl.id = ["innerExec2Id"];
					execDecl.args.push({
						"ref": [
							"secondArg"
						],
						type: "id"
					} as lunaLang.Argument);
					return execDecl;
				}),
				createExecDecl(execDecl => {
					execDecl.code = "innerSub1";	
					execDecl.id = ["innerExec1Id"];
					execDecl.args.push({
						"ref": [
							"firstArg"
						],
						type: "id"
					} as lunaLang.Argument);
					return execDecl;
				}),
			];
			return sub;
		}),

		"innerSub1": createSubroutine(sub => {
			sub.args.push({
				type: "int",
				id: "innerArgName"
			});
			return sub;
		}),

		"innerSub2": createSubroutine(sub => {
			sub.args.push({
				type: "name",
				id: "innerArgName"
			});
			return sub;
		})
	}
}

function mainExecWithArgs (args: IArgument[]) : IExecCf {
	return createExecCf(execCf => {
		execCf.id.main = "mainExecId";
		execCf.args = args;
		execCf.prototypeName = "execPrototype";
		execCf.prototype = program.source[execCf.prototypeName];
		return execCf;
	});
}

function expectedResultForArgs (args: IArgument[]) : IExecCf[] {
	return [
			{
			type: "exec",
			id: {
				main: "mainExecId.innerExec2Id",
				indicies: []
			},
			prototype: program.source["innerSub2"],
			prototypeName: "innerSub2",
			args: [args[1]]
		},
		{
			type: "exec",
			id: {
				main: "mainExecId.innerExec1Id",
				indicies: []
			},
			prototype: program.source["innerSub1"],
			prototypeName: "innerSub1",
			args: [args[0]]
		}
	]
}

it("argument passed out of order on Exec Subroutine", () => {
	const args = [
		{ 
			type: 'iconst', 
			value: 11 
		} as lunaLang.IConstArgument,
		{ 
			type: 'id', 
			id: { main: "dfArgId", indicies: [] },
			value: undefined 
		} as IDfArgument
	]
	const res = executeCf(mainExecWithArgs(args), program);
	expect(res.dfs).toHaveLength(0);
	expect(res.cfs).toHaveLength(2);
	expect(res.cfs).toEqual(expectedResultForArgs(args));
});
