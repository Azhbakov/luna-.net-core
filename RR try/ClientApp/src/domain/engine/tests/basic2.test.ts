import { Worker } from '../runtime/worker'
import { basic2, basic2Atoms } from './tasks/basic2';
import { LocalStorage } from '../runtime/localStorage';

/// import c_init(int, name) as init;
/// import c_print(value) as print;
/// import c_iprint(int) as iprint;
///
/// sub main()
/// {
/// 	df x;
///
/// 	print(x);
///	
/// 	cf b[10]: init(7, x);
/// }

it('worker executes basic program', () => {
	const worker = new Worker(basic2, basic2Atoms, new LocalStorage());
	worker.step().then(() => {
		return worker.step();
	}).then(() => {
		return worker.step();
	}).then(() => {
		return worker.step();
	}).then(() => {
		return worker.step();
	})
});
