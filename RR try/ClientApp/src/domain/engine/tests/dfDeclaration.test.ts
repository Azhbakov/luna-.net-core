import { lunaLang } from "../lang_processing/lunaLang";
import { IExecCf } from "../lang_processing/cf_impl/execCf";
import { IProgram } from "../lang_processing/cf";
import { executeCf } from "../lang_processing/factories";
import { createSubroutine, createExecCf, createExecDecl } from "./utils";
import { IArgument, IDfArgument } from "../lang_processing/arguments";

/// def mainSub()
/// 	df x;
/// 	innerExec(x)
///
/// def innerAtom(newValue: int, res: name)
/// 	init(newValue, res)
/// 		res.value = newValue
///
/// call mainExec()


const program: IProgram = {
	atoms: {"init": (newValue: number, res: IDfArgument) => {
		res.value = newValue;
	}},
	source: {

		"execPrototype": createSubroutine(sub => {
			sub.args = [];
			sub.body.push({
				type: 'dfs',
				names: ['x']
			} as lunaLang.IDfDeclaration);
			sub.body.push(createExecDecl(execDecl => {
				execDecl.code = "innerAtom";	
				execDecl.id = ["innerExecId"];
				execDecl.args = [{
					type: 'iconst',
					value: 42
				} as lunaLang.IConstArgument,
				{
					"ref": [
						"x"
					],
					type: "id"
				} as lunaLang.Argument];
				return execDecl;
			}));
			return sub;
		}),

		"innerAtom": {
			type: 'extern',
			code: 'init',
			args: [
				{ type: 'int' },
				{ type: 'name' }
			]
		}
	}
}

function mainExecWithOneArg (arg: IArgument) : IExecCf {
	return createExecCf(execCf => {
		execCf.id.main = "mainExecId";
		execCf.args.push(
			arg
		);
		execCf.prototypeName = "execPrototype"
		execCf.prototype = program.source[execCf.prototypeName];
		return execCf;
	});
}

it("DF declaration works on Exec Subroutine", () => {
	const main = createExecCf(execCf => {
		execCf.id.main = "mainExecId";
		execCf.prototypeName = "execPrototype"
		execCf.prototype = program.source[execCf.prototypeName];
		return execCf;
	});
	const res = executeCf(main, program);
	expect(res.dfs).toHaveLength(0);
	expect(res.cfs).toHaveLength(1);
	expect(res.cfs[0]).toEqual({
		type: "exec",
		id: {
			main: "mainExecId.innerExecId",
			indicies: []
		},
		prototype: program.source["innerAtom"],
		prototypeName: "innerAtom",
		args: [
			{
				type: 'iconst',
				value: 42
			} as lunaLang.IConstArgument,
			{
				type: 'id',
				id: { main: 'mainExecId.x', indicies: [] },
				value: undefined
			} as IDfArgument
		]
	});

	const res2 = executeCf(res.cfs[0], program);
	expect(res2.dfs).toHaveLength(1);
	expect(res2.cfs).toHaveLength(0);
	expect(res2.dfs[0]).toEqual({
		type: 'id',
		id: { main: 'mainExecId.x', indicies: [] },
		value: 42
	});
});
