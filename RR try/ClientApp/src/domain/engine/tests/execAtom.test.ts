import { lunaLang } from "../lang_processing/lunaLang";
import { IExecCf } from "../lang_processing/cf_impl/execCf";
import { IProgram } from "../lang_processing/cf";
import { executeCf } from "../lang_processing/factories";
import { createExecCf } from "./utils";
import { IArgument, IDfArgument } from "../lang_processing/arguments";

/// def mainSub(firstArg: df)
/// 	external incAtom(innerArgName)
///
/// incAtom(x: int, res: name): void 
///		res = x + 1
///
/// call mainExec(df)


const program: IProgram = {
	atoms: {
		"incAtom": (x: number, res: IDfArgument): void => {
			res.value = x + 1;
		}
	},
	source: {
		"atomSub": {
			type: "extern",
			code: "incAtom",
			args: [
				{ type: 'int' },
				{ type: 'name' }
			]
		}
	}
}

function mainExecWithArgs (arg: IArgument) : IExecCf {
	return createExecCf(execCf => {
		execCf.id.main = "mainExecId";
		execCf.args = [
			{
				type: 'iconst',
				value: 41
			} as lunaLang.IConstArgument,
			arg
		];
		execCf.prototypeName = "atomSub";
		execCf.prototype = program.source[execCf.prototypeName];
		return execCf;
	});
}

it("atom executes and returns on Exec Atom", () => {
	const arg = { 
		type: 'id',
		id: { main: 'somePreviouslyCreatedDf', indicies: [] },
		value: undefined 
	} as IDfArgument
	
	const res = executeCf(mainExecWithArgs(arg), program);
	expect(res.cfs).toHaveLength(0);
	expect(res.dfs).toHaveLength(1);
	expect(res.dfs[0]).toEqual({
		type: 'id',
		id: { main: 'somePreviouslyCreatedDf', indicies: [] },
		value: 42
	} as IDfArgument);
});
