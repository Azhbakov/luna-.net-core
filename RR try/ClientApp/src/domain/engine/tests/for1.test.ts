import { Worker } from '../runtime/worker'
import { for1, for1Atoms } from './tasks/for1';
import { LocalStorage } from '../runtime/localStorage';

/// import c_init(int, name) as init;
/// import c_show(string, int) as show;
/// 
/// sub main() {
///         df f, t;
/// 
///         init(0, f);
///         init(4, t);
/// 
///         for i = f..t
///         {
///                 show("f value is:", f);
///                 show("t value is:", t);
///                 show("i value is:", i);
///         }
/// }

it('for loop works', () => {
	const storage = new LocalStorage();
	const worker = new Worker(for1, for1Atoms, storage);
	worker.step().then(() => { // Execute main(), new CFs: init, init, for
		expect(storage.getCfs()).toHaveLength(3); // init, init, for
		expect(storage.getDfs()).toHaveLength(0);
		return worker.step();
	}).then(() => { // Execute init(0, f), new DF { f, 0 }
		expect(storage.getCfs()).toHaveLength(2); // init, for
		expect(storage.getDfs()).toHaveLength(1); // f
		return worker.step();
	}).then(() => { // Execute init(5, t), new DF { t, 5 }
		expect(storage.getCfs()).toHaveLength(1); // for
		expect(storage.getDfs()).toHaveLength(2); // f, t
		return worker.step();
	}).then(() => { // Execute for 0..5 { show, show, show }
		expect(storage.getCfs()).toHaveLength(3 * 5); // x5 show(f), show(t), show(i)
		expect(storage.getDfs()).toHaveLength(2); // f, t
		return worker.step();
	}).then(() => {
		expect(storage.getCfs()).toHaveLength(3 * 5 - 1);
		expect(storage.getDfs()).toHaveLength(2); // f, t
	});
});
