import { Worker } from '../runtime/worker'
import { indexAndOperations, indexAndOperationsAtoms } from './tasks/indexAndOperations';
import { LocalStorage } from '../runtime/localStorage';

/// ---------------
/// PROGRAM
/// ---------------
/// import c_init(int, name) as init;
/// import c_iprint(int) as iprint;
///
/// sub initialize(name x, int val)
/// {
/// 	init(val, x[6]); 
/// }
///
/// sub display(name y, int idx)
/// 	iprint(y[idx*2][6]);
///
/// sub main()
/// {
/// 	df z;
/// 	initialize(z[10], 5);
/// 	display(z, z[10][6]);
/// }

it('DF indicies are parsed and operations work', () => {
	const storage = new LocalStorage();
	const worker = new Worker(indexAndOperations, indexAndOperationsAtoms, storage);
	worker.step().then(() => { // Execute main()
		expect(storage.getCfs()).toHaveLength(2); // initialize, dispay
		expect(storage.getDfs()).toHaveLength(0);
		return worker.step();
	}).then(() => { // Execute initialize(z[10], 5)
		expect(storage.getCfs()).toHaveLength(2); // display, init
		expect(storage.getDfs()).toHaveLength(0);
		return worker.step();
	}).then(() => { // Execute display(z, z[10][6]) - missing z[10][6]
		expect(storage.getCfs()).toHaveLength(2); // init, display
		expect(storage.getDfs()).toHaveLength(0);
		return worker.step();
	}).then(() => { // Execute init(val == 5, x[6] == z[10][6])
		expect(storage.getCfs()).toHaveLength(1); // display
		expect(storage.getDfs()).toHaveLength(1); // z[10][6]
		return worker.step();
	}).then(() => { // Execute display(z, z[10][6] == 5)
		expect(storage.getCfs()).toHaveLength(1); // iprint
		expect(storage.getDfs()).toHaveLength(1); // z[10][6]
		return worker.step();
	}).then(() => { // Execute iprint(y[idx*2][6]) == z[ z[10][6] * 2 ][6] == z[5*2][6] == z[10][6]
		expect(storage.getCfs()).toHaveLength(0);
		expect(storage.getDfs()).toHaveLength(1); // z[10][6]
		return worker.step();
	});
});
