import { Worker } from '../runtime/worker'
import { LocalStorage } from '../runtime/localStorage';
import { poi1d, poi1dAtoms } from './tasks/poi1d';



it('poi1d works', async () => {
	const storage = new LocalStorage();
	const worker = new Worker(poi1d, poi1dAtoms, storage);	
	await worker.work();
}, 20000);
