import { lunaLang } from "../lang_processing/lunaLang";
import { IExecCf } from "../lang_processing/cf_impl/execCf";
import { IProgram } from "../lang_processing/cf";
import { executeCf } from "../lang_processing/factories";
import { createSubroutine, createExecCf, createExecDecl } from "./utils";
import { IArgument, IDfArgument } from "../lang_processing/arguments";

/// def mainSub(firstArg: int)
/// 	innerExec(firstArg)
///
/// def innerSub(innerArgName: int)
///
/// call mainExec(*differentArgs*)


const program: IProgram = {
	atoms: [],
	source: {

		"execPrototype": createSubroutine(sub => {
			sub.body.push(createExecDecl(execDecl => {
				execDecl.code = "innerSub";	
				execDecl.id = ["innerExecId"];
				execDecl.args.push({
					"ref": [
						"firstArg"
					],
					type: "id"
				} as lunaLang.Argument);
				return execDecl;
			}));
			sub.args.push(
				{
					id: "firstArg",
					type: "int"
				}
			);
			return sub;
		}),

		"innerSub": createSubroutine(sub => {
			sub.args.push({
				type: "int",
				id: "innerArgName"
			});
			return sub;
		})
	}
}

function mainExecWithOneArg (arg: IArgument) : IExecCf {
	return createExecCf(execCf => {
		execCf.id.main = "mainExecId";
		execCf.args.push(
			arg
		);
		execCf.prototypeName = "execPrototype"
		execCf.prototype = program.source[execCf.prototypeName];
		return execCf;
	});
}

function expectedResultForArg (arg: IArgument) : IExecCf {
	return ({
		type: "exec",
		id: {
			main: "mainExecId.innerExecId",
			indicies: []
		},
		prototype: program.source["innerSub"],
		prototypeName: "innerSub",
		args: [
			arg
		]
	})
}

it("const argument passed on Exec Subroutine", () => {
	const constArg = { 
		type: 'iconst', 
		value: 11 
	} as lunaLang.IConstArgument;
	const res = executeCf(mainExecWithOneArg(constArg), program);
	expect(res.dfs).toHaveLength(0);
	expect(res.cfs).toHaveLength(1);
	expect(res.cfs[0]).toEqual(expectedResultForArg(constArg));
});

it("DF argument passed on Exec Subroutine", () => {
	const dfArg = { 
		type: 'id', 
		id: { main: "dfId", indicies: [] },
		value: undefined
	} as IDfArgument;
	const res = executeCf(mainExecWithOneArg(dfArg), program);
	expect(res.dfs).toHaveLength(0);
	expect(res.cfs).toHaveLength(1);
	expect(res.cfs[0]).toEqual(expectedResultForArg(dfArg));
});
