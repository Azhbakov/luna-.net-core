import { lunaLang } from "../../lang_processing/lunaLang";
import { IDf } from '../../lang_processing/cf';

/// ---------------
/// PROGRAM
/// ---------------
/// import c_init(int, name) as init;
/// import c_print(value) as print;
/// import c_iprint(int) as iprint;
///
/// sub main()
/// {
/// 	df x;
///
/// 	print(x);
///	
/// 	cf b[10]: init(7, x);
/// }

export const basic2: lunaLang.Program = {
	"main": {
		type: 'struct',
		args: [],
		body: [
			{
				type: 'dfs',
				names: [ 'x' ]
			} as lunaLang.IDfDeclaration,
			{
				type: 'exec',
				code: 'print',
				args: [
					{
						type: 'id',
						ref: [ 'x' ]
					} as lunaLang.IDfArgument
				],
				id: [ "_l12" ]
			} as lunaLang.IExec,
			{
				type: 'exec',
				code: 'init',
				args: [
					{
						type: 'iconst',
						value: 7
					} as lunaLang.IConstArgument,
					{
						type: 'id',
						ref: [ 'x' ]
					} as lunaLang.IDfArgument
				],
				id: [ 
					"b",
					{
						type: 'iconst',
						value: 12
					} as lunaLang.IConstArgument
				]
			} as lunaLang.IExec,
			{
				type: 'exec',
				code: 'iprint',
				args: [
					{
						type: 'id',
						ref: [ 'x' ]
					} as lunaLang.IDfArgument
				],
				id: [ 'a' ]
			} as lunaLang.IExec
		]
	} as lunaLang.Subroutine,
	
	"init": {
		type: 'extern',
		code: 'c_init',
		args: [
			{ 'type': 'int' },
			{ 'type': 'name' }
		]
	} as lunaLang.Atom,

	"iprint": {
		type: 'extern',
		code: 'c_iprint',
		args: [
			{ 'type': 'int' }
		]
	} as lunaLang.Atom,

	"print": {
		type: 'extern',
		code: 'c_print',
		args: [
			{ 'type': 'value' }
		]
	} as lunaLang.Atom

} as lunaLang.Program

export const basic2Atoms = {

	"c_init": (value: number, df: IDf) => {
		df.value = value;
	},

	"c_print": (df: IDf) => {
		console.log("C_PRINT: " + df);
	},

	"c_iprint": (value: number) => {
		console.log("C_IPRINT: " + value);
	}

}