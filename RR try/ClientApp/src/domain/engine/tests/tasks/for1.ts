import { lunaLang } from "../../lang_processing/lunaLang";
import { IDf } from "../../lang_processing/cf";

/// ---------------
/// PROGRAM
/// ---------------
/// import c_init(int, name) as init;
/// import c_show(string, int) as show;
/// 
/// sub main() {
///         df f, t;
/// 
///         init(0, f);
///         init(4, t);
/// 
///         for i = f..t
///         {
///                 show("f value is:", f);
///                 show("t value is:", t);
///                 show("i value is:", i);
///         }
/// }

export const for1: lunaLang.Program = {
	"main": {
		type: "struct", 
		args: [], 
		body: [
            {
            	type: "dfs", 
                names: [ "f", "t" ]
            } as lunaLang.IDfDeclaration, 
            {
                code: "init", 
                args: [
                    {
                        type: "iconst", 
                        value: 0
                    } as lunaLang.IIntConst, 
                    {
                        ref: ["f"], 
                        type: "id"
                    } as lunaLang.IDfArgument
                ], 
                type: "exec", 
                id: ["_l10"]
            } as lunaLang.IExec, 
            {
                code: "init", 
                args: [
                    {
                        type: "iconst",
                        value: 4
                    } as lunaLang.IIntConst, 
                    {
                        ref: [
                            "t"
                        ], 
                        type: "id"
                    } as lunaLang.IDfArgument
                ],
                type: "exec", 
                id: ["_l11"]
            } as lunaLang.IExec, 
            {
                body: [ 
                    {
                        code: "show",
                        args: [
                            {
                                type: "sconst", 
                                value: "f value is:"
                            } as lunaLang.IStringConst, 
                            {
                                ref: ["f"], 
                                type: "id"
                            } as lunaLang.IDfArgument
                        ],
                        type: "exec", 
                        id: [
                            "_l17"
                        ]
					} as lunaLang.IExec,
					{
                        code: "show",
                        args: [
                            {
                                type: "sconst", 
                                value: "t value is:"
                            } as lunaLang.IStringConst, 
                            {
                                ref: ["t"], 
                                type: "id"
                            } as lunaLang.IDfArgument
                        ],
                        type: "exec", 
                        id: [
                            "_l18"
                        ]
					} as lunaLang.IExec,
					{
                        code: "show",
                        args: [
                            {
                                type: "sconst", 
                                value: "i value is:"
                            } as lunaLang.IStringConst, 
                            {
                                ref: ["i"], 
                                type: "id"
                            } as lunaLang.IDfArgument
                        ],
                        type: "exec", 
                        id: [
                            "_l19"
                        ]
                    } as lunaLang.IExec
				], 
				first: {
                    ref: ["f"], 
                    type: "id"
                } as lunaLang.IDfArgument,
                last: {
                    ref: ["t"], 
                    type: "id"
                } as lunaLang.IDfArgument, 
                type: "for", 
                var: "i",
            } as lunaLang.IFor
        ],
	} as lunaLang.Subroutine,

	"init": {
        type: "extern", 
        code: "c_init",
        args: [{ type: "int" }, { type: "name" }]
    } as lunaLang.Atom,

	"show": {
        type: "extern", 
        code: "c_show", 
        args: [{ type: "string" }, { type: "int" }]
    } as lunaLang.Atom
}

export const for1Atoms = {
	"c_init": (value: number, df: IDf) => {
		df.value = value;
	},

	"c_show": (text: string, value: number) => {
		console.log ("C_SHOW: " + text + " " + value);
	}
}