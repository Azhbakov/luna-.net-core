import { lunaLang } from "../../lang_processing/lunaLang";
import { IIntConst, IDfArgument } from "../../lang_processing/arguments";
import { IDf } from "../../lang_processing/cf";

/// ---------------
/// PROGRAM
/// ---------------
/// import c_init(int, name) as init;
/// import c_iprint(int) as iprint;
///
/// sub initialize(name x, int val)
/// {
/// 	init(val, x[6]); 
/// }
///
/// sub display(name y, int idx)
/// 	iprint(y[idx*2][6]);
///
/// sub main()
/// {
/// 	df z;
/// 	initialize(z[10], 5);
/// 	display(z, z[10][6]);
/// }

export const indexAndOperations: lunaLang.Program = {
	"main": {
        body: [
            {
                type: "dfs", 
                names: ["z"]
            } as lunaLang.IDfDeclaration, 
            {
                code: "initialize",
                args: [
                    {
                        ref: [
                            "z", 
                            {
                                type: "iconst",
                                value: 10
                            } as lunaLang.IIntConst
                        ], 
                        type: "id"
                    } as lunaLang.IDfArgument, 
                    {
                        type: "iconst",
                        value: 5
                    } as IIntConst
                ],
            	type: "exec", 
                id: ["_l18"]
            } as lunaLang.IExec, 
            {
                code: "display",
                args: [
                    {
                        ref: ["z"], 
                        type: "id"
                    } as lunaLang.IDfArgument, 
                    {
                        ref: [
                            "z", 
                            {
                                type: "iconst",
                                value: 10
                            } as IIntConst, 
                            {
                                type: "iconst",
                                value: 6
                            } as IIntConst
                        ], 
                        type: "id"
                    } as lunaLang.IDfArgument
                ], 
                type: "exec", 
                id: ["_l19"]
            } as lunaLang.IExec
        ], 
        type: "struct", 
        args: []
	} as lunaLang.Subroutine,
	
	"initialize": {
        body: [
            {
                type: "dfs", 
                names: []
            } as lunaLang.IDfDeclaration, 
            {
                code: "init",
                args: [
                    {
                        ref: [
                            "val"
                        ], 
                        type: "id"
                    } as lunaLang.IDfArgument, 
                    {
                        ref: [
                            "x", 
                            {
                                type: "iconst",
                                value: 6
                            } as lunaLang.IIntConst
                        ], 
                        type: "id"
                    } as lunaLang.IDfArgument
                ],
                type: "exec", 
                id: ["_l9"]
            } as lunaLang.IExec
        ], 
        type: "struct", 
        args: [
            {
                type: "name", 
                id: "x"
            } as lunaLang.NamedParameter, 
            {
                type: "int", 
                id: "val"
            } as lunaLang.NamedParameter
        ]
    } as lunaLang.Subroutine,

	"display": {
        body: [
            {
                code: "iprint",
                args: [
                    {
                        "ref": [
                            "y", 
                            {
                                "type": "*",
                                "operands": [
                                    {
                                        ref: ["idx"], 
                                        type: "id",
                                    } as lunaLang.IDfArgument, 
                                    {
                                        type: "iconst",
                                        value: 2
                                    } as lunaLang.IIntConst
                                ]
                            } as lunaLang.Expression, 
                            {
                                type: "iconst",
                                value: 6
                            } as IIntConst
                        ], 
                        type: "id"
                    } as lunaLang.IDfArgument
                ],
                type: "exec", 
                id: ["_l13"]
            } as lunaLang.IExec
        ], 
        type: "struct", 
        args: [
            {
                type: "name", 
                id: "y"
            } as lunaLang.NamedParameter, 
            {
                type: "int", 
                id: "idx"
            } as lunaLang.NamedParameter
        ]
	} as lunaLang.Subroutine,
	
	"init": {
        type: "extern", 
        code: "c_init",
        args: [
            { "type": "int" }  as lunaLang.Parameter, 
            { "type": "name" }  as lunaLang.Parameter
        ]
    } as lunaLang.Atom,

	"iprint": {
        type: "extern", 
        code: "c_iprint",
        args: [{ type: "int" } as lunaLang.Parameter]
    } as lunaLang.Atom, 
}

export const indexAndOperationsAtoms = {
	"c_init": (value: number, df: IDf) => {
		df.value = value;
	},

	"c_iprint": (value: number) => {
		console.log("C_IPRINT: " + value);
	}
}