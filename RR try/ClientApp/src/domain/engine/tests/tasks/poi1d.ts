import { lunaLang } from "../../lang_processing/lunaLang";

/*
 Solver for Poisson equation in 3D space with 1D fragmentation.
*/
///
/// import c_set_int(name, int) as set_int;
/// import c_set_real(name, real) as set_real;
///
/// import c_print_int(int) as print_int;
/// import c_print_as_real(value) as print;
///
/// import c_init_distribution(int,int,name #dft, name #sft, name #dim) as init_dist;
/// import c_init_F(int #id,int #n,int #size,value #sft,value #dim, name #F) as init_F;
///
/// import c_print_F(int, int, value, value) as print_F;
/// import c_print_border(int,int,int,value) as print_border;
/// import c_fwd(value,name) as copy_arr;
/// import c_change_border(int #id, value #dim, value #border1,value #border2, value #F, name, int)  as change_border;
/// import c_rmax(real #val1, real #val2, name #out) as rmax;
/// import c_calc_maxdiff(int #id, value #dft , value #sft, value #FF, name #mx, int #L) as calc_maxdiff;
/// import c_ps(int #id, value #sft, value #dim, value #F0, name #FF, name #down, name #up, name #mx, int #L) as ps;
///
/// import c_print_info(int, int, int, int, int) as print_info;
///
/// ##const N 200
/// ##const NY 200
/// ##const NZ 200
/// ##const max 100
/// ##const SIZE 32
/// ##const E 0.9
///
/// sub calc_max(int count, name mx, name maxdif)
/// {
///     set_real(mx[1][0], mx[0][0]);
///     for i = 1 .. count
///         cf rmax[i] : rmax(mx[0][i], mx[1][i - 1], mx[1][i]);
///     set_real(maxdif, mx[1][count]);
/// }
///
/// sub find_maxdiff(int it, name dim, name sft, name FF, name mxd)
/// {
///     df mxc;
///     for i = 0 .. $SIZE - 1
/// 	cf maxdiff[i] : calc_maxdiff(i, dim, sft, FF[i], mxc[0][i], it % 2);
///     calc_max($SIZE - 1, mxc, mxd);
///     //print(mxd);
/// }
///
/// sub puasson(int it, name sft, name dim, name F, name maxdif)
/// {
///     df FF, down, up, mx;
///
///     for i = 0 .. $SIZE - 1
///         cf ps[i] : ps(i, sft, dim, F[it][i], FF[it][i], down[it][(i - 1 + $SIZE) % $SIZE], up[it][(i + 1) % $SIZE], mx[it][0][i], it) --> (F[it][i]);
///
///     for i = 0 .. $SIZE - 1
///         cf border[i] : change_border(i, dim, up[it][i], down[it][i], FF[it][i], F[it+1][i], (it + 1) % 2) --> (FF[it][i]);
///
///     calc_max($SIZE - 1, mx[it], maxdif[it + 1]);
/// }
///
/// sub main()
/// {	
/// 	df dft, sft, dim, F, mx, iter, maxdif;
///
/// 	print_info($N, $NY, $NZ, $SIZE, $max);
///
/// 	init_dist($N, $SIZE, dft, sft, dim);
///
/// 	for i = 0..$SIZE-1
/// 		cf init_f[i] : init_F(i, $N, $SIZE, sft, dim, F[0][i]);
///
///         set_real(maxdif[0], 1.0);
///
/// 	while (int(maxdif[it] > $E) && (it < $max)), it = 0 .. out iter
/// 	{
/// 	    puasson(it, sft, dim, F, maxdif);
/// 	    //print(maxdif[it + 1]) --> (maxdif[it - 1]);
/// 	}
///
///     print_int(iter);
/// 	print(maxdif[iter]);
/// 	find_maxdiff(iter, dim, sft, F[iter], mx);
/// }


export const poi1d: lunaLang.Program = {
    "change_border": {
        "type": "extern", 
        "code": "c_change_border", 
        "nocpu": "false", 
        "cuda_support": "false", 
        "args": [
            {
                "type": "int"
            }, 
            {
                "type": "value", 
                "reuse": "False"
            }, 
            {
                "type": "value", 
                "reuse": "False"
            }, 
            {
                "type": "value", 
                "reuse": "False"
            }, 
            {
                "type": "value", 
                "reuse": "False"
            }, 
            {
                "type": "name"
            }, 
            {
                "type": "int"
            }
        ]
    }, 
    "ps": {
        "type": "extern", 
        "code": "c_ps", 
        "nocpu": "false", 
        "cuda_support": "false", 
        "args": [
            {
                "type": "int"
            }, 
            {
                "type": "value", 
                "reuse": "False"
            }, 
            {
                "type": "value", 
                "reuse": "False"
            }, 
            {
                "type": "value", 
                "reuse": "False"
            }, 
            {
                "type": "name"
            }, 
            {
                "type": "name"
            }, 
            {
                "type": "name"
            }, 
            {
                "type": "name"
            }, 
            {
                "type": "int"
            }
        ]
    }, 
    "main": {
        "body": [
            {
                "type": "dfs", 
                "names": [
                    "dft", 
                    "sft", 
                    "dim", 
                    "F", 
                    "mx", 
                    "iter", 
                    "maxdif"
                ]
            }, 
            {
                "code": "print_info", 
                "rules": [], 
                "args": [
                    {
                        "type": "iconst", 
                        "is_expr": true, 
                        "value": 10
                    }, 
                    {
                        "type": "iconst", 
                        "is_expr": true, 
                        "value": 10
                    }, 
                    {
                        "type": "iconst", 
                        "is_expr": true, 
                        "value": 10
                    }, 
                    {
                        "type": "iconst", 
                        "is_expr": true, 
                        "value": 10
                    }, 
                    {
                        "type": "iconst", 
                        "is_expr": true, 
                        "value": 1000
                    }
                ], 
                "line": 64, 
                "type": "exec", 
                "id": [
                    "_l64"
                ]
            }, 
            {
                "code": "init_dist", 
                "rules": [], 
                "args": [
                    {
                        "type": "iconst", 
                        "is_expr": true, 
                        "value": 10
                    }, 
                    {
                        "type": "iconst", 
                        "is_expr": true, 
                        "value": 10
                    }, 
                    {
                        "ref": [
                            "dft"
                        ], 
                        "type": "id", 
                        "is_expr": true
                    }, 
                    {
                        "ref": [
                            "sft"
                        ], 
                        "type": "id", 
                        "is_expr": true
                    }, 
                    {
                        "ref": [
                            "dim"
                        ], 
                        "type": "id", 
                        "is_expr": true
                    }
                ], 
                "line": 66, 
                "type": "exec", 
                "id": [
                    "_l66"
                ]
            }, 
            {
                "body": [
                    {
                        "code": "init_F", 
                        "rules": [], 
                        "args": [
                            {
                                "ref": [
                                    "i"
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }, 
                            {
                                "type": "iconst", 
                                "is_expr": true, 
                                "value": 10
                            }, 
                            {
                                "type": "iconst", 
                                "is_expr": true, 
                                "value": 10
                            }, 
                            {
                                "ref": [
                                    "sft"
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }, 
                            {
                                "ref": [
                                    "dim"
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }, 
                            {
                                "ref": [
                                    "F", 
                                    {
                                        "type": "iconst", 
                                        "is_expr": true, 
                                        "value": 0
                                    }, 
                                    {
                                        "ref": [
                                            "i"
                                        ], 
                                        "type": "id", 
                                        "is_expr": true
                                    }
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }
                        ], 
                        "line": 69, 
                        "type": "exec", 
                        "id": [
                            "init_f", 
                            {
                                "ref": [
                                    "i"
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }
                        ]
                    }
                ], 
                "last": {
                    "type": "iconst", 
                    "is_expr": true, 
                    "value": 9
                }, 
                "type": "for", 
                "var": "i", 
                "where": {
                    "type": "luna"
                }, 
                "first": {
                    "type": "iconst", 
                    "is_expr": true, 
                    "value": 0
                }
            }, 
            {
                "code": "set_real", 
                "rules": [], 
                "args": [
                    {
                        "ref": [
                            "maxdif", 
                            {
                                "type": "iconst", 
                                "is_expr": true, 
                                "value": 0
                            }
                        ], 
                        "type": "id", 
                        "is_expr": true
                    }, 
                    {
                        "type": "rconst", 
                        "is_expr": true, 
                        "value": 1.0
                    }
                ], 
                "line": 71, 
                "type": "exec", 
                "id": [
                    "_l71"
                ]
            }, 
            {
                "body": [
                    {
                        "code": "puasson", 
                        "rules": [], 
                        "args": [
                            {
                                "ref": [
                                    "it"
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }, 
                            {
                                "ref": [
                                    "sft"
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }, 
                            {
                                "ref": [
                                    "dim"
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }, 
                            {
                                "ref": [
                                    "F"
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }, 
                            {
                                "ref": [
                                    "maxdif"
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }
                        ], 
                        "line": 75, 
                        "type": "exec", 
                        "id": [
                            "_l75"
                        ]
                    }
                ], 
                "wout": {
                    "type": "id", 
                    "ref": [
                        "iter"
                    ]
                }, 
                "where": {
                    "type": "luna"
                }, 
                "start": {
                    "type": "iconst", 
                    "is_expr": true, 
                    "value": 0
                }, 
                "cond": {
                    "type": "&&", 
                    "is_expr": true, 
                    "operands": [
                        {
                            "expr": {
                                "type": ">", 
                                "is_expr": true, 
                                "operands": [
                                    {
                                        "ref": [
                                            "maxdif", 
                                            {
                                                "ref": [
                                                    "it"
                                                ], 
                                                "type": "id", 
                                                "is_expr": true
                                            }
                                        ], 
                                        "type": "id", 
                                        "is_expr": true
                                    }, 
                                    {
                                        "type": "rconst", 
                                        "is_expr": true, 
                                        "value": 0.9
                                    }
                                ]
                            }, 
                            "type": "icast", 
                            "is_expr": true
                        }, 
                        {
                            "type": "<", 
                            "is_expr": true, 
                            "operands": [
                                {
                                    "ref": [
                                        "it"
                                    ], 
                                    "type": "id", 
                                    "is_expr": true
                                }, 
                                {
                                    "type": "iconst", 
                                    "is_expr": true, 
                                    "value": 1000
                                }
                            ]
                        }
                    ]
                }, 
                "var": "it", 
                "type": "while"
            }, 
            {
                "code": "print_int", 
                "rules": [], 
                "args": [
                    {
                        "ref": [
                            "iter"
                        ], 
                        "type": "id", 
                        "is_expr": true
                    }
                ], 
                "line": 79, 
                "type": "exec", 
                "id": [
                    "_l79"
                ]
            }, 
            {
                "code": "print", 
                "rules": [], 
                "args": [
                    {
                        "ref": [
                            "maxdif", 
                            {
                                "ref": [
                                    "iter"
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }
                        ], 
                        "type": "id", 
                        "is_expr": true
                    } as lunaLang.IDfArgument
                ], 
                "line": 80, 
                "type": "exec", 
                "id": [
                    "_l80"
                ]
            } as lunaLang.IExec, 
            {
                "code": "find_maxdiff", 
                "rules": [], 
                "args": [
                    {
                        "ref": [
                            "iter"
                        ], 
                        "type": "id", 
                        "is_expr": true
                    }, 
                    {
                        "ref": [
                            "dim"
                        ], 
                        "type": "id", 
                        "is_expr": true
                    }, 
                    {
                        "ref": [
                            "sft"
                        ], 
                        "type": "id", 
                        "is_expr": true
                    }, 
                    {
                        "ref": [
                            "F", 
                            {
                                "ref": [
                                    "iter"
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }
                        ], 
                        "type": "id", 
                        "is_expr": true
                    }, 
                    {
                        "ref": [
                            "mx"
                        ], 
                        "type": "id", 
                        "is_expr": true
                    }
                ], 
                "line": 81, 
                "type": "exec", 
                "id": [
                    "_l81"
                ]
            }
        ], 
        "type": "struct", 
        "args": [], 
        "where": {
            "type": "luna"
        }
    }, 
    "set_int": {
        "type": "extern", 
        "code": "c_set_int", 
        "nocpu": "false", 
        "cuda_support": "false", 
        "args": [
            {
                "type": "name"
            }, 
            {
                "type": "int"
            }
        ]
    }, 
    "find_maxdiff": {
        "body": [
            {
                "type": "dfs", 
                "names": [
                    "mxc"
                ]
            }, 
            {
                "body": [
                    {
                        "code": "calc_maxdiff", 
                        "rules": [], 
                        "args": [
                            {
                                "ref": [
                                    "i"
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }, 
                            {
                                "ref": [
                                    "dim"
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }, 
                            {
                                "ref": [
                                    "sft"
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }, 
                            {
                                "ref": [
                                    "FF", 
                                    {
                                        "ref": [
                                            "i"
                                        ], 
                                        "type": "id", 
                                        "is_expr": true
                                    }
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }, 
                            {
                                "ref": [
                                    "mxc", 
                                    {
                                        "type": "iconst", 
                                        "is_expr": true, 
                                        "value": 0
                                    }, 
                                    {
                                        "ref": [
                                            "i"
                                        ], 
                                        "type": "id", 
                                        "is_expr": true
                                    }
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }, 
                            {
                                "type": "%", 
                                "is_expr": true, 
                                "operands": [
                                    {
                                        "ref": [
                                            "it"
                                        ], 
                                        "type": "id", 
                                        "is_expr": true
                                    }, 
                                    {
                                        "type": "iconst", 
                                        "is_expr": true, 
                                        "value": 2
                                    }
                                ]
                            }
                        ], 
                        "line": 42, 
                        "type": "exec", 
                        "id": [
                            "maxdiff", 
                            {
                                "ref": [
                                    "i"
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }
                        ]
                    }
                ], 
                "last": {
                    "type": "iconst", 
                    "is_expr": true, 
                    "value": 9
                }, 
                "type": "for", 
                "var": "i", 
                "where": {
                    "type": "luna"
                }, 
                "first": {
                    "type": "iconst", 
                    "is_expr": true, 
                    "value": 0
                }
            }, 
            {
                "code": "calc_max", 
                "rules": [], 
                "args": [
                    {
                        "type": "iconst", 
                        "is_expr": true, 
                        "value": 9
                    }, 
                    {
                        "ref": [
                            "mxc"
                        ], 
                        "type": "id", 
                        "is_expr": true
                    } as lunaLang.IDfArgument, 
                    {
                        "ref": [
                            "mxd"
                        ], 
                        "type": "id", 
                        "is_expr": true
                    }
                ], 
                "line": 43, 
                "type": "exec", 
                "id": [
                    "_l43"
                ]
            } as lunaLang.IExec
        ], 
        "type": "struct", 
        "args": [
            {
                "type": "int", 
                "id": "it"
            }, 
            {
                "type": "name", 
                "id": "dim"
            }, 
            {
                "type": "name", 
                "id": "sft"
            }, 
            {
                "type": "name", 
                "id": "FF"
            }, 
            {
                "type": "name", 
                "id": "mxd"
            }
        ], 
        "where": {
            "type": "luna"
        }
    }, 
    "calc_maxdiff": {
        "type": "extern", 
        "code": "c_calc_maxdiff", 
        "nocpu": "false", 
        "cuda_support": "false", 
        "args": [
            {
                "type": "int"
            }, 
            {
                "type": "value", 
                "reuse": "False"
            }, 
            {
                "type": "value", 
                "reuse": "False"
            }, 
            {
                "type": "value", 
                "reuse": "False"
            }, 
            {
                "type": "name"
            }, 
            {
                "type": "int"
            }
        ]
    }, 
    "print_int": {
        "type": "extern", 
        "code": "c_print_int", 
        "nocpu": "false", 
        "cuda_support": "false", 
        "args": [
            {
                "type": "int"
            }
        ]
    }, 
    "rmax": {
        "type": "extern", 
        "code": "c_rmax", 
        "nocpu": "false", 
        "cuda_support": "false", 
        "args": [
            {
                "type": "real"
            }, 
            {
                "type": "real"
            }, 
            {
                "type": "name"
            }
        ]
    }, 
    "copy_arr": {
        "type": "extern", 
        "code": "c_fwd", 
        "nocpu": "false", 
        "cuda_support": "false", 
        "args": [
            {
                "type": "value", 
                "reuse": "False"
            }, 
            {
                "type": "name"
            }
        ]
    }, 
    "calc_max": {
        "body": [
            {
                "type": "dfs", 
                "names": []
            }, 
            {
                "code": "set_real", 
                "rules": [], 
                "args": [
                    {
                        "ref": [
                            "mx", 
                            {
                                "type": "iconst", 
                                "is_expr": true, 
                                "value": 1
                            }, 
                            {
                                "type": "iconst", 
                                "is_expr": true, 
                                "value": 0
                            }
                        ], 
                        "type": "id", 
                        "is_expr": true
                    }, 
                    {
                        "ref": [
                            "mx", 
                            {
                                "type": "iconst", 
                                "is_expr": true, 
                                "value": 0
                            }, 
                            {
                                "type": "iconst", 
                                "is_expr": true, 
                                "value": 0
                            }
                        ], 
                        "type": "id", 
                        "is_expr": true
                    }
                ], 
                "line": 32, 
                "type": "exec", 
                "id": [
                    "_l32"
                ]
            }, 
            {
                "body": [
                    {
                        "code": "rmax", 
                        "rules": [], 
                        "args": [
                            {
                                "ref": [
                                    "mx", 
                                    {
                                        "type": "iconst", 
                                        "is_expr": true, 
                                        "value": 0
                                    }, 
                                    {
                                        "ref": [
                                            "i"
                                        ], 
                                        "type": "id", 
                                        "is_expr": true
                                    }
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }, 
                            {
                                "ref": [
                                    "mx", 
                                    {
                                        "type": "iconst", 
                                        "is_expr": true, 
                                        "value": 1
                                    }, 
                                    {
                                        "type": "-", 
                                        "is_expr": true, 
                                        "operands": [
                                            {
                                                "ref": [
                                                    "i"
                                                ], 
                                                "type": "id", 
                                                "is_expr": true
                                            }, 
                                            {
                                                "type": "iconst", 
                                                "is_expr": true, 
                                                "value": 1
                                            }
                                        ]
                                    }
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }, 
                            {
                                "ref": [
                                    "mx", 
                                    {
                                        "type": "iconst", 
                                        "is_expr": true, 
                                        "value": 1
                                    }, 
                                    {
                                        "ref": [
                                            "i"
                                        ], 
                                        "type": "id", 
                                        "is_expr": true
                                    }
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }
                        ], 
                        "line": 34, 
                        "type": "exec", 
                        "id": [
                            "rmax", 
                            {
                                "ref": [
                                    "i"
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }
                        ]
                    }
                ], 
                "last": {
                    "ref": [
                        "count"
                    ], 
                    "type": "id", 
                    "is_expr": true
                }, 
                "type": "for", 
                "var": "i", 
                "where": {
                    "type": "luna"
                }, 
                "first": {
                    "type": "iconst", 
                    "is_expr": true, 
                    "value": 1
                }
            }, 
            {
                "code": "set_real", 
                "rules": [], 
                "args": [
                    {
                        "ref": [
                            "maxdif"
                        ], 
                        "type": "id", 
                        "is_expr": true
                    }, 
                    {
                        "ref": [
                            "mx", 
                            {
                                "type": "iconst", 
                                "is_expr": true, 
                                "value": 1
                            }, 
                            {
                                "ref": [
                                    "count"
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }
                        ], 
                        "type": "id", 
                        "is_expr": true
                    } as lunaLang.IDfArgument
                ], 
                "line": 35, 
                "type": "exec", 
                "id": [
                    "_l35"
                ]
            } as lunaLang.IExec
        ], 
        "type": "struct", 
        "args": [
            {
                "type": "int", 
                "id": "count"
            }, 
            {
                "type": "name", 
                "id": "mx"
            }, 
            {
                "type": "name", 
                "id": "maxdif"
            }
        ], 
        "where": {
            "type": "luna"
        }
    }, 
    "init_dist": {
        "type": "extern", 
        "code": "c_init_distribution", 
        "nocpu": "false", 
        "cuda_support": "false", 
        "args": [
            {
                "type": "int"
            }, 
            {
                "type": "int"
            }, 
            {
                "type": "name"
            }, 
            {
                "type": "name"
            }, 
            {
                "type": "name"
            }
        ]
    }, 
    "print_info": {
        "type": "extern", 
        "code": "c_print_info", 
        "nocpu": "false", 
        "cuda_support": "false", 
        "args": [
            {
                "type": "int"
            }, 
            {
                "type": "int"
            }, 
            {
                "type": "int"
            }, 
            {
                "type": "int"
            }, 
            {
                "type": "int"
            }
        ]
    }, 
    "init_F": {
        "type": "extern", 
        "code": "c_init_F", 
        "nocpu": "false", 
        "cuda_support": "false", 
        "args": [
            {
                "type": "int"
            }, 
            {
                "type": "int"
            }, 
            {
                "type": "int"
            }, 
            {
                "type": "value", 
                "reuse": "False"
            }, 
            {
                "type": "value", 
                "reuse": "False"
            }, 
            {
                "type": "name"
            }
        ]
    }, 
    "print": {
        "type": "extern", 
        "code": "c_print_as_real", 
        "nocpu": "false", 
        "cuda_support": "false", 
        "args": [
            {
                "type": "value", 
                "reuse": "False"
            } as lunaLang.Parameter
        ]
    } as lunaLang.Atom, 
    "print_F": {
        "type": "extern", 
        "code": "c_print_F", 
        "nocpu": "false", 
        "cuda_support": "false", 
        "args": [
            {
                "type": "int"
            }, 
            {
                "type": "int"
            }, 
            {
                "type": "value", 
                "reuse": "False"
            }, 
            {
                "type": "value", 
                "reuse": "False"
            }
        ]
    }, 
    "set_real": {
        "type": "extern", 
        "code": "c_set_real", 
        "nocpu": "false", 
        "cuda_support": "false", 
        "args": [
            {
                "type": "name"
            }, 
            {
                "type": "real"
            }
        ]
    }, 
    "puasson": {
        "body": [
            {
                "type": "dfs", 
                "names": [
                    "FF", 
                    "down", 
                    "up", 
                    "mx"
                ]
            }, 
            {
                "body": [
                    {
                        "code": "ps", 
                        "rules": [
                            {
                                "ruletype": "indexed", 
                                "type": "rule", 
                                "dfs": [
                                    {
                                        "ref": [
                                            "F", 
                                            {
                                                "ref": [
                                                    "it"
                                                ], 
                                                "type": "id", 
                                                "is_expr": true
                                            }, 
                                            {
                                                "ref": [
                                                    "i"
                                                ], 
                                                "type": "id", 
                                                "is_expr": true
                                            }
                                        ], 
                                        "type": "id", 
                                        "is_expr": true
                                    }
                                ]
                            }
                        ], 
                        "args": [
                            {
                                "ref": [
                                    "i"
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }, 
                            {
                                "ref": [
                                    "sft"
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }, 
                            {
                                "ref": [
                                    "dim"
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }, 
                            {
                                "ref": [
                                    "F", 
                                    {
                                        "ref": [
                                            "it"
                                        ], 
                                        "type": "id", 
                                        "is_expr": true
                                    }, 
                                    {
                                        "ref": [
                                            "i"
                                        ], 
                                        "type": "id", 
                                        "is_expr": true
                                    }
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }, 
                            {
                                "ref": [
                                    "FF", 
                                    {
                                        "ref": [
                                            "it"
                                        ], 
                                        "type": "id", 
                                        "is_expr": true
                                    }, 
                                    {
                                        "ref": [
                                            "i"
                                        ], 
                                        "type": "id", 
                                        "is_expr": true
                                    }
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }, 
                            {
                                "ref": [
                                    "down", 
                                    {
                                        "ref": [
                                            "it"
                                        ], 
                                        "type": "id", 
                                        "is_expr": true
                                    }, 
                                    {
                                        "type": "%", 
                                        "is_expr": true, 
                                        "operands": [
                                            {
                                                "type": "+", 
                                                "is_expr": true, 
                                                "operands": [
                                                    {
                                                        "type": "-", 
                                                        "is_expr": true, 
                                                        "operands": [
                                                            {
                                                                "ref": [
                                                                    "i"
                                                                ], 
                                                                "type": "id", 
                                                                "is_expr": true
                                                            }, 
                                                            {
                                                                "type": "iconst", 
                                                                "is_expr": true, 
                                                                "value": 1
                                                            }
                                                        ]
                                                    }, 
                                                    {
                                                        "type": "iconst", 
                                                        "is_expr": true, 
                                                        "value": 10
                                                    }
                                                ]
                                            }, 
                                            {
                                                "type": "iconst", 
                                                "is_expr": true, 
                                                "value": 10
                                            }
                                        ]
                                    }
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }, 
                            {
                                "ref": [
                                    "up", 
                                    {
                                        "ref": [
                                            "it"
                                        ], 
                                        "type": "id", 
                                        "is_expr": true
                                    }, 
                                    {
                                        "type": "%", 
                                        "is_expr": true, 
                                        "operands": [
                                            {
                                                "type": "+", 
                                                "is_expr": true, 
                                                "operands": [
                                                    {
                                                        "ref": [
                                                            "i"
                                                        ], 
                                                        "type": "id", 
                                                        "is_expr": true
                                                    }, 
                                                    {
                                                        "type": "iconst", 
                                                        "is_expr": true, 
                                                        "value": 1
                                                    }
                                                ]
                                            }, 
                                            {
                                                "type": "iconst", 
                                                "is_expr": true, 
                                                "value": 10
                                            }
                                        ]
                                    }
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }, 
                            {
                                "ref": [
                                    "mx", 
                                    {
                                        "ref": [
                                            "it"
                                        ], 
                                        "type": "id", 
                                        "is_expr": true
                                    }, 
                                    {
                                        "type": "iconst", 
                                        "is_expr": true, 
                                        "value": 0
                                    }, 
                                    {
                                        "ref": [
                                            "i"
                                        ], 
                                        "type": "id", 
                                        "is_expr": true
                                    }
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }, 
                            {
                                "ref": [
                                    "it"
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }
                        ], 
                        "line": 52, 
                        "type": "exec", 
                        "id": [
                            "ps", 
                            {
                                "ref": [
                                    "i"
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }
                        ]
                    }
                ], 
                "last": {
                    "type": "iconst", 
                    "is_expr": true, 
                    "value": 9
                }, 
                "type": "for", 
                "var": "i", 
                "where": {
                    "type": "luna"
                }, 
                "first": {
                    "type": "iconst", 
                    "is_expr": true, 
                    "value": 0
                }
            }, 
            {
                "body": [
                    {
                        "code": "change_border", 
                        "rules": [
                            {
                                "ruletype": "indexed", 
                                "type": "rule", 
                                "dfs": [
                                    {
                                        "ref": [
                                            "FF", 
                                            {
                                                "ref": [
                                                    "it"
                                                ], 
                                                "type": "id", 
                                                "is_expr": true
                                            }, 
                                            {
                                                "ref": [
                                                    "i"
                                                ], 
                                                "type": "id", 
                                                "is_expr": true
                                            }
                                        ], 
                                        "type": "id", 
                                        "is_expr": true
                                    }
                                ]
                            }
                        ], 
                        "args": [
                            {
                                "ref": [
                                    "i"
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }, 
                            {
                                "ref": [
                                    "dim"
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }, 
                            {
                                "ref": [
                                    "up", 
                                    {
                                        "ref": [
                                            "it"
                                        ], 
                                        "type": "id", 
                                        "is_expr": true
                                    }, 
                                    {
                                        "ref": [
                                            "i"
                                        ], 
                                        "type": "id", 
                                        "is_expr": true
                                    }
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }, 
                            {
                                "ref": [
                                    "down", 
                                    {
                                        "ref": [
                                            "it"
                                        ], 
                                        "type": "id", 
                                        "is_expr": true
                                    }, 
                                    {
                                        "ref": [
                                            "i"
                                        ], 
                                        "type": "id", 
                                        "is_expr": true
                                    }
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }, 
                            {
                                "ref": [
                                    "FF", 
                                    {
                                        "ref": [
                                            "it"
                                        ], 
                                        "type": "id", 
                                        "is_expr": true
                                    }, 
                                    {
                                        "ref": [
                                            "i"
                                        ], 
                                        "type": "id", 
                                        "is_expr": true
                                    }
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }, 
                            {
                                "ref": [
                                    "F", 
                                    {
                                        "type": "+", 
                                        "is_expr": true, 
                                        "operands": [
                                            {
                                                "ref": [
                                                    "it"
                                                ], 
                                                "type": "id", 
                                                "is_expr": true
                                            }, 
                                            {
                                                "type": "iconst", 
                                                "is_expr": true, 
                                                "value": 1
                                            }
                                        ]
                                    }, 
                                    {
                                        "ref": [
                                            "i"
                                        ], 
                                        "type": "id", 
                                        "is_expr": true
                                    }
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }, 
                            {
                                "type": "%", 
                                "is_expr": true, 
                                "operands": [
                                    {
                                        "type": "+", 
                                        "is_expr": true, 
                                        "operands": [
                                            {
                                                "ref": [
                                                    "it"
                                                ], 
                                                "type": "id", 
                                                "is_expr": true
                                            }, 
                                            {
                                                "type": "iconst", 
                                                "is_expr": true, 
                                                "value": 1
                                            }
                                        ]
                                    }, 
                                    {
                                        "type": "iconst", 
                                        "is_expr": true, 
                                        "value": 2
                                    }
                                ]
                            }
                        ], 
                        "line": 55, 
                        "type": "exec", 
                        "id": [
                            "border", 
                            {
                                "ref": [
                                    "i"
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }
                        ]
                    }
                ], 
                "last": {
                    "type": "iconst", 
                    "is_expr": true, 
                    "value": 9
                }, 
                "type": "for", 
                "var": "i", 
                "where": {
                    "type": "luna"
                }, 
                "first": {
                    "type": "iconst", 
                    "is_expr": true, 
                    "value": 0
                }
            }, 
            {
                "code": "calc_max", 
                "rules": [], 
                "args": [
                    {
                        "type": "iconst", 
                        "is_expr": true, 
                        "value": 9
                    }, 
                    {
                        "ref": [
                            "mx", 
                            {
                                "ref": [
                                    "it"
                                ], 
                                "type": "id", 
                                "is_expr": true
                            }
                        ], 
                        "type": "id", 
                        "is_expr": true
                    }, 
                    {
                        "ref": [
                            "maxdif", 
                            {
                                "type": "+", 
                                "is_expr": true, 
                                "operands": [
                                    {
                                        "ref": [
                                            "it"
                                        ], 
                                        "type": "id", 
                                        "is_expr": true
                                    }, 
                                    {
                                        "type": "iconst", 
                                        "is_expr": true, 
                                        "value": 1
                                    }
                                ]
                            }
                        ], 
                        "type": "id", 
                        "is_expr": true
                    } as lunaLang.IDfArgument
                ], 
                "line": 57, 
                "type": "exec", 
                "id": [
                    "_l57"
                ]
            } as lunaLang.IExec
        ], 
        "type": "struct", 
        "args": [
            {
                "type": "int", 
                "id": "it"
            }, 
            {
                "type": "name", 
                "id": "sft"
            }, 
            {
                "type": "name", 
                "id": "dim"
            }, 
            {
                "type": "name", 
                "id": "F"
            }, 
            {
                "type": "name", 
                "id": "maxdif"
            }
        ], 
        "where": {
            "type": "luna"
        }
    }, 
    "print_border": {
        "type": "extern", 
        "code": "c_print_border", 
        "nocpu": "false", 
        "cuda_support": "false", 
        "args": [
            {
                "type": "int"
            }, 
            {
                "type": "int"
            }, 
            {
                "type": "int"
            }, 
            {
                "type": "value", 
                "reuse": "False"
            }
        ]
    }
} as lunaLang.Program


// -----------------------------------------------------------------
const atoms = {} as any

atoms.N = 10;
atoms.NY = 10;
atoms.NZ = 10;
atoms.X = 2.0;
atoms.Y = 2.0;
atoms.Z = 2.0;
atoms.SIZE = 10;
atoms.a = 1;
atoms.EPS = 0.9;

atoms.hx = atoms.X/atoms.N;
atoms.hy = atoms.Y/atoms.NY;
atoms.hz = atoms.Z/atoms.NZ;

atoms.owx = atoms.hx*atoms.hx;
atoms.owy = atoms.hy*atoms.hy;
atoms.owz = atoms.hz*atoms.hz;
atoms.c = 2./atoms.owx + 2./atoms.owy + 2./atoms.owz + atoms.a;


/*intArg, nameArg*/
atoms.c_init = (intArg: any, df: any) => { // nameArg aka returnPlace {externPrefix, externName, externIndices}
	df.value = intArg;
}

atoms.c_show = (arg1: any, arg2: any) => { // nameArg aka returnPlace {externPrefix, externName, externIndices}
	console.log ("C_SHOW: " + arg1 + " " + arg2);
}

// int nx, int ny, int nz, int num_of_fragments, int max_iter
atoms.c_print_info = (nx: any, ny: any, nz: any, num_of_fragments: any, max_iter: any) => {
	// const nx = args[0];
	// const ny = args[1];
	// const nz = args[2];
	// const num_of_fragments = args[3];
	// const max_iter = args[4];

	let output = "Size: " + nx + " " + ny + " " + nz + "\n";
	output += "Fragments: " + num_of_fragments + "\n";
	output += "Iterations: " + max_iter;

	console.log ("C_PRINT_INFO: " + output);
}


// int id, InputDF &df1, InputDF &df2, InputDF &df3, OutputDF &df4, int L 
atoms.c_calc_maxdiff = (id: any, df1: any, df2: any, df3: any, df4: any, L: any) => {
	// ARGS
	// const id = args[0];
	// const df1 = args[1];
	// const df2 = args[2];
	// const df3 = args[3];
	// const df4 = args[4];
	// const L = args[5];

	
    const jn = atoms.NY;
    const kn = atoms.NZ;

    const dft = df1;//df1.getData<int>();
    const sft = df2;//df2.getData<int>();
    const F = df3;//df3.getData<double>();
            
    const dt = atoms.calc_div(id, atoms.SIZE, sft);

    let mx = 0.0;
    for (let i = 1; i < dft[id] - 1; i++) {
        for (let j = 1; j < jn - 1; j++) {
            for (let k = 1; k < kn-1; k++) {
            	const defF = F[(dft[id]*jn*kn) * L + i*jn*kn + j*kn + k];
                const F1 = /*atoms.fabs*/Math.abs(defF - atoms.Fresh((i + dt)*atoms.hx, j*atoms.hy, k*atoms.hz));
                if (F1 > mx) {
                    mx = F1;
                }
    	    }
        }
    }

    df4.value = mx;
}


// int n, int size, OutputDF &df3, OutputDF &df4, OutputDF &df5
atoms.c_init_distribution = (n: any, size: any, df3: any, df4: any, df5: any) => {  
	// ARGS
    // const n = args[0];
    // const size = args[1];
    // const df3 = args[2];
    // const df4 = args[3];
    // const df5 = args[4];

    df3.value = [];//df3.create<int>(size);
    
    for (let i = 0; i < size; i++) {
        df3.value[i] = Math.floor(n/size) + ((i < (n % size)) as any);
	}
        
    df4.value = [];//df4.create<int>(size + 1);

    df4.value[0] = 0;
    for (let i = 1; i <= size; i++) {
        df4.value[i] = df4.value[i - 1] + df3.value[i - 1];
    }
        
    df5.value = [];//df5.create<int>(size);
    
    df5.value[0] = df3.value[0] + 1;
    df5.value[size - 1] = df3.value[size - 1] + 1;
    for (let i = 1; i < size - 1; i++)
        df5.value[i] = df3.value[i] + 2;

    if (size === 1) {
        df5.value[0]--;
    }
}


// double *F, int in0, int in1, int fi0, int fi1, int ki, int dt, int jn, int kn
atoms.inic = (F: any, in0: any, in1: any, fi0: any, fi1: any, ki: any, dt: any, jn: any, kn: any) => {	
    // ARGS
    // const F = args[0];
    // const in0 = args[1];
    // const in1 = args[2];
    // const fi0 = args[3];
    // const fi1 = args[4];
    // const ki = args[5];
    // const dt = args[6];
    // const jn = args[7];
    // const kn = args[8];

    
    for (let i = in0; i < in1; i++) {
        for (let j = 0; j < jn; j++) {
            for (let k = 0; k < kn; k++) {
                if((i !== fi0) && (j !== 0) && (k !== 0) && (k !== kn - 1) && (i !== fi1) && (j !== jn - 1)) {
                    F[i*jn*kn + j*kn + k] = 0.0;
                    F[ki*jn*kn + i*jn*kn + j*kn + k] = 0.0;
                } else {
                    F[i*jn*kn + j*kn + k] = atoms.Fresh((i + dt)*atoms.hx, j*atoms.hy, k*atoms.hz);
                    F[ki*jn*kn + i*jn*kn + j*kn + k] = atoms.Fresh((i + dt)*atoms.hx, j*atoms.hy, k*atoms.hz);
                }
            }
        }
    }
}


// int id, int n, InputDF& df1, InputDF& df2
atoms.c_print_F = (id: any, n: any, df1: any, df2: any) => {
	// ARGS
	// const id = args[0];
	// const n = args[1];
	// const df1 = args[2];
	// const df2 = args[3];

	
    const totaldim = df1;//df1.getData<int>();
    const df2_data = df2;
    
    let output = "F" + id + "\n";
    
    for(let i =0; i < totaldim[id]; i++) {
		for(let j = 0; j < atoms.NY; j++) {
		    for(let k = 0; k < atoms.NZ; k++) {
				output += df2_data[i*atoms.NY*atoms.NZ+atoms.NZ*j+k] + " ";
		    }
		    output += "\n";
		}
		output += "\n";
	}
	
	output += "-------------\n";
	output += "-------------\n";

    for(let i = 0; i < totaldim[id]; i++) {
		for(let j = 0; j < atoms.NY; j++) {
		    for(let k = 0; k < atoms.NZ; k++) {
				output += df2_data[ (atoms.NY*atoms.NZ*totaldim[id])*1+i*atoms.NZ*atoms.NY+atoms.NZ*j+k] + " ";
		    }
			output += "\n";
		}
		output += "\n";
    }
 
	console.log ("C_PRINT_F: " + output);
}


// int id, int nx, int ny, InputDF &df1
atoms.c_print_border = (id: any, nx: any, ny: any, df1: any) => {
    // ARGS
    // const id = args[0];
    // const nx = args[1];
    // const ny = args[2];
    // const df1  = args[3];

    let output = "br" + id + "\n";

    if(df1.length !== 0) {
        const df1_data = df1;
        for (let j = 1; j < nx - 1; j++) {
            for (let k = 1; k < ny - 1; k++)
                output += df1_data[ny*j + k] + "\n";
            output += "\n";
        }
    } else {
        output += "Buffer is empty\n";
    }

	console.log ("C_PRINT_BORDER: " + output);
}

// int id, int n, int size, InputDF &df_sft, InputDF &df_dim, OutputDF &df_f
atoms.c_init_F = (id: any, n: any, size: any, df_sft: any, df_dim: any, df_f: any) => {
    // ARGS
    // const id = args[0];
    // const n = args[1];
    // const size = args[2];
    // const df_sft = args[3];
    // const df_dim = args[4];
    // const df_f = args[5];
 
    const sft = df_sft;//.getData<int>();
    const dim = df_dim;//.getData<int>();     
    
    const dt = atoms.calc_div(id, size, sft);
    
    df_f.value = [];
    for (let q = 0; q < 2*atoms.NY*atoms.NZ*dim[id]; q++) {
        df_f.value[q] = 0;
    }
    const F = df_f.value;//df_f.create<double>(2*NY*NZ*dim[id]);
    
    if(id === 0) {
        atoms.inic(F, 0, dim[id], 0, -1, dim[id], dt, atoms.NY, atoms.NZ);
    } else {
        if(id === size - 1)
            atoms.inic(F, 0, dim[id], -1, dim[id]-1, dim[id], dt, atoms.NY, atoms.NZ);
        else
            atoms.inic(F, 0, dim[id], -1, -1, dim[id], dt, atoms.NY, atoms.NZ);
    }
    
}

// OutputDF &df, double val
atoms.c_set_real = (df: any, val: any) => {
    df.value = val;
}

// OutputDF &df, int val
atoms.c_set_int = (df: any, val: any) => {
    df.value = val;
}

// InputDF &df
atoms.c_print = (df: any) => {
    const output = /*args[0].globalName + " is " + */df;

	console.log ("C_PRINT: " + output);
}

// int val
atoms.c_print_int = (output: any) => {
    // const output = args[0];

	console.log ("C_PRINT_INT: " + output);
}

// double val
atoms.c_print_real = (output: any) => {
    //const output = args[0];

	console.log ("C_PRINT_REAL: " + output);
}

// InputDF &df)
atoms.c_print_as_real = (output: any) => {
    //const output = args[0];

	console.log ("C_PRINT_AS_REAL: " + output);
}

// double x, double y, OutputDF &df
atoms.c_rmax = (x: any, y: any, df: any) => {
	// ARGS
	// const x = args[0];
	// const y = args[1];
	// const df = args[2];
    const toPass = x > y ? x : y;
    atoms.c_set_real(df, toPass);
}

// const double *up, double *F, int dim, int L
atoms.change_up = (up: any, F: any, dim: any, L: any) => {
	// ARGS
	// const up = args[0];
	// const F = args[1];
	// const dim = args[2];
	// const L = args[3];

	
    const i = 0;
    for (let j = 0; j < atoms.NY; j++)
        for (let k = 0; k < atoms.NZ; k++)
            F[(dim*atoms.NZ*atoms.NY)*L + i*atoms.NY*atoms.NZ + j*atoms.NZ + k] = up[j*atoms.NZ + k];
}

// const double *down, double *F, int dim, int L
atoms.change_down = (down: any, F: any, dim: any, L: any) => {
	// ARGS
	// const down = args[0];
	// const F = args[1];
	// const dim = args[2];
	// const L = args[3];

	
    const i = dim - 1;
    for (let j = 0; j < atoms.NY; j++)
        for (let k = 0; k < atoms.NZ; k++)
            F[(dim*atoms.NY*atoms.NZ)*L + i*atoms.NY*atoms.NZ + j*atoms.NZ + k] = down[j*atoms.NZ + k];
}

/*
int id,
luna::ucenv::InputDF &df_dim,		//dim
luna::ucenv::InputDF &df_border1,		//border1
luna::ucenv::InputDF &df_border2,		//border2
luna::ucenv::InputDF &df_f,		//F
luna::ucenv::OutputDF &df_ff, 		//FF
int L
*/
atoms.c_change_border = (id: any, df_dim: any, df_border1: any, df_border2: any, df_f: any, df_ff: any, L: any) => {
	// ARGS
	// const id = args[0];
	// const df_dim = args[1];
	// const df_border1 = args[2];
	// const df_border2 = args[3];
	// const df_f = args[4];
	// const df_ff = args[5];
    // const L = args[6];

    const dim = df_dim;//.getData<int>();
    const border1 = df_border1;//.getData<double>();
    const border2 = df_border2;//.getData<double>();
    
    //df_ff.copy(df_f);
    df_ff.value = [];
   	for (let i = 0; i < df_f.length; i++) {
   		df_ff.value[i] = df_f[i];
   	}

    const F = df_ff.value;//getData<double>();
        
    if(border2 !== null)
        atoms.change_down(border2, F, dim[id], L);
    
    if(border1!== null)
        atoms.change_up(border1, F, dim[id], L);
}

/*
int id,
luna::ucenv::InputDF &df_sft,		//sft
luna::ucenv::InputDF &df_dim,		//dim
luna::ucenv::InputDF &df3,		//F[it]
luna::ucenv::OutputDF &df4,		//F[it+1]
luna::ucenv::OutputDF &df_down,		//down[it+1][i-1]
luna::ucenv::OutputDF &df_up,		//up[it+1][i+1]
luna::ucenv::OutputDF &df_max, 	//max
int iter
*/
atoms.c_ps = (id: any, df_sft: any, df_dim: any, df3: any, df4: any, df_down: any, df_up: any, df_max: any, iter: any) => {
	// ARGS
	// const id = args[0];
	// const df_sft = args[1];		//sft
	// const df_dim = args[2];		//dim
	// const df3 = args[3];		//F[it]
	// const df4 = args[4];		//F[it+1]
	// const df_down = args[5];		//down[it+1][i-1]
	// const df_up = args[6];		//up[it+1][i+1]
	// const df_max = args[7]; 	//max
	// const iter = args[8];

	
    const jn = atoms.NY;
    const kn = atoms.NZ;

    const sft = df_sft;//.getData<int>();
    const dim = df_dim;//.getData<int>();
        
    const dt = atoms.calc_div(id, atoms.SIZE, sft);
            
    const L0 = iter % 2;
    const L1 = 1 - L0;

    //df4.copy(df3);
    df4.value = [];
   	for (let i = 0; i < df3.length; i++) {
   		df4.value[i] = df3[i];
   	}

    const F = df4.value;//.getData<double>();

    //printf("%s %s %s %s\n", df_sft.getCName(), df_dim.getCName(), df3.getCName(), df4.getCName());

//#define F(l,i,j,k) F[(dim[id]*jn*kn)*l+i*jn*kn+j*kn+k]

    let mx = 0.0;
    for (let i = 1; i <= dim[id] - 2; i++) {
        for (let j = 1; j < jn - 1; j++) {
            for (let k = 1; k < kn - 1; k++) {

            	let deff1 = F[(dim[id]*jn*kn)*L0 + (i+1)*jn*kn + j*kn + k];
            	let deff2 = F[(dim[id]*jn*kn)*L0 + (i-1)*jn*kn + j*kn + k];
                const Fi = (deff1 + deff2) / atoms.owx;//(F(L0, (i+1), j, k) + F(L0, (i-1), j, k)) / owx;
                
                deff1 = F[(dim[id]*jn*kn)*L0 + i*jn*kn + (j+1)*kn + k];
            	deff2 = F[(dim[id]*jn*kn)*L0 + i*jn*kn + (j-1)*kn + k];
            	const Fj = (deff1 + deff2) / atoms.owy; //(F(L0, i, (j+1), k) + F(L0, i, (j-1), k)) / owy;
                
                deff1 = F[(dim[id]*jn*kn)*L0 + i*jn*kn + j*kn + (k+1)];
            	deff2 = F[(dim[id]*jn*kn)*L0 + i*jn*kn + j*kn + (k-1)];
                const Fk = (deff1 + deff2) / atoms.owz; //(F(L0, i, j, (k+1)) + F(L0, i, j, (k-1))) / owz;
	    	     
                const Ra = atoms.Ro((i + dt)*atoms.hx, j*atoms.hy, k*atoms.hz);
         
                //F(L1, i, j, k) = (Fi + Fj + Fk - Ra) / c;
                F[(dim[id]*jn*kn)*L1 + i*jn*kn + j*kn+k] = (Fi + Fj + Fk - Ra) / atoms.c;

                deff1 = F[(dim[id]*jn*kn)*L1 + i*jn*kn + j*kn + k];
            	deff2 = F[(dim[id]*jn*kn)*L0 + i*jn*kn + j*kn + k];
                const Fa = Math.abs(deff1-deff2);//fabs(F(L1, i, j, k) - F(L0, i, j, k));
                if(Fa > mx) {
                    mx = Fa;
                }
            }
        }
    }
    df_max.value = mx;

    //df4 = df3;
            
    if(id !== 0) {
        df_down.value = [];//.create(jn*kn*sizeof(double));
        const up = df_down.value;//.getData<double>();
        const i = 1;
        for (let j = 0; j < jn; j++)
            for (let k = 0; k < kn; k++) {
                up[j*kn + k] = F[(dim[id]*jn*kn)*L1 + i*jn*kn + j*kn + k];//F(L1, i, j, k);
            }
    }
    else {
		df_down.value = null;//.create(0);
        /*for (let q = 0; q < jn*kn; q++) {
            df_down.value[q] = 0;
        }*/
    }
            
    if(id !== atoms.SIZE-1) {
        df_up.value = [];//.create(jn*kn*sizeof(double));
        const down = df_up.value;//.getData<double>();
        const i = dim[id]-2;
        for (let j = 0; j < jn; j++)
            for (let k = 0; k < kn; k++) {
                down[j*kn + k] = F[(dim[id]*jn*kn)*L1 + i*jn*kn + j*kn + k];//F(L1, i, j, k);
            }
    }
    else {
    	df_up.value = null;//.create(0);
        // for (let q = 0; q < jn*kn; q++) {
        //     df_up.value[q] = 0;
        // }
    }
}

// double x, double y, double z
atoms.Ro = (x: any, y: any, z: any) => {
    return -atoms.a*(x + y + z);
}

// double x, double y, double z
atoms.Fresh = (x: any, y: any, z: any) => {
    return x + y + z;
}

// int r, int m, const int *sft
atoms.calc_div = (r: any, m: any, sft: any) => {
	// ARGS
	// const r = args[0];
	// const m = args[1];
	// const sft = args[2];
    return (m !== 1) ? sft[r] - ((r !== 0) as any) : 0;
}

export const poi1dAtoms = ({...atoms})