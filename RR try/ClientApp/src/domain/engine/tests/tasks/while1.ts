import{ lunaLang } from "../../lang_processing/lunaLang";
import { IDf } from "../../lang_processing/cf";

/// ---------------
/// PROGRAM
/// ---------------
/// import c_init(int, name) as init;
/// import c_show(string, int) as show;
///
/// sub main()
/// {
///         df i, dummy;
///         init (4, i);
///
///         while j < i, j = 1..out dummy[i]
///                 cf a[i][j]: show("It:", i*100 + j);
/// }

export const while1: lunaLang.Program = {
	main: {
        body: [
            {
                type: "dfs", 
                names: [ "i", "dummy"]
            } as lunaLang.IDfDeclaration, 
            {
                code: "init", 
                args: [
                    {
                        type: "iconst",
                        value: 4
                    } as lunaLang.IIntConst, 
                    {
                        ref: ["i"], 
                        type: "id"
                    } as lunaLang.IDfArgument
                ],
                type: "exec", 
                id: ["_l9"]
            }, 
            {
                body: [
                    {
                        code: "show",
                        args: [
                            {
                                type: "sconst", 
                                is_expr: true, 
                                value: "It:"
                            } as lunaLang.IStringConst, 
                            {
                                type: "+",
                                operands: [
                                    {
                                        type: "*", 
                                        is_expr: true, 
                                        operands: [
                                            {
                                                ref: [
                                                    "i"
                                                ], 
                                                type: "id"
                                            } as lunaLang.IDfArgument, 
                                            {
                                                type: "iconst",
                                                value: 100
                                            } as lunaLang.IConstArgument
                                        ]
                                    } as lunaLang.Expression, 
                                    {
                                        ref: ["j"], 
                                        type: "id"
                                    } as lunaLang.IDfArgument
                                ]
                            } as lunaLang.Expression
                        ],
                        type: "exec", 
                        id: [
                            "a", 
                            {
                                ref: ["i"], 
                                type: "id"
                            } as lunaLang.IDfArgument, 
                            {
                                ref: ["j"], 
                                type: "id"
                            } as lunaLang.IDfArgument
                        ]
                    } as lunaLang.IExec
                ], 
                wout: {
                    type: "id", 
                    ref: [
                        "dummy", 
                        {
                            ref: ["i"], 
                            type: "id"
                        } as lunaLang.IDfArgument
                    ] 
                } as lunaLang.IDfArgument,
                start: {
                    type: "iconst", 
                    is_expr: true, 
                    value: 1
                }, 
                cond: {
                    type: "<", 
                    is_expr: true, 
                    operands: [
                        {
                            ref: [
                                "j"
                            ], 
                            type: "id",
                        } as lunaLang.IDfArgument, 
                        {
                            ref: [
                                "i"
                            ], 
                            type: "id"
                        } as lunaLang.IDfArgument
                    ]
                } as lunaLang.Expression, 
                var: "j", 
                type: "while"
            } as lunaLang.IWhile
        ], 
        type: "struct", 
        args: [], 
        where: {
            type: "luna"
        }
	} as lunaLang.Subroutine,
	
	init: {
        type: "extern", 
        code: "c_init",
        args: [{ type: "int" }, { type: "name" } ]
	} as lunaLang.Atom,
	
	show: {
        type: "extern", 
        code: "c_show", 
        args: [{ type: "string" }, { type: "int" }]
    } as lunaLang.Atom
}

export const while1Atoms = {
	c_init: (value: number, df: IDf) => {
		df.value = value;
	},

	c_show: (text: string, value: number) => {
		console.log ("C_SHOW: " + text + " " + value);
	}
}