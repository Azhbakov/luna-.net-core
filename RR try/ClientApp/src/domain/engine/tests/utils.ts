import { IExecCf } from "../lang_processing/cf_impl/execCf";
import { lunaLang } from "../lang_processing/lunaLang";

export function createSubroutine(init: (sub: lunaLang.Subroutine) => lunaLang.Subroutine): lunaLang.Subroutine {
	return init({
		type: "struct",
		args: [
			// {
			// 	name: "firstArg",
			// 	type: "int"
			// }
		] as lunaLang.NamedParameter[],
		body: [
			// innerExecCf
		]
	} as lunaLang.Subroutine);
}

export function createExecDecl(init: (execDecl: lunaLang.IExec) => lunaLang.IExec): lunaLang.IExec {
	return init({
		type: 'exec',
		id: [
			"innerId"
		],
		args: [
			// {
			// 	"ref": [
			// 		"firstArg"
			// 	],
			// 	type: "id"
			// }
		],
		code: "innerSub"
	} as lunaLang.IExec);
}

export function createExecCf(init: (execCf: IExecCf) => IExecCf): IExecCf {
	return init({
		type: 'exec',
		id: {
			main: "_execMainCf",
			indicies: []
		},
		args: [
			// { 
			// 	type: 'iconst', 
			// 	value: 11 
			// } as IConstArgument,
		],
		prototypeName: "execPrototype",
		prototype: {
			type: "struct",
			args: [
				// {
				// 	type: "int",
				// 	name: "innerArgName"
				// }
			],
			body:[]
		}
	} as IExecCf);
}