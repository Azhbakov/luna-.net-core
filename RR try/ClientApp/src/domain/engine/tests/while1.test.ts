import { Worker } from '../runtime/worker'
import { while1, while1Atoms } from './tasks/while1';
import { LocalStorage } from '../runtime/localStorage';

/// ---------------
/// PROGRAM
/// ---------------
/// import c_init(int, name) as init;
/// import c_show(string, int) as show;
///
/// sub main()
/// {
///         df i, dummy;
///         init (4, i);
///
///         while j < i, j = 1..out dummy[i]
///                 cf a[i][j]: show("It:", i*100 + j);
/// }

it('while loop works', () => {
	const storage = new LocalStorage();
	const worker = new Worker(while1, while1Atoms, storage);
	worker.step().then(() => { // Execute main()
		expect(storage.getCfs()).toHaveLength(2); // init, while
		expect(storage.getDfs()).toHaveLength(0);
		return worker.step();
	}).then(() => { // Execute init(4, i), new DF { i, 0 }
		expect(storage.getCfs()).toHaveLength(1); // while
		expect(storage.getDfs()).toHaveLength(1); // i == 4
		return worker.step();
	}).then(() => { // Execute while(1)
		expect(storage.getCfs()).toHaveLength(2); // show(4*100 + 1), while(2)
		expect(storage.getDfs()).toHaveLength(1); // i == 4
		return worker.step();
	}).then(() => { // Execute show(4*100 + 1)
		expect(storage.getCfs()).toHaveLength(1); // while(2)
		expect(storage.getDfs()).toHaveLength(1); // i == 4
		return worker.step();
	})

	.then(() => { // Execute while(2)
		expect(storage.getCfs()).toHaveLength(2); // show(4*100 + 2), while(3)
		expect(storage.getDfs()).toHaveLength(1); // i == 4
		return worker.step();
	}).then(() => { // Execute show(4*100 + 2)
		expect(storage.getCfs()).toHaveLength(1); // while(3)
		expect(storage.getDfs()).toHaveLength(1); // i == 4
		return worker.step();
	})

	.then(() => { // Execute while(3)
		expect(storage.getCfs()).toHaveLength(2); // show(4*100 + 3), while(4)
		expect(storage.getDfs()).toHaveLength(1); // i == 4
		return worker.step();
	}).then(() => { // Execute show(4*100 + 3)
		expect(storage.getCfs()).toHaveLength(1); // while(4)
		expect(storage.getDfs()).toHaveLength(1); // i == 4
		return worker.step();
	})

	.then(() => { // Execute while(4)
		expect(storage.getCfs()).toHaveLength(0);
		expect(storage.getDfs()).toHaveLength(2); // i == 4, dummy[4] == 4
	});
	
});
