// Action Types
export const actionTypes = {
    COUNTER_INCREMENT: 'COUNTER/INCREMENT',
    COUNTER_DECREMENT: 'COUNTER/DECREMENT'
};

// Action Objects
export interface CounterIncrementAction { type: 'COUNTER/INCREMENT' }
export interface CounterDecrementAction { type: 'COUNTER/DECREMENT' }
export type CounterAction = CounterIncrementAction | CounterDecrementAction;

// Action Creators
const increment = (): CounterIncrementAction => ({ type: 'COUNTER/INCREMENT' })
const decrement = (): CounterDecrementAction => ({ type: 'COUNTER/DECREMENT' })

export const actionCreators = {
    increment,
    decrement
}