export interface CounterState {
    counter: number;
}

export const InitialCounterState : CounterState = {
    counter: 0
}