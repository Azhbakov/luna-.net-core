import { CounterState, InitialCounterState } from "./interfaces";
import update from 'immutability-helper';
import { CounterAction, actionTypes } from "./actions";
import { Reducer } from "redux";

const incrementCounter = (state: CounterState): CounterState => {
    return update(state, { counter: { $set: state.counter + 1 }});
}

const decrementCounter = (state: CounterState): CounterState => {
    return update(state, { counter: { $set: state.counter - 1 }});
}

export const counterReducer: Reducer<CounterState> = (state: CounterState = InitialCounterState, action: CounterAction): CounterState => {
    switch (action.type) {
        case actionTypes.COUNTER_INCREMENT:
            return incrementCounter(state);
        case actionTypes.COUNTER_DECREMENT:
            return decrementCounter(state);
        default: 
            return state;
    }
}