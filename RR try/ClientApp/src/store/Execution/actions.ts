import { lunaLang } from "src/domain/engine/lang_processing/lunaLang";
import { Atoms, ICf, IDf } from "src/domain/engine/lang_processing/cf";
import { ExecutionStatus } from "./interfaces";
import { ExecutionPeriod, IdlePeriod } from "src/domain/engine/runtime/worker";
import { ExecutionSettings } from "src/domain/ExecutionSettings";
import { IBatchUpdate } from "src/domain/engine/runtime/webworker/bufferedStorageView";

export const executionActionTypes = {
	SET_TASK: "EXECUTION/SET_TASK",
	START_EXECUTION: "EXECUTION/START",
	STEP_EXECUTION: "EXECUTION/STEP",
	CANCEL_EXECUTION: "EXECUTION/CANCEL",
	STATUS: "EXECUTION/STATUS",

	CF_ADDED: "EXECUTION/CF_ADDED",
	CF_REMOVED: "EXECUTION/CF_REMOVED",
	DF_ADDED: "EXECUTION/DF_ADDED",
	BATCH_UPDATE: "EXECUTION/BATCH_UPDATE",

	CF_EXEC_STARTED: "EXECUTION/CF_EXEC_STARTED",
	CF_EXEC_FINISHED: "EXECUTION/CF_EXEC_FINISHED",
	IDLE_STARTED: "EXECUTION/IDLE_STARTED",
	IDLE_FINISHED: "EXECUTION/IDLE_FINISHED",
}

export interface SetTaskAction {
	type: "EXECUTION/SET_TASK",
	payload: { atoms: Atoms, program: lunaLang.Program, settings: ExecutionSettings };
}
export interface StartAction {
	type: "EXECUTION/START"
}
export interface StepAction {
	type: "EXECUTION/STEP"
}
export interface CancelAction {
	type: "EXECUTION/CANCEL"
}
export interface UpdateStatusAction {
	type: "EXECUTION/STATUS",
	payload: { status: ExecutionStatus }
}


export interface CfAddedAction {
	type: "EXECUTION/CF_ADDED",
	payload: { cf: ICf }
}
export interface CfRemovedAction {
	type: "EXECUTION/CF_REMOVED",
	payload: { cf: ICf }
}
export interface DfAddedAction {
	type: "EXECUTION/DF_ADDED",
	payload: { df: IDf }
}
export interface BatchUpdateAction {
	type: "EXECUTION/BATCH_UPDATE",
	payload: IBatchUpdate
}

export interface CfExecStartedAction {
	type: "EXECUTION/CF_EXEC_STARTED",
	payload: { period: ExecutionPeriod }
}
export interface CfExecFinishedAction {
	type: "EXECUTION/CF_EXEC_FINISHED",
	payload: { period: ExecutionPeriod }
}
export interface IdleStartedAction {
	type: "EXECUTION/IDLE_STARTED",
	payload: { period: IdlePeriod }
}
export interface IdleFinishedAction {
	type: "EXECUTION/IDLE_FINISHED",
	payload: { period: IdlePeriod }
}

export type ExecutionAction = SetTaskAction | StartAction | StepAction
	| CancelAction | UpdateStatusAction
	| CfAddedAction | CfRemovedAction | DfAddedAction | BatchUpdateAction
	| CfExecStartedAction | CfExecFinishedAction | IdleStartedAction | IdleFinishedAction

const setTask = (atoms: Atoms, program: lunaLang.Program, settings: ExecutionSettings): SetTaskAction => ({
	type: "EXECUTION/SET_TASK",
	payload: { atoms, program, settings }
})
const start = (): StartAction => ({
	type: "EXECUTION/START"
})
const step = (): StepAction => ({
	type: "EXECUTION/STEP"
})
const cancel = (): CancelAction => ({
	type: "EXECUTION/CANCEL"
})
const updateStatus = (status: ExecutionStatus): UpdateStatusAction => ({
	type: "EXECUTION/STATUS",
	payload: { status }
})

const cfAdded = (cf: ICf): CfAddedAction => ({
	type: "EXECUTION/CF_ADDED",
	payload: { cf }
})
const cfRemoved = (cf: ICf): CfRemovedAction => ({
	type: "EXECUTION/CF_REMOVED",
	payload: { cf }
})
const dfAdded = (df: IDf): DfAddedAction => ({
	type: "EXECUTION/DF_ADDED",
	payload: { df }
})
const batchUpdate = (b: IBatchUpdate): BatchUpdateAction => ({
	type: "EXECUTION/BATCH_UPDATE",
	payload: b
})

const cfExecutionStarted = (period: ExecutionPeriod): CfExecStartedAction => ({
	type: "EXECUTION/CF_EXEC_STARTED",
	payload: { period }
})
const cfExecutionFinished = (period: ExecutionPeriod): CfExecFinishedAction => ({
	type: "EXECUTION/CF_EXEC_FINISHED",
	payload: { period }
})
const idleStarted = (period: IdlePeriod): IdleStartedAction => ({
	type: "EXECUTION/IDLE_STARTED",
	payload: { period }
})
const idleFinished = (period: IdlePeriod): IdleFinishedAction => ({
	type: "EXECUTION/IDLE_FINISHED",
	payload: { period }
})

export const executionActions = {
	setTask, start, step, cancel, updateStatus,
	cfAdded, cfRemoved, dfAdded, batchUpdate,
	cfExecutionStarted, cfExecutionFinished, idleStarted, idleFinished
}