import { IDf, ICf, Atoms } from "src/domain/engine/lang_processing/cf";
import { lunaLang } from "src/domain/engine/lang_processing/lunaLang";
import { ExecutionPeriod, IdlePeriod } from "src/domain/engine/runtime/worker";
import { ExecutionSettings } from "src/domain/ExecutionSettings";

export type ExecutionStatus = 'NOT STARTED' | 'RUNNING' | 'RUNNING STEP' | 'AWAITING STEP' | 'FINISHED' | 'CANCELLED'

export interface ExecutionState {
	status: ExecutionStatus
	cfs: ICf[],
	dfs: IDf[],
	execPeriods: ExecutionPeriod[],
	idlePeriods: IdlePeriod[],
	settings: ExecutionSettings,
	atoms: Atoms | null,
	program: lunaLang.Program | null
}

export const InitialExecutionState = {
	status: 'NOT STARTED',
	cfs: [],
	dfs: [],
	execPeriods: [],
	idlePeriods: [],
	settings: { updateUI: true, mode: 'STEP' } as ExecutionSettings,
	atoms: null,
	program: null
} as ExecutionState