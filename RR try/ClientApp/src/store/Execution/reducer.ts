import { ExecutionState, InitialExecutionState, ExecutionAction, DfAddedAction } from ".";
import { Reducer } from "redux";
import { executionActionTypes, SetTaskAction, CfAddedAction, CfRemovedAction, UpdateStatusAction, CfExecStartedAction, CfExecFinishedAction, IdleStartedAction, IdleFinishedAction, BatchUpdateAction } from "./actions";
import update from "immutability-helper";
import { fragmentIdMethods } from "src/domain/engine/lang_processing/cf";

export const executionReducer: Reducer<ExecutionState> = (state = InitialExecutionState, action: ExecutionAction): ExecutionState => {
	switch (action.type) {

		case executionActionTypes.SET_TASK:
			const setTaskAction = action as SetTaskAction;
			return update(state, { 
				atoms: { $set: setTaskAction.payload.atoms },
				program: { $set: setTaskAction.payload.program },
				settings: { $set: setTaskAction.payload.settings },
			});

		case executionActionTypes.CF_ADDED:
			const cfAddedAction = action as CfAddedAction;
			return update(state, { 
				cfs: { $push: [cfAddedAction.payload.cf] }
			});

		case executionActionTypes.DF_ADDED:
			const dfAddedAction = action as DfAddedAction;
			return update(state, { 
				dfs: { $push: [dfAddedAction.payload.df] }
			});

		case executionActionTypes.CF_REMOVED:
			const removedCf = (action as CfRemovedAction).payload.cf;
			const removedCfIndex = state.cfs.findIndex(cf => fragmentIdMethods.equals(cf.id, removedCf.id));
			// if (removedCfIndex === -1) throw new Error("Trying to remove CF but it is not present in state."); // TODO: discover cases
			return update(state, { 
				cfs: { $splice: [[removedCfIndex, 1]] }
			});

		case executionActionTypes.BATCH_UPDATE:
			const b = (action as BatchUpdateAction).payload;
			state.cfs.push(...b.cfs);
			state.dfs.push(...b.dfs);
			return update(state, {
					cfs: { $set: state.cfs.filter(cf => b.removedCfs.findIndex(c => fragmentIdMethods.equals(cf.id, c.id)) === -1) },
					dfs: { $set: state.dfs }
				});

		case executionActionTypes.STATUS:
			const status = (action as UpdateStatusAction).payload.status;
			return update(state, {
				status : { $set: status }
			});


		case executionActionTypes.CF_EXEC_STARTED:
			const execStart = (action as CfExecStartedAction).payload.period;
			return update(state, { execPeriods: { $push: [execStart] } });

		case executionActionTypes.CF_EXEC_FINISHED:
			const execFinish = (action as CfExecFinishedAction).payload.period;
			const execStartIndex = state.execPeriods.findIndex(st => st === execFinish);
			if (execStartIndex === -1) throw new Error("Trying to log time but start entry not found.");

		case executionActionTypes.IDLE_STARTED:
			const idleStart = (action as IdleStartedAction).payload.period;
			return update(state, { idlePeriods: { $push: [idleStart] } });

		case executionActionTypes.IDLE_FINISHED:
			const idleFinish = (action as IdleFinishedAction).payload.period;
			const idleStartIndex = state.idlePeriods.findIndex(st => st === idleFinish);
			if (idleStartIndex === -1) throw new Error("Trying to log time but start entry not found.");
		
		default: return state;
	}
}