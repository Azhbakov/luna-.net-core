import { executionActionTypes, SetTaskAction, executionActions } from "./actions";
import { takeLatest, take, put, call, fork, select, cancelled } from "redux-saga/effects";
import { DistributedStorage, IObservableStorage } from "src/domain/engine/runtime/distributedStorage";
import { Worker, IWorker } from "src/domain/engine/runtime/worker";
import { eventChannel } from "redux-saga";
import { IPathfinderContext, SimplePathfinderCreator } from "src/domain/engine/runtime/simplePathfinder";
import { AppState } from "../configureStore";
import { ILobby } from "src/domain/Lobby";
import { CommModule, ICommContext, IPeerComm } from "src/domain/engine/runtime/commModule";
import { actionTypes as lobbyActionTypes, actionCreators as lobbyActions } from "../Lobby/actions"
import { Atoms } from "src/domain/engine/lang_processing/cf";
import { lunaLang } from "src/domain/engine/lang_processing/lunaLang";
import { ExecutionSettings } from "src/domain/ExecutionSettings";
import { WebworkerChannel } from "src/domain/engine/runtime/webworker/webworkerChannel";
import { ThreadStorageProxy } from "src/domain/engine/runtime/webworker/threadStorageProxy";
import { ThreadWorkerProxy } from "src/domain/engine/runtime/webworker/threadWorkerProxy";
import { ThreadCommModule } from "src/domain/engine/runtime/webworker/threadCommModule";
import { InitPathfinder, InitWebworker as InitWebWorker } from "src/domain/engine/runtime/webworker/messageTypes";
import { IObservableBufferedStorage } from "src/domain/engine/runtime/webworker/bufferedStorageView";
import { Poi1dPathfinderCreator } from "src/domain/engine/runtime/poi1dPathfinder";
// import { Poi1dPathfinder } from "src/domain/engine/runtime/poi1dPathfinder";


export function *executeTask(atoms: Atoms, program: lunaLang.Program, settings: ExecutionSettings, peerComms: IPeerComm[]) {
	let webworker: WebworkerChannel | undefined;
	try {
		webworker = new WebworkerChannel();
		const { worker, storage } = yield call(initWebworker, webworker, atoms, program, settings, peerComms);

		// UI initialization
		if (settings.updateUI) {
			yield fork(processStorageCallbacks, storage);
			yield fork(processWorkerCallbacks, worker);
		}

		if (settings.mode === 'STEP') {
			yield call(executeWithSteps, worker, storage);
		} else {
			yield call(executeContinuous, worker);
		}
	} finally {
		if (webworker) webworker.close();
	}
}

function *initWebworker(webworker: WebworkerChannel, atoms: Atoms, program: lunaLang.Program, settings: ExecutionSettings, peerComms: IPeerComm[]) {
	const { selfId, lobby } = yield select((appState: AppState) => appState.lobby);
	if (lobby === null || selfId === null) throw new Error("Starting task execution but state is not fully initialized.")

	// Pathfinder
	const pathfinderContext = {
		leaderId: (lobby as ILobby).leaderId,
		selfId, 
		topology: (lobby as ILobby).topology
	} as IPathfinderContext;
	const pathfinder = SimplePathfinderCreator(pathfinderContext);

	// Comm module
	const commContext = { selfId, peerConnections: peerComms } as ICommContext;
	const commModule = new CommModule(commContext, pathfinder);

	// Remote comm module proxy
	const threadCommModule = new ThreadCommModule(commModule, webworker);

	// const storage = new DistributedStorage(selfId, pathfinder, commModule);
	// const worker = new Worker(program, atoms, storage, settings.isLeader);
	
	// Remote storage
	const { storage, initMessage: storageMessage } = ThreadStorageProxy.init(webworker, selfId);

	// Remote worker
	const { worker, initMessage: workerMessage } = ThreadWorkerProxy.init(webworker, program, atoms, settings.isLeader);

	webworker.postMessage({ 
		type: 'initWebworker',
		worker: workerMessage, 
		storage: storageMessage,
		pathfinder: {
			type: 'initPathfinder',
			pathfinderCreator: Poi1dPathfinderCreator.toString() as any,
			pathfinderContext
		} as InitPathfinder 
	} as InitWebWorker);

	return ({ webworker, storage, worker });
}

function *executeWithSteps(worker: IWorker) {
	while (true) {
		yield put(executionActions.updateStatus('AWAITING STEP'));
		yield take(executionActionTypes.STEP_EXECUTION);
		yield put(executionActions.updateStatus('RUNNING STEP'));
		yield call([worker, worker.step]);
	}
}

function *executeContinuous(worker: IWorker) {
	yield call([worker, worker.work]);
}

function *processStorageCallbacks(storage: IObservableStorage | IObservableBufferedStorage) {
	for (const cf of storage.getCfs()) {
		yield put(executionActions.cfAdded(cf));
	};

	const channel = eventChannel(emitter => {
		storage.onCfAdded.addCallback(cf => emitter(executionActions.cfAdded(cf)));
		storage.onDfAdded.addCallback(df => emitter(executionActions.dfAdded(df)));
		storage.onCfRemoved.addCallback(cf => emitter(executionActions.cfRemoved(cf)));
		if ((storage as IObservableBufferedStorage).onBatchUpdate) {
			(storage as IObservableBufferedStorage).onBatchUpdate.addCallback(b => emitter(executionActions.batchUpdate(b)));
		}
		return () => { return; };
	});

	while(true) {
		const action = yield take(channel);
		yield put(action);
	}
}

function *processWorkerCallbacks(worker: IWorker) {
	const channel = eventChannel(emitter => {
		worker.onCfExecutionStart.addCallback(execPeriod => emitter(executionActions.cfExecutionStarted(execPeriod)));
		worker.onCfExecutionFinish.addCallback(execPeriod => emitter(executionActions.cfExecutionFinished(execPeriod)));
		worker.onIdleStart.addCallback(idlePeriod => emitter(executionActions.idleStarted(idlePeriod)));
		worker.onIdleFinish.addCallback(idlePeriod => emitter(executionActions.idleFinished(idlePeriod)));

		return () => { return; };
	});

	while(true) {
		const action = yield take(channel);
		yield put(action);
	}
}