import { Lobby } from "src/store/Lobbies";

export const actionTypes = {
    LOBBIES_FETCH_START: 'LOBBIES_FETCH_START',
    LOBBIES_FETCH_SUCCESS: 'LOBBIES_FETCH_SUCCESS',
    LOBBIES_FETCH_FAILURE: 'LOBBIES_FETCH_FAILURE',
 
    LOBBY_REMOVE_START: 'LOBBY_REMOVE_START',
    LOBBY_REMOVE_SUCCESS: 'LOBBY_REMOVE_SUCCESS',
    LOBBY_REMOVE_FAILURE: 'LOBBY_REMOVE_FAILURE',

    // LOBBY_CREATE_START: 'LOBBY_CREATE_START',
    // LOBBY_CREATE_SUCCESS: 'LOBBY_CREATE_SUCCESS',
    // LOBBY_CREATE_FAILURE: 'LOBBY_CREATE_FAILURE',
}

export interface LobbiesFetchStartAction {
    type: 'LOBBIES_FETCH_START'
}
export interface LobbiesFetchSuccessAction {
    type: 'LOBBIES_FETCH_SUCCESS',
    payload: Lobby[]
}
export interface LobbiesFetchFailureAction {
    type: 'LOBBIES_FETCH_FAILURE',
    error: Error
}

export interface LobbyRemoveStartAction {
    type: 'LOBBY_REMOVE_START'
    payload: string
}
export interface LobbyRemoveSuccessAction {
    type: 'LOBBY_REMOVE_SUCCESS',
    payload: string
}
export interface LobbyRemoveFailureAction {
    type: 'LOBBY_REMOVE_FAILURE',
    payload: string,
    error: Error
}

// export interface LobbyCreateStartAction {
//     type: 'LOBBY_CREATE_START'
//     payload: LobbyCreateVM
// }
// export interface LobbyCreateSuccessAction {
//     type: 'LOBBY_CREATE_SUCCESS'
// }
// export interface LobbyCreateFailureAction {
//     type: 'LOBBY_CREATE_FAILURE'
//     error: Error
// }

export type LobbiesFetchAction = 
    LobbiesFetchStartAction | LobbiesFetchSuccessAction | LobbiesFetchFailureAction;
// LobbyRemoveStartAction | LobbyRemoveSuccessAction | LobbyRemoveFailureAction;

// ACTION CREATORS
const lobbiesFetchStart = (): LobbiesFetchStartAction => ({
    type: 'LOBBIES_FETCH_START'
});
const lobbiesFetchFailure = (error: Error): LobbiesFetchFailureAction => ({
    type: 'LOBBIES_FETCH_FAILURE',
    error
});
const lobbiesFetchSuccess = (lobbies: Lobby[]): LobbiesFetchSuccessAction => ({
    type: 'LOBBIES_FETCH_SUCCESS',
    payload: lobbies
});

const lobbyRemoveStart = (id: string): LobbyRemoveStartAction => ({
    type: 'LOBBY_REMOVE_START',
    payload: id
});
const lobbyRemoveFailure = (id: string, error: Error): LobbyRemoveFailureAction => ({
    type: 'LOBBY_REMOVE_FAILURE',
    payload: id,
    error
});
const lobbyRemoveSuccess = (id: string): LobbyRemoveSuccessAction => ({
    type: 'LOBBY_REMOVE_SUCCESS',
    payload: id
});

// const lobbyCreateStart = (lobby: LobbyCreateVM): LobbyCreateStartAction => ({
//     type: 'LOBBY_CREATE_START',
//     payload: lobby
// });
// const lobbyCreateFailure = (error: Error): LobbyCreateFailureAction => ({
//     type: 'LOBBY_CREATE_FAILURE',
//     error
// });
// const lobbyCreateSuccess = (): LobbyCreateSuccessAction => ({
//     type: 'LOBBY_CREATE_SUCCESS',
// });


export const actionCreators = { 
    lobbiesFetchStart, lobbiesFetchSuccess, lobbiesFetchFailure, 
    lobbyRemoveStart, lobbyRemoveSuccess, lobbyRemoveFailure,
//    lobbyCreateStart, lobbyCreateSuccess, lobbyCreateFailure 
}