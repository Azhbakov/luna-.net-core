import { serverUrl } from '..';

export * from './interfaces';
export * from './actions';
export * from './reducer';
export * from './saga';

export const lobbiesFetchUrl = serverUrl + 'api/lobbies/';
export const lobbyRemoveUrl = (id: string) => serverUrl + 'api/lobbies/' + id;
export const lobbyCreateUrl = serverUrl + 'api/lobbies/';