export interface Lobby {
    id: string;
    lobbyName: string;
    taskId: string;
    userCount: number;
    maxUsersCount: number | null;
}

export interface LobbyCreateModel {
    taskId: string,
    lobbyName: string,
    maxUsersCount: number | null;
}

export interface LobbiesState {
    isLoading: boolean,
    lobbies: Lobby[],
    error: Error | null
}

export const InitialLobbiesState: LobbiesState = {
    isLoading: false,
    lobbies: [],
    error: null
}