import { LobbiesState, actionTypes, InitialLobbiesState, LobbiesFetchSuccessAction, LobbiesFetchFailureAction, LobbiesFetchAction } from ".";
import { Reducer } from "redux";

export const lobbiesFetchReducer: Reducer<LobbiesState> =
(state: LobbiesState = InitialLobbiesState, action: LobbiesFetchAction): LobbiesState =>
{
    switch (action.type) {
        case actionTypes.LOBBIES_FETCH_START:
            return {
                isLoading: true,
                lobbies: [],
                error: null
            }
        case actionTypes.LOBBIES_FETCH_SUCCESS:
            return {
                isLoading: false,
                lobbies: (action as LobbiesFetchSuccessAction).payload,
                error: null
            }
        case actionTypes.LOBBIES_FETCH_FAILURE:
            return {
                isLoading: false,
                lobbies: [],
                error: (action as LobbiesFetchFailureAction).error,
            }
        default:
            return state;
    }
}