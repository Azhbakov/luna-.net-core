import { actionTypes, lobbiesFetchUrl, actionCreators, LobbyRemoveStartAction, lobbyRemoveUrl } from ".";
import { takeLatest, takeEvery, put, call } from "redux-saga/effects";
import { func } from "../../../node_modules/@types/prop-types";

export function* lobbiesSaga() {
    yield takeLatest(actionTypes.LOBBIES_FETCH_START, fetchLobbies);
    yield takeEvery(actionTypes.LOBBY_REMOVE_START, removeLobby);
    yield takeLatest(actionTypes.LOBBY_REMOVE_SUCCESS, fetchLobbies);
}

function* fetchLobbies() {
    try {
        const response = yield call(fetch, lobbiesFetchUrl);        
        const lobbies = yield call([response, response.json]);
        yield put(actionCreators.lobbiesFetchSuccess(lobbies));
    } catch (e) {
        yield put(actionCreators.lobbiesFetchFailure(e));
        return;
    }
}

function* removeLobby(action: LobbyRemoveStartAction) {
    const id = action.payload;
    try {
        const response = yield call(fetch, lobbyRemoveUrl(id), { method: "DELETE"});
        if (response.ok) {
            yield put(actionCreators.lobbyRemoveSuccess(id));
        } else {
            yield put(actionCreators.lobbyRemoveFailure(id, new Error(`${response.status}: ${response.statusText}`)));
        }
    } catch (e) {
        yield put(actionCreators.lobbyRemoveFailure(id, e));
        return;
    }
}