import { ILobby, LobbyStatus } from "../../domain/Lobby";
import { ExecutionSettings } from "src/domain/ExecutionSettings";

export const actionTypes = {
	LOBBY_JOIN: 'LOBBY/JOIN',
	LOBBY_LEAVE: 'LOBBY/LEAVE',
	LOBBY_START: 'LOBBY/START',
	LOBBY_STARTED: 'LOBBY/STARTED',
	LOBBY_JOINED: 'LOBBY/JOINED',
	LOBBY_CONNECTION_REFUSED: 'LOBBY/CONNECTION_REFUSED',
	LOBBY_PEER_CONNECTED: 'LOBBY/PEER_CONNECTED',
	LOBBY_PEER_DISCONNECTED: 'LOBBY/PEER_DISCONNECTED',
	LOBBY_UPDATE: 'LOBBY/UPDATE',
	LOBBY_ERROR: 'LOBBY/ERROR'
}

export interface LobbyJoinAction {
	type: 'LOBBY/JOIN',
	payload: string
}
export interface LobbyLeaveAction {
    type: 'LOBBY/LEAVE',
}
export interface LobbyStartAction {
	type: 'LOBBY/START',
	payload: { settings: ExecutionSettings }
}
export interface LobbyStartedAction {
	type: 'LOBBY/STARTED',
	payload: { lobby: ILobby, settings: ExecutionSettings }
}
export interface LobbyJoined {
	type: 'LOBBY/JOINED',
	payload: { id: string, lobby: ILobby }
}
export interface LobbyConnectionRefusedAction {
	type: 'LOBBY/CONNECTION_REFUSED',
	payload: string
}
export interface LobbyPeerConnectedAction {
	type: 'LOBBY/PEER_CONNECTED',
	payload: { connectedPeerId: string, lobby: ILobby }
}
export interface LobbyPeerDisconnectedAction {
	type: 'LOBBY/PEER_DISCONNECTED',
	payload: { disconnectedPeerId: string, lobby: ILobby }
}
export interface LobbyUpdateAction {
	type: 'LOBBY/UPDATE',
	payload: { lobby: ILobby }
}
export interface LobbyErrorAction {
	type: 'LOBBY/ERROR',
	payload: Error
}

export type LobbyAction = LobbyJoinAction | LobbyLeaveAction  | LobbyStartAction
	| LobbyJoined | LobbyConnectionRefusedAction
	| LobbyPeerConnectedAction | LobbyPeerDisconnectedAction | LobbyUpdateAction
	| LobbyErrorAction
	| LobbyStartedAction; 

const lobbyJoin = (lobbyId: string) => ({
	type: actionTypes.LOBBY_JOIN,
	payload: lobbyId
})
const lobbyStart = (settings: ExecutionSettings) => ({
	type: actionTypes.LOBBY_START,
	payload: { settings }
})
const lobbyLeave = () => ({
	type: actionTypes.LOBBY_LEAVE,
})
const lobbyJoined = (id: string, lobby: ILobby): LobbyJoined => ({
	type: 'LOBBY/JOINED',
	payload: { id, lobby }
})
const lobbyConnectionRefused = (error: string): LobbyConnectionRefusedAction => ({
	type: 'LOBBY/CONNECTION_REFUSED',
	payload: error
})

const lobbyPeerConnected = (connectedPeerId: string, lobby: ILobby): LobbyPeerConnectedAction => ({
	type: 'LOBBY/PEER_CONNECTED',
	payload: { connectedPeerId, lobby}
})
const lobbyPeerDisconnected = (disconnectedPeerId: string, lobby: ILobby): LobbyPeerDisconnectedAction => ({
	type: 'LOBBY/PEER_DISCONNECTED',
	payload: { disconnectedPeerId, lobby}
})
const lobbyUpdate = (lobby: ILobby): LobbyUpdateAction => ({
	type: 'LOBBY/UPDATE',
	payload: { lobby }
})

const LobbyError = (error: Error): LobbyErrorAction => ({
	type: 'LOBBY/ERROR',
	payload: error
})

const lobbyStarted = (lobby: ILobby, settings: ExecutionSettings) => ({
	type: actionTypes.LOBBY_STARTED,
	payload: { lobby, settings }
})

export const actionCreators = { 
	lobbyJoin, lobbyStart, lobbyLeave, 
	lobbyJoined, lobbyConnectionRefused,
	lobbyPeerConnected, lobbyPeerDisconnected,
	LobbyError, lobbyUpdate,
	lobbyStarted
}