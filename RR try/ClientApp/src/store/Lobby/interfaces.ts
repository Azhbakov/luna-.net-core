import { ILobby } from "src/domain/Lobby";

export interface LobbyState {
	selfId: string | null;
	lobby: ILobby | null;
	lobbyError: Error | null;
	allPeersReady: boolean;
}

export const InitialLobbyState : LobbyState = {
	selfId: null,
	lobby: null,
	lobbyError: null,
	allPeersReady: false
}