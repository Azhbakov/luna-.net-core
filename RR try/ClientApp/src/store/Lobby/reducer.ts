import { Reducer } from "redux";
import { LobbyState, InitialLobbyState } from "./interfaces";
import { LobbyAction, LobbyJoined, LobbyErrorAction, LobbyConnectionRefusedAction, LobbyPeerConnectedAction, LobbyPeerDisconnectedAction, LobbyUpdateAction, LobbyStartedAction } from "./actions";
import { actionTypes } from "../Lobby";
import update from "immutability-helper";

export const lobbyReducer: Reducer<LobbyState> = (state = InitialLobbyState, action: LobbyAction): LobbyState => {
	switch (action.type) {
		case actionTypes.LOBBY_LEAVE:
			return InitialLobbyState;
		case actionTypes.LOBBY_JOINED:
			const connData = (action as LobbyJoined).payload;
			return update(state, { selfId: { $set: connData.id }, lobby: { $set: connData.lobby } });
		case actionTypes.LOBBY_CONNECTION_REFUSED:
			return update(state, { lobbyError: { $set: new Error((action as LobbyConnectionRefusedAction).payload) } });
		case actionTypes.LOBBY_PEER_CONNECTED:
			const connectedPeerData = (action as LobbyPeerConnectedAction).payload;
			return update(state, { lobby: { $set: connectedPeerData.lobby } } );
		case actionTypes.LOBBY_PEER_DISCONNECTED:
			const diconnectedPeerData = (action as LobbyPeerDisconnectedAction).payload;
			return update(state, { lobby: { $set: diconnectedPeerData.lobby } });
		case actionTypes.LOBBY_UPDATE:
			const lobby = (action as LobbyUpdateAction).payload.lobby;
			return update(state, { lobby: { $set: lobby } });
		case actionTypes.LOBBY_ERROR:
			return update(state, { lobbyError: { $set: (action as LobbyErrorAction).payload } })
		case actionTypes.LOBBY_STARTED:
			const lobbyStarted = (action as LobbyStartedAction).payload.lobby;
			return update(state, { lobby: { $set: lobbyStarted } });
		default:
			return state;
	}
}