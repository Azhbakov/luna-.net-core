import { actionTypes as scActionTypes, actionCreators as scActions } from "../ServerConnection/actions"
import { actionTypes as lobbyActionTypes, actionCreators as lobbyActions } from "./actions"
import { actionTypes as webRtcActionTypes, actionCreators as webRtcActions, establishPeerConnectionByCreatingOffer, establishPeerConnectionByAcceptingOffer } from "src/store/PeerConnection";
import { put, take, call, fork, cancel, race, select, actionChannel, join } from "redux-saga/effects";
import * as signalR from "@aspnet/signalr";
import * as lobbyModule from "./actions";
import * as webRtcModule from "../PeerConnection/actions";
import { Task } from "redux-saga";
import { ServerConnectionDynamicContext, establishServerConnection } from "../ServerConnection";
import { ILobby, IEdge } from "src/domain/Lobby";
import { actionCreators as taskActions, fetchTaskData } from "../Tasks";
import { IPeerComm } from "src/domain/engine/runtime/commModule";
import { WebRtcPeerComm } from "../../domain/engine/runtime/webRtcPeerComm";
import { getLobbyFromState } from "./selectors";
import { executionActions, executeTask } from "../Execution";
import { AppDynamicContext } from "../configureStore";

export function* lobbySagas(appDC: AppDynamicContext) {
	while (true) {
		const joinAction = yield take(lobbyActionTypes.LOBBY_JOIN);
		yield race({
			saga: call(mainLobbySaga, joinAction, appDC.ServerConnectionDC),
			cancel: take(lobbyActionTypes.LOBBY_LEAVE)
		});
		yield put(scActions.stop());
	}
}

function *mainLobbySaga(lobbyJoinAction: lobbyModule.LobbyJoinAction, scDynamicContext: ServerConnectionDynamicContext) {
	try {
		// 1. Establish server connection
		yield call(establishServerConnection);
		const serverConnection = scDynamicContext.serverConnection;
		if (!serverConnection) throw new Error();

		const joinAnswer: { id: string, lobby: ILobby} = yield call([serverConnection, serverConnection.invoke], 'JoinLobby', lobbyJoinAction.payload, "test peer name");
		// TODO: check peer is accepted, process refusal
		yield put(lobbyActions.lobbyJoined(joinAnswer.id, joinAnswer.lobby));

		const peerConnections: IPeerConnection[] = [];
		
		yield call(updatePeerReadyStatus, serverConnection, { peerConnections, changeIndex: joinAnswer.lobby.changeIndex });

		// 2. Start listening to server messages - init peer connection creation/closing, reroute server messages to peer-recipient
		const serverListenerSaga = yield fork(manageServerConnection, serverConnection, joinAnswer.id, joinAnswer.lobby.changeIndex, peerConnections);

		// Fetch atoms and program
		const { atoms, program } = yield call(fetchTaskData, joinAnswer.lobby.task.id);
		yield put(taskActions.taskDataFetched(joinAnswer.lobby.task.id, atoms, program));

		// Init execution
		const settings = yield call(awaitStartSaga, serverConnection);
		const latestLobby = yield select(getLobbyFromState);
		settings.isLeader =  latestLobby.leaderId === joinAnswer.id;
		yield put(executionActions.setTask(atoms, program, settings));

		const peerComms: IPeerComm[] = peerConnections.map(pc => new WebRtcPeerComm(pc.targetPeerId, pc.targetDirection, pc.dataChannel));
		yield call(executeTask, atoms, program, settings, peerComms);

		yield join(serverListenerSaga); // wait forever until error
	} catch (error) {
		yield put(lobbyActions.LobbyError(error));
		throw error;
	} finally {
		// if (serverConnection) {
		// 	yield call([serverConnection, serverConnection.stop])
		// 	yield put(scActions.stopped());
		// }
	}
}

function *manageServerConnection(
		serverConnection: signalR.HubConnection, 
		selfId: string,
		changeIndex: number,
		peerConnections: IPeerConnection[]) {

	const dynamicContext = { peerConnections, changeIndex }
	yield fork(listenToDataChannelOpen, serverConnection, dynamicContext)

	while (true) {
		const action = yield take([lobbyActionTypes.LOBBY_PEER_CONNECTED, lobbyActionTypes.LOBBY_PEER_DISCONNECTED, webRtcActionTypes.RECEIVE_OFFER]);

		switch (action.type) {
			case lobbyActionTypes.LOBBY_PEER_CONNECTED: // Creating offers to new peers
			case lobbyActionTypes.LOBBY_PEER_DISCONNECTED:
				yield put(action);
				const lobby = action.payload.lobby as ILobby;
				dynamicContext.changeIndex = lobby.changeIndex;
				const { connectionsToBeRemoved, edgesToBeAdded } = yield call(syncConnections, lobby, selfId, peerConnections, serverConnection);
				
				for (const pc of connectionsToBeRemoved) {
					yield cancel(pc.saga);
					peerConnections.splice(peerConnections.indexOf(pc), 1);
				};

				for (const e of edgesToBeAdded) {
					const connectPc = { targetPeerId: e.toId, targetDirection: e.fromKey, edge: e } as IPeerConnection;
					connectPc.saga = yield fork(establishPeerConnectionByCreatingOffer, connectPc.targetPeerId, serverConnection, (dc: RTCDataChannel) => { connectPc.dataChannel = dc });
					peerConnections.push(connectPc);
				};

				yield call(updatePeerReadyStatus, serverConnection, dynamicContext);
				break;
			
			case webRtcActionTypes.RECEIVE_OFFER: // Waiting for offers from existing peers 
				const receiveOfferAction = action as webRtcModule.ReceiveOfferAction;
				yield put(action);
				
				const currentEdge = ((yield select(getLobbyFromState)) as ILobby).topology.edges.find(ed => ed.fromId === receiveOfferAction.payload.sourcePeerId && ed.toId === selfId);
				if (!currentEdge) throw new Error("Trying to accept offer but no such edge exists.");

				const targetPc = { targetPeerId: currentEdge.fromId, targetDirection: currentEdge.toKey, edge: currentEdge } as IPeerConnection;
				targetPc.saga = yield fork(establishPeerConnectionByAcceptingOffer, targetPc.targetPeerId, receiveOfferAction.payload.offer, serverConnection, (dc: RTCDataChannel) => { targetPc.dataChannel = dc });
				peerConnections.push(targetPc);
				break;

			default: 
				throw new Error(`Unknown server message: ${action.type}`);
		}
	}
}

function *listenToDataChannelOpen(serverConnection: signalR.HubConnection, 
		dynamicContext: { 
			peerConnections: IPeerConnection[],
			changeIndex: number
		}) {
	const bufferedChannel = yield actionChannel(webRtcActionTypes.ON_DATA_CHANNEL_OPEN);

	while (true) {
		const openDataChannelAction = (yield take(bufferedChannel)) as webRtcModule.OnDataChannelOpenAction;
		
		const establishedPeerId = openDataChannelAction.payload;
		const establishedPeerConnection = dynamicContext.peerConnections.find(p => p.targetPeerId === establishedPeerId);
		if (!establishedPeerConnection) throw new Error("Peer data channel is opened but peer is not registered.");
		establishedPeerConnection.isReady = true;

		// Check if all neighbor connections are established so we are ready
		yield call(updatePeerReadyStatus, serverConnection, dynamicContext);
	}
}

function *awaitStartSaga(serverConnection: signalR.HubConnection) {
	const codepath = yield race({
		startedHere: take(lobbyActionTypes.LOBBY_START),
		startedRemotely: take(lobbyActionTypes.LOBBY_STARTED)
	});

	const settings = codepath.startedHere ?
		(codepath.startedHere as lobbyModule.LobbyStartAction).payload.settings
		: (codepath.startedRemotely as lobbyModule.LobbyStartedAction).payload.settings;

	if (codepath.startedHere) {
		const answer: { started: boolean, lobby: ILobby } = yield call([serverConnection, serverConnection.invoke], "LobbyStart", settings);
		if (!answer.started) throw new Error("Server cancelled execution start.");
		yield put(lobbyActions.lobbyStarted(answer.lobby, settings));
	}
	return settings;
}

function *updatePeerReadyStatus(serverConnection: signalR.HubConnection, dynamicContext: { 
	peerConnections: IPeerConnection[],
	changeIndex: number
}) {
	const isReady = dynamicContext.peerConnections.every(p => p.isReady);
	const answer: { isCorrectChangeIndex: boolean, lobby: ILobby } = yield call([serverConnection, serverConnection.invoke], "SetPeerReady", dynamicContext.changeIndex, isReady);
	if (answer.isCorrectChangeIndex) yield put(lobbyActions.lobbyUpdate(answer.lobby));
	else console.log("WRONG CHANGE INDEX WHEN UPDATING PEER READY STATUS")
}

function *syncConnections(lobby: ILobby, selfId: string, peerConnections: IPeerConnection[]) {
	const relatedEdges = lobby.topology.edges.filter(e => e.fromId === selfId);
	const connectionsToBeRemoved = peerConnections.filter(pc => !relatedEdges.some(e => e === pc.edge));
	const edgesToBeAdded = relatedEdges.filter(e => !peerConnections.some(pc => e === pc.edge));

	return { connectionsToBeRemoved, edgesToBeAdded };
}

interface IPeerConnection {
	targetPeerId: string,
	targetDirection: string, 
	isReady: boolean, 
	edge: IEdge, 
	saga: Task, 
	dataChannel: RTCDataChannel;
}

