import { LobbyState } from "./interfaces";
import { ILobby } from "src/domain/Lobby";
import { AppState } from "../configureStore";

export function getLobby(state: LobbyState): ILobby {
	if (!state.lobby) throw new Error("Expected to have Lobby");
	return state.lobby;
}
export function getLobbyFromState(state: AppState): ILobby {
	return getLobby(state.lobby);
}

export function getSelfPeer(state: LobbyState) {
	const res = getLobby(state).topology.peers.find(p => p.id === state.selfId);
	if (!res) throw new Error("Self peer not found");
	return res;
}

export function isLeader(state: LobbyState, peerId: string | null = null) {
	const targetPeerId = peerId ? peerId : state.selfId;
	return getLobby(state).leaderId === targetPeerId;
}

export function areReady(state: LobbyState) {
	return getLobby(state).topology.peers.every(p => p.isReady);
}