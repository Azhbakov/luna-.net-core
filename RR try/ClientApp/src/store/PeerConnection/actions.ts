export const actionTypes = {
	// OFFER
	CREATE_OFFER: "WEBRTC/CREATE_OFFER",
	SEND_OFFER: "WEBRTC/SEND_OFFER",
	RECEIVE_ANSWER: "WEBRTC/RECEIVE_ANSWER",

	// ANSER
	RECEIVE_OFFER: "WEBRTC/RECEIVE_OFFER",
	SEND_ANSWER: "WEBRTC/SEND_ANSWER",

	// ICE SIGNALING
	SEND_ICE_CANDIDATE: "WEBRTC/SEND_ICE_CANDIDATE",
	RECEIVE_ICE_CANDIDATE: "WEBRTC/RECEIVE_ICE_CANDIDATE",

	// STATE
	ON_PEER_CONNECTION_CREATED: "WEBRTC/ON_PEER_CONNECTION_CREATED",
	ON_PEER_CONNECTION_CLOSED: "WEBRTC/ON_PEER_CONNECTION_CLOSED",
	ON_CONNECTION_STATE_CHANGE: "WEBRTC/ON_CONNECTION_STATE_CHANGE",
	ON_ICE_CONNECTION_STATE_CHANGE: "WEBRTC/ON_ICE_CONNECTION_STATE_CHANGE",
	ON_ICE_GATHERING_STATE_CHANGE: "WEBRTC/ON_ICE_GATHERING_STATE_CHANGE",
	ON_SIGNALING_STATE_CHANGE: "WEBRTC/ON_SIGNALING_STATE_CHANGE",
	ON_ERROR: "WEBRTC/ON_ERROR",

	// DATA CHANNEL
	ON_DATA_CHANNEL_OPEN: "WEBRTC/ON_DATA_CHANNEL_OPEN",
	ON_DATA_CHANNEL_CLOSE: "WEBRTC/ON_DATA_CHANNEL_CLOSE",
	ON_DATA_CHANNEL_ERROR: "WEBRTC/ON_DATA_CHANNEL_ERROR",
	ON_DATA_CHANNEL_MESSAGE_IN: "WEBRTC/ON_DATA_CHANNEL_MESSAGE_IN",
	ON_DATA_CHANNEL_MESSAGE_OUT: "WEBRTC/ON_DATA_CHANNEL_MESSAGE_OUT",

	STOP_COMMUNICATION: "WEBRTC/STOP_COMMUNICATION",
}

// OFFERER
export interface CreateOfferAction {
	type: "WEBRTC/CREATE_OFFER"
	payload: string
}
export interface SendOfferAction {
	type: "WEBRTC/SEND_OFFER"
	payload: { targetPeerId: string, offer: RTCSessionDescription }
}
export interface ReceiveAnswerAction {
	type: "WEBRTC/RECEIVE_ANSWER",
	payload: { sourcePeerId: string, offer: RTCSessionDescriptionInit }
}

// ANSWERER
export interface ReceiveOfferAction {
	type: "WEBRTC/RECEIVE_OFFER",
	payload: { sourcePeerId: string, offer: RTCSessionDescriptionInit }
}
// export interface CreateAnswerAction {
// 	type: "WEBRTC/CREATE_ANSWER"
// 	payload: { targetPeerId: string, answer: RTCSessionDescription }
// }
export interface SendAnswerAction {
	type: "WEBRTC/SEND_ANSWER"
	payload: { targetPeerId: string, answer: RTCSessionDescription }
}

// ICE
export interface SendIceCandidateAction {
	type: "WEBRTC/SEND_ICE_CANDIDATE",
	payload: { targetPeerId: string, candidate: RTCIceCandidate }
}
export interface ReceiveIceCandidateAction {
	type: "WEBRTC/RECEIVE_ICE_CANDIDATE",
	payload: { sourcePeerId: string, candidate: RTCIceCandidate }
}

export interface StopCommunicationAction {
	type: "WEBRTC/STOP_COMMUNICATION",
	payload: string
}

// STATE CHANGE
export interface OnPeerConnectionCreatedAction {
	type: "WEBRTC/ON_PEER_CONNECTION_CREATED",
	payload: string;
}
export interface OnPeerConnectionClosedAction {
	type: "WEBRTC/ON_PEER_CONNECTION_CLOSED",
	payload: string;
}
export interface OnConnectionStateChangeAction {
	type: "WEBRTC/ON_CONNECTION_STATE_CHANGE",
	payload: { targetPeerId: string, state: RTCPeerConnectionState };
}
export interface OnIceConnectionStateChangeAction {
	type: "WEBRTC/ON_ICE_CONNECTION_STATE_CHANGE",
	payload: { targetPeerId: string, state: RTCIceConnectionState };
}
export interface OnIceGatheringStateChangeAction {
	type: "WEBRTC/ON_ICE_GATHERING_STATE_CHANGE",
	payload: { targetPeerId: string, state: RTCIceGatherCandidate };
}
export interface OnSignalingStateChangeAction {
	type: "WEBRTC/ON_SIGNALING_STATE_CHANGE",
	payload: { targetPeerId: string, state: RTCSignalingState };
}
export interface OnErrorAction {
	type: "WEBRTC/ON_ERROR",
	payload: { targetPeerId: string, error: string };
}

// DATA CHANNEL
export interface OnDataChannelOpenAction {
	type: "WEBRTC/ON_DATA_CHANNEL_OPEN",
	payload: string;
}
export interface OnDataChannelCloseAction {
	type: "WEBRTC/ON_DATA_CHANNEL_CLOSE",
	payload: string;
}
export interface OnDataChannelErrorAction {
	type: "WEBRTC/ON_DATA_CHANNEL_ERROR",
	payload: { peerId: string, error: any };
}
export interface OnDataChannelMessageInAction {
	type: "WEBRTC/ON_DATA_CHANNEL_MESSAGE_IN",
	payload: { peerId: string, message: any };
}
export interface OnDataChannelMessageOutAction {
	type: "WEBRTC/ON_DATA_CHANNEL_MESSAGE_OUT",
	payload: { peerId: string, message: any };
}

// SERVICE
export interface StopCommunicationAction {
	type: "WEBRTC/STOP_COMMUNICATION",
	payload: string
}

export type WebRtcAction =
	CreateOfferAction | SendOfferAction | ReceiveAnswerAction
	| ReceiveOfferAction /*| CreateAnswerAction */| SendAnswerAction
	| SendIceCandidateAction | ReceiveIceCandidateAction
	| OnPeerConnectionCreatedAction | OnPeerConnectionClosedAction 
	| OnConnectionStateChangeAction | OnSignalingStateChangeAction
	| OnIceConnectionStateChangeAction | OnIceGatheringStateChangeAction | OnErrorAction
	| OnDataChannelOpenAction | OnDataChannelCloseAction | OnDataChannelErrorAction
	| OnDataChannelMessageInAction | OnDataChannelMessageOutAction
	| StopCommunicationAction

// OFFERER
const createOffer = (targetPeerId: string): CreateOfferAction => ({
	type: "WEBRTC/CREATE_OFFER",
	payload: targetPeerId
})
const sendOffer = (targetPeerId: string, offer: RTCSessionDescription): SendOfferAction => ({
	type: "WEBRTC/SEND_OFFER",
	payload: { targetPeerId, offer }
})
const receiveAnswer = (sourcePeerId: string, offer: RTCSessionDescriptionInit): ReceiveAnswerAction => ({
	type: "WEBRTC/RECEIVE_ANSWER",
	payload: { sourcePeerId, offer }
})

// ANSWERER
const receiveOffer = (sourcePeerId: string, offer: RTCSessionDescriptionInit): ReceiveOfferAction => ({
	type: "WEBRTC/RECEIVE_OFFER",
	payload: { sourcePeerId, offer }
})
// const createAnswer = (targetPeerId: string, answer: RTCSessionDescription): CreateAnswerAction => ({
// 	type: "WEBRTC/CREATE_ANSWER",
// 	payload: { targetPeerId, answer }
// })
const sendAnswer = (targetPeerId: string, answer: RTCSessionDescription): SendAnswerAction => ({
	type: "WEBRTC/SEND_ANSWER",
	payload: { targetPeerId, answer }
})

// ICE
const sendIceCandidate = (targetPeerId: string, candidate: RTCIceCandidate): SendIceCandidateAction => ({
	type: "WEBRTC/SEND_ICE_CANDIDATE",
	payload: { targetPeerId, candidate }
})
const receiveIceCandidate = (sourcePeerId: string, candidate: RTCIceCandidate): ReceiveIceCandidateAction => ({
	type: "WEBRTC/RECEIVE_ICE_CANDIDATE",
	payload: { sourcePeerId, candidate }
})

// STATE CHANGE
const peerConnectionCreated = (targetPeerId: string): OnPeerConnectionCreatedAction => ({
	type: "WEBRTC/ON_PEER_CONNECTION_CREATED",
	payload: targetPeerId
})
const peerConnectionClosed = (targetPeerId: string): OnPeerConnectionClosedAction => ({
	type: "WEBRTC/ON_PEER_CONNECTION_CLOSED",
	payload: targetPeerId
})
const connectionStateChanged = (targetPeerId: string, state: RTCPeerConnectionState): OnConnectionStateChangeAction => ({
	type: "WEBRTC/ON_CONNECTION_STATE_CHANGE",
	payload: { targetPeerId, state }
})
const signalingStateChanged = (targetPeerId: string, state: RTCSignalingState): OnSignalingStateChangeAction => ({
	type: "WEBRTC/ON_SIGNALING_STATE_CHANGE",
	payload: { targetPeerId, state }
})
const iceConnectionStateChanged = (targetPeerId: string, state: RTCIceConnectionState): OnIceConnectionStateChangeAction => ({
	type: "WEBRTC/ON_ICE_CONNECTION_STATE_CHANGE",
	payload: { targetPeerId, state }
})
const iceGatheringStateChanged = (targetPeerId: string, state: RTCIceGatherCandidate): OnIceGatheringStateChangeAction => ({
	type: "WEBRTC/ON_ICE_GATHERING_STATE_CHANGE",
	payload: { targetPeerId, state }
})
const signalingError = (targetPeerId: string, error: string): OnErrorAction => ({
	type: "WEBRTC/ON_ERROR",
	payload: { targetPeerId, error }
})

// DATA CHANNEL
const dataChannelOpen = (peerId: string) => ({
	type: "WEBRTC/ON_DATA_CHANNEL_OPEN",
	payload: peerId
})
const dataChannelClose = (peerId: string) => ({
	type: "WEBRTC/ON_DATA_CHANNEL_CLOSE",
	payload: peerId
})
const dataChannelError = (peerId: string, error: any) => ({
	type: "WEBRTC/ON_DATA_CHANNEL_ERROR",
	payload: { peerId, error }
})
const dataChannelMessageIn = (peerId: string, message: any) => ({
	type: "WEBRTC/ON_DATA_CHANNEL_MESSAGE_IN",
	payload: { peerId, message }
})
const dataChannelMessageOut = (peerId: string, message: any) => ({
	type: "WEBRTC/ON_DATA_CHANNEL_MESSAGE_OUT",
	payload: { peerId, message }
})

const stopCommunication = (targetPeerId: string): StopCommunicationAction => ({
	type: "WEBRTC/STOP_COMMUNICATION",
	payload: targetPeerId
})

export const actionCreators = {
	createOffer, sendOffer, receiveAnswer,
	receiveOffer, /*createAnswer, */sendAnswer,
	sendIceCandidate, receiveIceCandidate,
	peerConnectionCreated, peerConnectionClosed,
	connectionStateChanged, signalingStateChanged, iceConnectionStateChanged, iceGatheringStateChanged, signalingError,
	dataChannelOpen, dataChannelClose, dataChannelError, dataChannelMessageIn, dataChannelMessageOut,
	stopCommunication
}