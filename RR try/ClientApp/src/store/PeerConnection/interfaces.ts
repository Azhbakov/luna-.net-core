
export interface WebRtcState {
	peerConnectionStates: IPeerConnectionState[]
}

export interface IPeerConnectionState {
	peerId: string;
	connectionState: RTCPeerConnectionState
	iceConnectionState: RTCIceConnectionState;
	iceGatheringState: RTCIceGatherCandidate;
	signalingState: RTCSignalingState;
	iceCandidates: number;
	error: string;
	dataChannelState: string;
	dataChannelError: string;
	dataChannelMessagesIn: number,
	dataChannelMessagesOut: number,
}
export const createPeerConnectionState = (peerId: string): IPeerConnectionState => ({
	peerId, iceCandidates: 0, dataChannelMessagesIn: 0, dataChannelMessagesOut: 0
} as IPeerConnectionState)

export const InitialWebRtcState: WebRtcState = {
	peerConnectionStates: [],
}