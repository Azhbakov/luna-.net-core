import { Reducer } from "redux";
import { WebRtcState, InitialWebRtcState, createPeerConnectionState } from "./interfaces";
import { WebRtcAction, actionTypes, OnPeerConnectionCreatedAction, OnPeerConnectionClosedAction, OnIceConnectionStateChangeAction, OnSignalingStateChangeAction, ReceiveIceCandidateAction, OnErrorAction, OnDataChannelOpenAction, OnDataChannelCloseAction, OnDataChannelErrorAction, OnDataChannelMessageInAction, OnDataChannelMessageOutAction } from "./actions";
import { actionTypes as lobbyActionTypes } from "../Lobby/actions"
import update from "immutability-helper";

export const webRtcReducer: Reducer<WebRtcState> = (state = InitialWebRtcState, action: WebRtcAction): WebRtcState => {
	switch (action.type) {
		case lobbyActionTypes.LOBBY_LEAVE:
			return InitialWebRtcState;
		case actionTypes.ON_PEER_CONNECTION_CREATED:
			return update(state, { peerConnectionStates: { $push: [ createPeerConnectionState((action as OnPeerConnectionCreatedAction).payload)] } });
		case actionTypes.ON_PEER_CONNECTION_CLOSED:
			let index = state.peerConnectionStates.findIndex(p => p.peerId === (action as OnPeerConnectionClosedAction).payload);
			return update(state, { peerConnectionStates: { $splice: [[index, 1]] } });
		case actionTypes.ON_ICE_CONNECTION_STATE_CHANGE:
			const icsAction = (action as OnIceConnectionStateChangeAction);
			index = state.peerConnectionStates.findIndex(p => p.peerId === icsAction.payload.targetPeerId);
			return update(state, { peerConnectionStates: { [index]: { iceConnectionState: { $set: icsAction.payload.state } } } });
		case actionTypes.ON_ICE_GATHERING_STATE_CHANGE:
			const igsAction = (action as OnIceConnectionStateChangeAction);
			index = state.peerConnectionStates.findIndex(p => p.peerId === igsAction.payload.targetPeerId);
			return update(state, { peerConnectionStates: { [index]: { iceGatheringState: {$set: igsAction.payload.state } } } });
		case actionTypes.ON_SIGNALING_STATE_CHANGE:
			const issAction = (action as OnSignalingStateChangeAction);
			index = state.peerConnectionStates.findIndex(p => p.peerId === issAction.payload.targetPeerId);
			return update(state, { peerConnectionStates: { [index]: { signalingState: {$set: issAction.payload.state } } } });
		case actionTypes.ON_CONNECTION_STATE_CHANGE:
			const csAction = (action as OnSignalingStateChangeAction);
			index = state.peerConnectionStates.findIndex(p => p.peerId === csAction.payload.targetPeerId);
			return update(state, { peerConnectionStates: { [index]: { signalingState: { $set: csAction.payload.state } } } });
		case actionTypes.RECEIVE_ICE_CANDIDATE:
			const riAction = (action as ReceiveIceCandidateAction);
			index = state.peerConnectionStates.findIndex(p => p.peerId === riAction.payload.sourcePeerId);
			return update(state, { peerConnectionStates: { [index]: { iceCandidates: { $set: state.peerConnectionStates[index].iceCandidates + 1 } } } });
		case actionTypes.ON_ERROR:
			const eAction = (action as OnErrorAction);
			index = state.peerConnectionStates.findIndex(p => p.peerId === eAction.payload.targetPeerId);
			return update(state, { peerConnectionStates: { [index]: { error: { $set: eAction.payload.error } } } });

		// DATA CHANNEL
		case actionTypes.ON_DATA_CHANNEL_OPEN:
			const dcoAction = (action as OnDataChannelOpenAction);
			index = state.peerConnectionStates.findIndex(p => p.peerId === dcoAction.payload);
			return update(state, { peerConnectionStates: { [index]: { dataChannelState: { $set: 'open' } } } });
		case actionTypes.ON_DATA_CHANNEL_CLOSE:
			const dccAction = (action as OnDataChannelCloseAction);
			index = state.peerConnectionStates.findIndex(p => p.peerId === dccAction.payload);
			return update(state, { peerConnectionStates: { [index]: { dataChannelState: { $set: 'closed' } } } });
		case actionTypes.ON_DATA_CHANNEL_ERROR:
			const dceAction = (action as OnDataChannelErrorAction);
			index = state.peerConnectionStates.findIndex(p => p.peerId === dceAction.payload.peerId);
			return update(state, { peerConnectionStates: { [index]: { dataChannelError: { $set: dceAction.payload.error.message } } } });
		case actionTypes.ON_DATA_CHANNEL_MESSAGE_IN:
			const dcmiAction = (action as OnDataChannelMessageInAction);
			index = state.peerConnectionStates.findIndex(p => p.peerId === dcmiAction.payload.peerId);
			return state;//update(state, { peerConnectionStates: { [index]: { dataChannelMessagesIn: { $set: state.peerConnectionStates[index].dataChannelMessagesIn + 1 } } } });
		case actionTypes.ON_DATA_CHANNEL_MESSAGE_OUT:
			const dcmoAction = (action as OnDataChannelMessageOutAction);
			index = state.peerConnectionStates.findIndex(p => p.peerId === dcmoAction.payload.peerId);
			return update(state, { peerConnectionStates: { [index]: { dataChannelMessagesOut: { $set: state.peerConnectionStates[index].dataChannelMessagesOut + 1 } } } });
		default:
			return state;
	}
}