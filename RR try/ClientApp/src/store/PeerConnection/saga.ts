import { put, call, take, fork, join, cancel } from "redux-saga/effects";
import * as webRtcModule from "../PeerConnection/actions";
import { actionTypes as webRtcActionTypes, actionCreators as webRtcActions } from "src/store/PeerConnection";
import { eventChannel, Channel } from "redux-saga";
import { HubConnection } from "@aspnet/signalr";

export function* establishPeerConnectionByCreatingOffer(
		targetPeerId: string,
		outputToServer: HubConnection,
		onDataChannel: (dataChannel: RTCDataChannel) => void) {
	const peerConnection: RTCPeerConnection = new RTCPeerConnection({ iceServers: [ { urls: 'stun:stun.l.google.com:19302'} ] });
	yield put(webRtcActions.peerConnectionCreated(targetPeerId));

	// Create peer connection and  peer channel
	const peerChannel = eventChannel(emitter => {
		initPeerConnection(peerConnection, outputToServer, targetPeerId, emitter, false); // true to enable support for media changes (seems not necessary)
		
		const dataChannel = peerConnection.createDataChannel(null);
		initDataChannel(dataChannel, targetPeerId, emitter);
		onDataChannel(dataChannel);

		return () => {
			peerConnection.close();
			emitter(webRtcActions.peerConnectionClosed(targetPeerId));
		}
	});

	// Fork and retranslate WebRtc signaling from channel to global space, server connection is listening
	const retranslateFromChannel = yield fork(broadcastChannelActions, peerChannel);

	// Fork and start listening for offer acceptance and ICE candidates via server connection
	const serverInputProcessing = yield fork(processServerInput, targetPeerId, peerConnection);

	// Fork and start listening to peer messages
	// yield takeEvery(peerChannel, retranslateChannelActions);

	try {
		// Create and send offer
		try {
			const offer = yield call([peerConnection, peerConnection.createOffer]);
			yield call([peerConnection, peerConnection.setLocalDescription], offer);
			if (!peerConnection.localDescription) throw new Error("Expected local description to be set.");
			yield call([outputToServer, outputToServer.invoke], "Retranslate", targetPeerId, { method: "ReceiveOffer", offer: peerConnection.localDescription });

			yield join(serverInputProcessing);
		} catch (error) {
			console.log(error.message);
			yield put(webRtcActions.signalingError(targetPeerId, error.message));
		}
	
	} finally {
		yield cancel(retranslateFromChannel, serverInputProcessing, retranslateFromChannel);
		peerChannel.close();
		peerConnection.close();
	}
}

export function* establishPeerConnectionByAcceptingOffer(peerId: string, offer: RTCSessionDescriptionInit,
		outputToServer: HubConnection,
		onDataChannel: (dataChannel: any) => void) {
	const sourcePeerId = peerId;
	const peerConnection: RTCPeerConnection = new RTCPeerConnection({ iceServers: [ { urls: 'stun:stun.l.google.com:19302'} ] });
	yield put(webRtcActions.peerConnectionCreated(sourcePeerId));

	const peerChannel = eventChannel(emitter => {
		initPeerConnection(peerConnection, outputToServer, sourcePeerId, emitter, false);
		
		peerConnection.ondatachannel = (event) => {
			initDataChannel(event.channel, sourcePeerId, emitter);
			onDataChannel(event.channel);
		}
		
		return () => {
			peerConnection.close();
			emitter(webRtcActions.peerConnectionClosed(sourcePeerId));
		}
	});

	// Fork and retranslate WebRtc signaling from channel to global space, server connection is listening
	const retranslateFromChannel = yield fork(broadcastChannelActions, peerChannel);

	// Fork and start listening for ICE candidates via server connection
	const serverInputProcessing = yield fork(processServerInput, sourcePeerId, peerConnection);

	// Accept offer and send answer
	try {
		try {
			yield call([peerConnection, peerConnection.setRemoteDescription], offer);
			const answer: RTCSessionDescriptionInit = yield call([peerConnection, peerConnection.createAnswer]);
			yield call([peerConnection, peerConnection.setLocalDescription], answer);
			if (!peerConnection.localDescription) throw new Error("Expected local description to be set.");
			yield call([outputToServer, outputToServer.invoke], "Retranslate", sourcePeerId, { method: "ReceiveAnswer", answer: peerConnection.localDescription });
		} catch (error) {
			console.log(error.message);
			yield put(webRtcActions.signalingError(sourcePeerId, error.message));
		}

		yield join(serverInputProcessing)
	} catch (error) {
		console.log(error.message);
		yield put(webRtcActions.signalingError(sourcePeerId, error.message));
	} finally {
		peerChannel.close();
		peerConnection.close();
		yield cancel(retranslateFromChannel, serverInputProcessing);
	}
}

//
// Waiting procedures
//
function *processServerInput(peerId: string, peerConnection: RTCPeerConnection) {
	while (true) {
		const action = yield take([webRtcActionTypes.RECEIVE_ANSWER, webRtcActionTypes.RECEIVE_ICE_CANDIDATE]);
		if (action.payload.sourcePeerId !== peerId) continue;

		switch (action.type) {
			case webRtcActionTypes.RECEIVE_ANSWER:
				const receiveAnswerAction = action as webRtcModule.ReceiveAnswerAction;
				yield call([peerConnection, peerConnection.setRemoteDescription], receiveAnswerAction.payload.offer as RTCSessionDescriptionInit); // TODO: await promise, check errors
				break;

			case webRtcActionTypes.RECEIVE_ICE_CANDIDATE:
				const receiveIceAction = action as webRtcModule.ReceiveIceCandidateAction;
				if (receiveIceAction.payload.sourcePeerId !== peerId) continue;
				yield call([peerConnection, peerConnection.addIceCandidate], receiveIceAction.payload.candidate); // TODO: await promise, check errors
				break;
				
			default:
				throw new Error(`Unprocessed message from server: ${action.type}`)
		}
	}
}

function* broadcastChannelActions(channel: Channel<any>) {
	while (true) {
		const action = yield take(channel);
		yield put(action);
	}
}

//
// WebRTC initializers
//
function initPeerConnection(peerConnection: RTCPeerConnection, outputToServer: HubConnection, targetPeerId: string, emitter: any, renegotiate: boolean): void {
	peerConnection.onicecandidate = (event) => {
		if (event.candidate) {
			outputToServer.invoke("Retranslate", targetPeerId, { method: 'ReceiveIceCandidate', candidate: event.candidate });
		} else { /* TODO: handle all ICE candidates sent */}
	}
	peerConnection.onconnectionstatechange = (event) => { emitter(webRtcActions.connectionStateChanged(targetPeerId, peerConnection.connectionState)) }
	peerConnection.onsignalingstatechange = (event) => { emitter(webRtcActions.signalingStateChanged(targetPeerId, peerConnection.signalingState)) }
	peerConnection.oniceconnectionstatechange = (event) => { emitter(webRtcActions.iceConnectionStateChanged(targetPeerId, peerConnection.iceConnectionState)) }
	peerConnection.onicegatheringstatechange = (event) => { emitter(webRtcActions.iceGatheringStateChanged(targetPeerId, peerConnection.iceGatheringState)) }
	peerConnection.onicecandidateerror = (event) => { console.log(event.errorText); emitter(webRtcActions.signalingError(targetPeerId, event.errorText)) }

	if (renegotiate) {
		peerConnection.onnegotiationneeded = () => {
			peerConnection.createOffer().then(offer => {
				return peerConnection.setLocalDescription(offer);
			}).then(() => {
				if (!peerConnection.localDescription) throw new Error("Expected local description to be set.");
				outputToServer.invoke("Retranslate", targetPeerId, { method: 'ReceiveAnswer', answer: peerConnection.localDescription })
			}).catch(error => {
				console.log(error);
				emitter(webRtcActions.signalingError(targetPeerId, error.message));
			});
		}
	}
}

function initDataChannel(dataChannel: RTCDataChannel, peerId: string, emitter: any) {
	dataChannel.onopen = () => {
		emitter(webRtcActions.dataChannelOpen(peerId));
	}
	dataChannel.onclose = () => {
		emitter(webRtcActions.dataChannelClose(peerId));
	}
	dataChannel.onerror = (event) => {
		console.log(event.error);
		if (event.error) emitter(webRtcActions.dataChannelError(peerId, event.error));
	}
	dataChannel.onmessage = (event) => {
		emitter(webRtcActions.dataChannelMessageIn(peerId, event.data));
	}
}
