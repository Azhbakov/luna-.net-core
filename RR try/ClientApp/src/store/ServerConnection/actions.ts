
export const actionTypes = {
	SC_START: 'SC/START',
	SC_STARTED: 'SC/STARTED',

	SC_STOP: 'SC/STOP',
	SC_STOPPED: 'SC/STOPPED',

	SC_ERROR: 'SC/ERROR',

	SC_MESSAGE: 'SC/MESSAGE'
}

export interface StartAction {
	type: 'SC/START'
}
export interface StartedAction {
	type: 'SC/STARTED'
}
export interface StopAction {
	type: 'SC/STOP'
}
export interface StoppedAction {
	type: 'SC/STOPPED'
}
export interface ErrorAction {
	type: 'SC/ERROR'
	payload: string
}
export interface MessageAction {
	type: 'SC/MESSAGE'
	payload: { method: string, data: any}
}
export type ServerConnectionAction = 
	StartAction | StartedAction |
	StopAction | StoppedAction |
	ErrorAction |
	MessageAction;

const start = (): StartAction => ({ type: "SC/START" });
const started = (): StartedAction => ({ type: 'SC/STARTED' })
const stop = (): StopAction => ({ type: 'SC/STOP' })
const stopped = (): StoppedAction => ({ type: "SC/STOPPED" })
const error = (err: string): ErrorAction => ({ type: 'SC/ERROR', payload: err })
const message = (method: string, data: any): MessageAction => ({ type: 'SC/MESSAGE', payload: { method, data } })

export const actionCreators = { 
	start, started, 
	stop, stopped,
	error, 
	message
}
