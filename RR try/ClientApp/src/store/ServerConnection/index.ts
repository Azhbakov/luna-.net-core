export * from './actions';
export * from './reducer';
export * from './interfaces';
export * from './saga';

import { serverUrl } from 'src/store';

export const serverHubUrl = serverUrl + 'api/lobby/';
