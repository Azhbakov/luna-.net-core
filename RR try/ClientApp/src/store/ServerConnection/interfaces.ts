import { END } from "redux-saga";

export interface ServerConnectionState {
	status: 'CONNECTED' | 'DISCONNECTED';
	error: string | null;
}

export const InitialServerConnectionState: ServerConnectionState = {
	status: 'DISCONNECTED',
	error: null
}

export interface IServerConnectionClient {
	registerMethods(connection: signalR.HubConnection, emitter: (input: {} | END) => void): void;
	registerMethodInvokers(connection: signalR.HubConnection, emitter: (input: {} | END) => void): void;
}