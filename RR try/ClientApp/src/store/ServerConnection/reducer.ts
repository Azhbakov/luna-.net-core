import { ServerConnectionState, InitialServerConnectionState } from "./interfaces";
import { Reducer } from "redux";
import { actionTypes, ServerConnectionAction, ErrorAction } from "./actions";
import update from "immutability-helper";

export const serverConnectionReducer: Reducer<ServerConnectionState>
= (state = InitialServerConnectionState, action: ServerConnectionAction): ServerConnectionState => {
	switch (action.type) {
		case actionTypes.SC_STARTED:
			console.log("Established server connection.");
			return update(state, { status: { $set: 'CONNECTED'} });
		case actionTypes.SC_STOPPED:
			console.log("Server connection closed.");
			return update(state, { status: { $set: 'DISCONNECTED'} });
		case actionTypes.SC_ERROR:
			return update(state, { error: { $set: (action as ErrorAction).payload } });
		default:
			return state;
	}
}