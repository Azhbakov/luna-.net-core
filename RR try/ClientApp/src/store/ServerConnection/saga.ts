import { Channel, eventChannel, buffers } from "redux-saga";
import { call, take, put, fork, cancel, select, all } from "redux-saga/effects";
import * as signalR from "@aspnet/signalr";
import { serverHubUrl } from ".";
import { actionTypes as lobbyActionTypes, actionCreators as lobbyActions } from "../Lobby"
import { actionTypes as webRtcActionTypes, actionCreators as webRtcActions } from "src/store/PeerConnection";
import { actionTypes as scActionTypes, actionCreators as scActions, ErrorAction } from "./actions"
import { ILobby } from "src/domain/Lobby";
import { ExecutionSettings } from "src/domain/ExecutionSettings";
import { AppState, AppDynamicContext } from "../configureStore";

export interface ServerConnectionDynamicContext {
	serverConnection?: signalR.HubConnection
}
export const InitialServerConnectionDynamicContext: ServerConnectionDynamicContext = {
	serverConnection: undefined
}

export function* serverConnectionSaga(appDC: AppDynamicContext) {
	const dynamicContext = appDC.ServerConnectionDC;
	while (true) {
		try {
			yield take(scActionTypes.SC_START);
			
			const serverChannel = yield call(startServerConnection, dynamicContext);
			
			// Await connecting to server
			const firstEvent = yield take(serverChannel); // only SC_STARTED is expected
			switch (firstEvent.type) {
				case scActionTypes.SC_STARTED: 
					yield put(firstEvent); // retranslate for UI
					break;
				case scActionTypes.SC_ERROR:
					throw new Error("Failed to establish server connection: " + (firstEvent as ErrorAction).payload);
				default:
					throw new Error(`Unexpected message when establishing server connection: expected ${scActionTypes.SC_START}, got ${firstEvent.type}`);
			}

			const retranslationSaga = yield fork(broadcastServerConnectionEvents, serverChannel);
			
			// Wait for server connection close
			const action = yield take([scActionTypes.SC_STOP, scActionTypes.SC_STOPPED]);
			
			// Stop retranslation
			yield cancel(retranslationSaga);
			(serverChannel as Channel<{}>).close();

			if (action.type === scActionTypes.SC_STOP && dynamicContext.serverConnection) {
				yield call([dynamicContext.serverConnection, dynamicContext.serverConnection.stop]);
			}
		} catch (error) {
			console.log(error.message);
			if (dynamicContext.serverConnection) yield call([dynamicContext.serverConnection, dynamicContext.serverConnection.stop]);
		} finally {
			dynamicContext.serverConnection = undefined;
		}
	}
}

function *startServerConnection(dynamicContext: ServerConnectionDynamicContext) {
	const serverConnection = new signalR.HubConnectionBuilder()
		.withUrl(serverHubUrl)
		.build();

	dynamicContext.serverConnection = serverConnection;

	const serverChannel = eventChannel(emitter => {
		// System handlers
		serverConnection.onclose(error => {
			if (error) console.error(`Server connection error: ${error.message}`);
			emitter(scActions.stopped)
		});

		// Lobby message handlers
		serverConnection.on('OnPeerConnected', (data: { connectedPeerId: string, lobby: ILobby }) => {
			emitter(lobbyActions.lobbyPeerConnected(data.connectedPeerId, data.lobby));
		});
		serverConnection.on('OnPeerDisconnected', (data: { disconnectedPeerId: string, lobby: ILobby }) => {
			emitter(lobbyActions.lobbyPeerDisconnected(data.disconnectedPeerId, data.lobby));
		});
		serverConnection.on("LobbyUpdate", (data: { lobby: ILobby }) => {
			emitter(lobbyActions.lobbyUpdate(data.lobby));
		});
		serverConnection.on("LobbyStarted", (data: { lobby: ILobby, settings: ExecutionSettings }) => {
			emitter(lobbyActions.lobbyStarted(data.lobby, data.settings));
		});

		serverConnection.on("ReceiveMessage", (data: { sourcePeerId: string, message: any }) => {
			switch (data.message.method) {
				// RTC message handlers
				case "ReceiveOffer":
					emitter(webRtcActions.receiveOffer(data.sourcePeerId, data.message.offer));
					break;
				case "ReceiveAnswer":
					emitter(webRtcActions.receiveAnswer(data.sourcePeerId, data.message.answer));
					break;
				case "ReceiveIceCandidate":
					emitter(webRtcActions.receiveIceCandidate(data.sourcePeerId, data.message.candidate));
					break;
				default:
					throw new Error(`Received unknown message from peer ${data.sourcePeerId}: ${data.message}`);
			}
		});
	
		serverConnection.start()
			.then(() => {
				emitter(scActions.started());
			})
			.catch(error => {
				emitter(scActions.error(error.message));
			})

		return () => { return; }
	}, buffers.fixed(20));

	return serverChannel;
}

function *broadcastServerConnectionEvents(serverChannel: Channel<{}>) {
	while (true) {
		const action = yield take(serverChannel);
		yield put(action);
	}
}

export function *establishServerConnection() {
	const connectionStatus = yield select((appState: AppState) => appState.serverConnection.status);
	if (connectionStatus === 'CONNECTED') return;
	yield all([
		put(scActions.start()),
		take(scActionTypes.SC_STARTED)
	]);
}