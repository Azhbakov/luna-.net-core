import { Task } from "./interfaces";
import { lunaLang } from "src/domain/engine/lang_processing/lunaLang";
import { Atoms } from "src/domain/engine/lang_processing/cf";

export const actionTypes = {
    TASK_FETCH_START: 'TASK_FETCH_START',
    TASK_FETCH_SUCCESS: 'TASK_FETCH_SUCCESS',
    TASK_FETCH_FAILURE: 'TASK_FETCH_FAILURE',
 
    TASK_REMOVE_START: 'TASK_REMOVE_START',
    TASK_REMOVE_SUCCESS: 'TASK_REMOVE_SUCCESS',
	TASK_REMOVE_FAILURE: 'TASK_REMOVE_FAILURE',
	
	// FETCH_ITEM_START: 'FETCH_ITEM_START',
	// FETCH_ITEM_FINISH: 'FETCH_ITEM_FINISH'
	TASK_DATA_FETCHED: 'TASK_DATA_FETCHED',
}

export interface TaskFetchStartAction {
    type: 'TASK_FETCH_START'
}
export interface TaskFetchSuccessAction {
    type: 'TASK_FETCH_SUCCESS',
    payload: Task[]
}
export interface TaskFetchFailureAction {
    type: 'TASK_FETCH_FAILURE',
    error: Error
}

export interface TaskRemoveStartAction {
    type: 'TASK_REMOVE_START'
    payload: string
}
export interface TaskRemoveSuccessAction {
    type: 'TASK_REMOVE_SUCCESS',
    payload: string
}
export interface TaskRemoveFailureAction {
    type: 'TASK_REMOVE_FAILURE',
    payload: string,
    error: Error
}
export interface FetchTaskDataAction {
	type: 'TASK_DATA_FETCHED',
	payload: { taskId: string, atoms: Atoms, program: lunaLang.Program }
}

export type TaskFetchAction = 
	TaskFetchStartAction | TaskFetchSuccessAction | TaskFetchFailureAction
	| FetchTaskDataAction; 
// TaskRemoveStartAction | TaskRemoveSuccessAction | TaskRemoveFailureAction;

// ACTION CREATORS
const taskFetchStart = (): TaskFetchStartAction => ({
    type: 'TASK_FETCH_START'
});
const taskFetchFailure = (error: Error): TaskFetchFailureAction => ({
    type: 'TASK_FETCH_FAILURE',
    error
});
const taskFetchSuccess = (tasks: Task[]): TaskFetchSuccessAction => ({
    type: 'TASK_FETCH_SUCCESS',
    payload: tasks
});

const taskRemoveStart = (id: string): TaskRemoveStartAction => ({
    type: 'TASK_REMOVE_START',
    payload: id
});
const taskRemoveFailure = (id: string, error: Error): TaskRemoveFailureAction => ({
    type: 'TASK_REMOVE_FAILURE',
    payload: id,
    error
});
const taskRemoveSuccess = (id: string): TaskRemoveSuccessAction => ({
    type: 'TASK_REMOVE_SUCCESS',
    payload: id
});

const taskDataFetched = (taskId: string, atoms: Atoms, program: lunaLang.Program): FetchTaskDataAction => ({
	type: 'TASK_DATA_FETCHED',
	payload: { taskId, atoms, program }
})


export const actionCreators = { 
    taskFetchStart, taskFetchSuccess, taskFetchFailure, 
	taskRemoveStart, taskRemoveSuccess, taskRemoveFailure,
	taskDataFetched
}