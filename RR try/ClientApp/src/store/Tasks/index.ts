import { serverUrl } from '..';

export * from './saga';
export * from './reducer';
export * from './interfaces';
export * from './actions';

export const tasksFetchUrl = serverUrl + 'api/tasks/';
export const taskAtomsFetchUrl = (id: string) => serverUrl + 'api/tasks/' + id + '/atoms';
export const taskProgramFetchUrl = (id: string) => serverUrl + 'api/tasks/' + id + '/program';
export const taskRemoveUrl = (id: string) => serverUrl + 'api/tasks/' + id;