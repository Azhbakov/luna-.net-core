import { lunaLang } from "src/domain/engine/lang_processing/lunaLang";
import { Atoms } from "src/domain/engine/lang_processing/cf";

export interface Task {
    id: string,
    name: string,
    atoms: Atoms | null,
    program: lunaLang.Program | null
}

export interface TasksState {
    isLoading: boolean,
    tasks: Task[],
	error: Error | null
}

export const InitialTasksState: TasksState = {
    isLoading: false,
    tasks: [],
    error: null
}

// TODO later if needed
// export interface FetchTaskDataState {
//     loadingItems: string[]
// 	error: Error | null
// }

// export const InitialFetchTaskDataState: FetchTaskDataState = {
//     loadingItems: [],
//     error: null
// }