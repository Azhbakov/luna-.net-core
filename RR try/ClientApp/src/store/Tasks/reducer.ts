import { TasksState, InitialTasksState } from "./interfaces";
import { Reducer } from "redux";
import { TaskFetchAction, actionTypes, TaskFetchSuccessAction, TaskFetchFailureAction, FetchTaskDataAction } from "./actions";
import update from "immutability-helper";

export const taskFetchReducer: Reducer<TasksState> =
(state: TasksState = InitialTasksState, action: TaskFetchAction): TasksState =>
{
    switch (action.type) {
        case actionTypes.TASK_FETCH_START:
            return {
                isLoading: true,
                tasks: [],
                error: null
            }
        case actionTypes.TASK_FETCH_SUCCESS:
            return {
                isLoading: false,
                tasks: (action as TaskFetchSuccessAction).payload,
                error: null
            }
        case actionTypes.TASK_FETCH_FAILURE:
            return {
                isLoading: false,
                tasks: [],
                error: (action as TaskFetchFailureAction).error,
			}
		case actionTypes.TASK_DATA_FETCHED:
			const { taskId, atoms, program } = (action as FetchTaskDataAction).payload;
			const taskInd = state.tasks.findIndex(t => t.id === taskId);
			if (taskInd === -1) return state;
			return update(state, { tasks: { [taskInd]: 
					{ 
						atoms: { $set: atoms }, 
						program: { $set: program 
					} 
			} } });
        default:
            return state;
    }
}