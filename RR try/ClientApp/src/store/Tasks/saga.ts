import { call, put, takeLatest, takeEvery } from 'redux-saga/effects';
import { actionTypes, actionCreators, taskRemoveUrl, TaskRemoveStartAction, taskAtomsFetchUrl, taskProgramFetchUrl } from '.';
import { tasksFetchUrl } from '.';
import { parseAtoms, parseProgram } from 'src/domain/engine/runtime/parser';

export function* taskFetchSaga() {
    yield takeLatest(actionTypes.TASK_FETCH_START, fetchTasks);
    yield takeEvery(actionTypes.TASK_REMOVE_START, removeTask);
    yield takeLatest(actionTypes.TASK_REMOVE_SUCCESS, fetchTasks);
}

function* fetchTasks() {
    try {
        const response = yield call(fetch, tasksFetchUrl);        
        const tasks = yield call([response, response.json]);
        yield put(actionCreators.taskFetchSuccess(tasks));
    } catch (e) {
        yield put(actionCreators.taskFetchFailure(e));
        return;
    }
}

function* removeTask(action: TaskRemoveStartAction) {
    const id = action.payload;
    try {
        const response = yield call(fetch, taskRemoveUrl(id), { method: "DELETE"});
        if (response.ok) {
            yield put(actionCreators.taskRemoveSuccess(id));
        } else {
            yield put(actionCreators.taskRemoveFailure(id, new Error(`${response.status}: ${response.statusText}`)));
        }
    } catch (e) {
        yield put(actionCreators.taskRemoveFailure(id, e));
        return;
    }
}

export function *fetchTaskData(taskId: string) {
	const atomsText = yield call(fetchText, taskAtomsFetchUrl(taskId));
	const atoms = parseAtoms(atomsText);
	const programText = yield call(fetchText, taskProgramFetchUrl(taskId));
	const program = parseProgram(programText);
	return { atoms, program };
}

function *fetchText(url: string) {
	const res = yield call(fetch, url);
	if (res.status !== 200 || !res.body) {
		throw res.statusText;
	}
	return yield call([res, res.text]);
}