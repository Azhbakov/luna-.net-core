export type FileType = 'fa' | 'atoms'

export const actionTypes = {
    UPLOAD_START: 'UPLOAD_START',
    UPLOAD_PROGRESS: 'UPLOAD_PROGRESS',
    UPLOAD_SUCCESS: 'UPLOAD_SUCCESS',
    UPLOAD_FAILURE: 'UPLOAD_FAILURE',
    UPLOAD_SET_FILE: 'UPLOAD_SET_FILE'
}

export interface UploadStartAction { 
    type: 'UPLOAD_START',
    taskName: string
    faFile: File,
    atomsFile: File
};
export interface UploadProgressAction { 
    type: 'UPLOAD_PROGRESS',
    payload: number,
};
export interface UploadSuccessAction { 
    type: 'UPLOAD_SUCCESS',
};
export interface UploadFailureAction { 
    type: 'UPLOAD_FAILURE',
    payload: Error,
    isError: boolean
};

export type UploadAction = 
    UploadStartAction 
    | UploadProgressAction 
    | UploadSuccessAction 
    | UploadFailureAction;

const uploadStart = (taskName: string, faFile: File, atomsFile: File): UploadStartAction => ({
    type: 'UPLOAD_START',
    taskName, faFile, atomsFile
});

const uploadProgress = (progress: number): UploadProgressAction => ({
    type: 'UPLOAD_PROGRESS',
    payload: progress,
});

const uploadSuccess = (): UploadSuccessAction => ({
    type: 'UPLOAD_SUCCESS',
});

const uploadFailure = (error: Error): UploadFailureAction => ({
    type: 'UPLOAD_FAILURE',
    payload: error,
    isError: true,
});

export const actionCreators = {
    uploadStart,
    uploadProgress,
    uploadSuccess,
    uploadFailure,
}