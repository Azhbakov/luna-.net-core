import { serverUrl } from '..';

export * from './saga';
export * from './reducer';
export * from './interfaces'
export * from './actions'

export const uploadUrl = serverUrl + 'api/tasks/';