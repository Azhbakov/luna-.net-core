export interface TaskUploadState {
    status: 'HIDDEN' | 'SHOWN' | 'UPLOADING' | 'SUCCESS' | 'FAILED';
    progress: number;
    error: Error | null;
}

export const InitialUploadState : TaskUploadState = {
    status: 'SHOWN',
    progress: 0,
    error: null
}