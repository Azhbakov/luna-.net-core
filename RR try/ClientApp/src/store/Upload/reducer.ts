import { TaskUploadState, InitialUploadState } from "./interfaces";
import { UploadAction, UploadProgressAction, UploadFailureAction, UploadStartAction } from "./actions";
import { Reducer } from 'redux';
import { actionTypes } from "./actions";
import update from 'immutability-helper';

const uploadStart = (state: TaskUploadState, action: UploadStartAction): TaskUploadState => {
    return update(state, { status: { $set: 'UPLOADING' } });
}

const uploadProgress = (state: TaskUploadState, action: UploadProgressAction): TaskUploadState => {
    return update(state, { status: { $set: 'UPLOADING' }, progress: {  $set: action.payload } });
}

const uploadSuccess = (state: TaskUploadState) => {
    return update(state, { status: { $set: 'SUCCESS' }, progress: {  $set: 1 } });
}

const uploadFailure = (state: TaskUploadState, action: UploadFailureAction) => {
    return update(state, { 
        status: { $set: 'FAILED' },
        error: { $set: action.payload }
    } );
}

export const uploadReducer: Reducer<TaskUploadState> =
(state: TaskUploadState = InitialUploadState, action: UploadAction): TaskUploadState => {
    switch (action.type) {
        case actionTypes.UPLOAD_START:
            return uploadStart(state, action as UploadStartAction);        
        case actionTypes.UPLOAD_PROGRESS:
            return uploadProgress(state, action as UploadProgressAction);
        case actionTypes.UPLOAD_SUCCESS:
            return uploadSuccess(state);
        case actionTypes.UPLOAD_FAILURE:
            return uploadFailure(state, action as UploadFailureAction);
        default:
            return state;
    }
}