import { eventChannel, END, buffers } from 'redux-saga';
import { call, take, put, takeEvery } from 'redux-saga/effects';
import { actionCreators, UploadStartAction, actionTypes } from './actions';
import { uploadUrl } from '.';

export function* uploadSaga() {
    yield takeEvery(actionTypes.UPLOAD_START, function*(action: UploadStartAction) {
        yield call(uploadFilesSaga, action.taskName, action.faFile, action.atomsFile);
    });
}

function* uploadFilesSaga(taskName: string, faFile: File, atomsFile: File) {
    if (!faFile || !atomsFile) throw new Error('Files for uploading are not provided.');
    const formData = new FormData();
    formData.append('taskName', taskName);
    formData.append('fa', faFile);
    formData.append('atoms', atomsFile);

    console.log(formData);
    
    const channel = yield call(createUploadFileChannel, uploadUrl, formData);
    while (true) {
        const { progress = 0, error, success } = yield take(channel);
        if (error) {
            yield put(actionCreators.uploadFailure(error));
            return;
        }
        if (success) {
            yield put(actionCreators.uploadSuccess());
            return;
        }
        yield put(actionCreators.uploadProgress(progress));
    }
}

function createUploadFileChannel(endpoint: string, data: FormData) {
    return eventChannel(emitter => {
        const xhr = new XMLHttpRequest();

        const onProgress = (e: ProgressEvent) => {
            if (e.lengthComputable) {
                const progress = e.loaded / e.total;
                emitter({progress});
            }
        };
        
        const onFailure = (e: ErrorEvent) => {
            emitter({error: new Error('Upload failed')});
            emitter(END);
        }

        xhr.upload.addEventListener('progress', onProgress);
        xhr.upload.addEventListener('error', onFailure);
        xhr.upload.addEventListener('abort', onFailure);
        xhr.onreadystatechange = (ev: Event) => {
            const { readyState, status } = xhr;
            if (readyState === 4) {
                if (status === 200) {
                    emitter({ success: true });
                    emitter(END);
                }
                else {
                    emitter({error: new Error('Upload failed')});
                    emitter(END);
                }
            }
        };
        console.log(data['fa']);
        xhr.open('POST', endpoint, true);
        xhr.send(data);

        return () => {
            xhr.upload.removeEventListener("progress", onProgress);
            xhr.upload.removeEventListener("error", onFailure);
            xhr.upload.removeEventListener("abort", onFailure);
            xhr.onreadystatechange = null;
            xhr.abort();
        }
    }, buffers.sliding(2));
}