import { routerMiddleware, routerReducer } from "react-router-redux";
import { createStore, applyMiddleware, Store, combineReducers } from "redux";
import { composeWithDevTools } from 'redux-devtools-extension';
import { CounterState, InitialCounterState, counterReducer } from "./Counter";
import { TaskUploadState, InitialUploadState } from "./Upload";
import { uploadReducer } from "./Upload";
import createSagaMiddleware, { Channel } from 'redux-saga';
import { uploadSaga } from "./Upload";
import { InitialTasksState, taskFetchReducer, TasksState, taskFetchSaga } from "./Tasks";
import { all, fork } from "redux-saga/effects";
import { InitialLobbiesState, lobbiesFetchReducer, LobbiesState } from "./Lobbies";
import { lobbiesSaga } from "./Lobbies";
import { LobbyState, InitialLobbyState, lobbyReducer, lobbySagas } from "./Lobby";
import { ServerConnectionState, InitialServerConnectionState } from "src/store/ServerConnection";
import { serverConnectionReducer } from "./ServerConnection";
import { WebRtcState, webRtcReducer, InitialWebRtcState } from "./PeerConnection";
import { ExecutionState, InitialExecutionState } from "./Execution";
import { executionReducer } from "./Execution/reducer";
import { ServerConnectionDynamicContext, serverConnectionSaga, InitialServerConnectionDynamicContext } from "./ServerConnection/saga";

export interface AppState {
    counter: CounterState,
    upload: TaskUploadState,
    tasks: TasksState,
	lobbies: LobbiesState,
	lobby: LobbyState,
	serverConnection: ServerConnectionState,
	webRtc: WebRtcState,
	execution: ExecutionState
}

export interface AppDynamicContext {
	ServerConnectionDC: ServerConnectionDynamicContext
}

const appReducers = {
    counter: counterReducer,
    upload: uploadReducer,
    tasks: taskFetchReducer,
	lobbies: lobbiesFetchReducer,
	lobby: lobbyReducer,
	serverConnection: serverConnectionReducer,
	webRtc: webRtcReducer,
	execution: executionReducer
}

const sagaMiddleware = createSagaMiddleware();

const appDynamicContext: AppDynamicContext = { ServerConnectionDC: InitialServerConnectionDynamicContext }

function* rootSaga() {
    yield all([
        fork(uploadSaga),
        fork(taskFetchSaga),
		fork(lobbiesSaga),
		fork(lobbySagas, appDynamicContext),
		fork(serverConnectionSaga, appDynamicContext)
    ])
}

export function configureStore (
        history: any, 
        initialState : AppState = {
            counter: InitialCounterState,
            upload: InitialUploadState,
            tasks: InitialTasksState,
			lobbies: InitialLobbiesState,
			lobby: InitialLobbyState,
			serverConnection: InitialServerConnectionState,
			webRtc: InitialWebRtcState,
			execution: InitialExecutionState
        }
): Store<{}> {
    const reduxRouterMiddleware = routerMiddleware(history);

    const createStoreWithMiddleware = composeWithDevTools(
        applyMiddleware(reduxRouterMiddleware, sagaMiddleware)
    )(createStore);
    
    const allReducers = Object.assign({}, appReducers, { routing: routerReducer })

    return createStoreWithMiddleware(combineReducers(allReducers), initialState);
}

export function runSagas() {
    sagaMiddleware.run(rootSaga);
}