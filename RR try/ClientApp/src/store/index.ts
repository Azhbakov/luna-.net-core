import * as Redux from 'redux';

export const serverUrl = window.location.protocol + '//' + window.location.hostname + ":" + window.location.port + '/';

export interface DispatchConnectedProps {
    dispatch: Redux.Dispatch;
}

export const mapDispatchToProps = (dispatch: Redux.Dispatch) => ({
    dispatch
});