export type TCallback<T> = (arg: T) => void;

// export type CallbackId = string; // TODO id for unregister

export class CallbackControl<T> implements CallbackStore<T> {
	private _callbacks: TCallback<T>[] = [];

	public addCallback(callback: TCallback<T>) {
		this._callbacks.push(callback);
	}

	public fire(arg: T) {
		this._callbacks.forEach(cb => cb(arg));
	}
}

export interface CallbackStore<T> {
	addCallback: (callback: TCallback<T>) => void;
}