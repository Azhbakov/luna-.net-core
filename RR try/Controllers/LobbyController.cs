﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Microsoft.AspNetCore.Http;
using LuNA1.Services;
using static LuNA1.Services.LobbyService;
using LuNA1.Domain;
using LuNA1.Domain.LineTopology;

namespace LuNA1.Controllers
{
    [Route("api/lobbies")]
    public class LobbyController : Controller
	{ 
        private readonly LobbyService _lobbyService;
        private readonly TaskStorage _taskStorage;

        public LobbyController(LobbyService lobbyService, TaskStorage taskStorage) 
        {
            _lobbyService = lobbyService ?? throw new ArgumentNullException(nameof(lobbyService));
            _taskStorage = taskStorage ?? throw new ArgumentNullException(nameof(taskStorage));
        }

		[HttpPost]
        public IActionResult CreateLobby([FromBody]LobbyCreateVM lobbyData)
        {
            try 
            {
                var task = _taskStorage.GetTask(lobbyData.TaskId);
                var lobby = new Lobby(Guid.NewGuid().ToString(), lobbyData.LobbyName, task, new LineTopology()) {
                    MaxUsersCount = lobbyData.MaxUsersCount
                };
                
                _lobbyService.AddLobby(lobby);
    			
                return Ok(new { id = lobby.Id });
            }
            catch (TaskStorageException ex) { return BadRequest(ex.Message); }
            catch (LobbyServiceException ex) { return BadRequest(ex.Message); }
        }

        [HttpGet]
        public IActionResult GetLobbies()
        {
            try 
            {
                var answer = _lobbyService.GetLobbies().Select(l => new LobbyDisplayVM() {
                    Id = l.Id,
                    LobbyName = l.Name,
                    TaskId = l.Task.Id,
                    UserCount = l.UserCount,
                    MaxUsersCount = l.MaxUsersCount
                });
    			
                return Json(answer);
            }
            catch (LobbyServiceException ex) { return StatusCode(500, ex.Message); }
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult DeleteLobby(string id)
        {
            try 
            {   
                _lobbyService.RemoveLobby(id);
                return Ok();
            }
            catch (LobbyServiceException ex) { return NotFound(ex.Message); }
        }
    }

    public class LobbyCreateVM 
    {
        public string TaskId { get; set; }
        public string LobbyName { get; set; }
        public int? MaxUsersCount { get; set; }
    }

    public class LobbyDisplayVM
    {
        public string Id { get; set; }
        public string LobbyName { get; set; }
        public string TaskId { get; set; }
        public int UserCount { get; set; }
        public int? MaxUsersCount { get; set; }
    }

}