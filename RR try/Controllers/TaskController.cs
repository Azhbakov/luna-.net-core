using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LuNA1.Domain;
using LuNA1.Services;
using LuNA1.Storage;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;

namespace LuNA1.Controllers
{
    [Route("api/tasks")]
    public class TaskController : Controller
    {
        private readonly TaskStorage _taskStorage;

        public TaskController(TaskStorage taskStorage) 
        {
            _taskStorage = taskStorage;
        }

        [HttpPost]
        public async Task<IActionResult> Upload(string taskName, IFormFile fa, IFormFile atoms)
        {
            try 
            {
                await _taskStorage.AddTask(taskName,
                    atoms.FileName, atoms.OpenReadStream(),
                    fa.FileName, fa.OpenReadStream());
                return Ok();
            }
            catch (TaskStorageException ex) { return BadRequest(ex.Message); }
        }

		[HttpGet("{id}/{obj}")]
        public IActionResult GetContent(string id, string obj)
        {
			try {
				Stream res = null;
				switch (obj) {
					case "atoms": 
						res = _taskStorage.GetAtoms(id);
						break;
					case "program":
						res = _taskStorage.GetProgram(id);
						break;
					default: return NotFound();
				}
				
				return new FileStreamResult(res, new MediaTypeHeaderValue("text/plain"));
			} 
			catch (TaskStorageException ex) {
				return BadRequest(ex.Message);
			}
			catch (FileStorageException ex) {
				return BadRequest(ex.Message);
			}
        }

        [HttpGet]
        public JsonResult GetTasks()
        {
            return Json(_taskStorage.GetTaskList());
        }

        [HttpDelete]
        [Route("{taskId}")]
        public async Task<IActionResult> RemoveTask(string taskId)
		{
			if (string.IsNullOrEmpty(taskId)) throw new ArgumentException(nameof(taskId));

            if (!(await _taskStorage.RemoveTask(taskId))) return NotFound();

			return Ok();
		}
    }
}