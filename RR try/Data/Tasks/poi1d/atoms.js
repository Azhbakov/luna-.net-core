var atoms = {}

atoms.N = 40;
atoms.NY = 40;
atoms.NZ = 40;
atoms.X = 2.0;
atoms.Y = 2.0;
atoms.Z = 2.0;
atoms.SIZE = 10;
atoms.a = 1;
atoms.EPS = 0.1;

atoms.hx = atoms.X/atoms.N;
atoms.hy = atoms.Y/atoms.NY;
atoms.hz = atoms.Z/atoms.NZ;

atoms.owx = atoms.hx*atoms.hx;
atoms.owy = atoms.hy*atoms.hy;
atoms.owz = atoms.hz*atoms.hz;
atoms.c = 2./atoms.owx + 2./atoms.owy + 2./atoms.owz + atoms.a;


/*intArg, nameArg*/
atoms.c_init = (intArg, df) => { // nameArg aka returnPlace {externPrefix, externName, externIndices}
	df.value = intArg;
}

atoms.c_show = (arg1, arg2) => { // nameArg aka returnPlace {externPrefix, externName, externIndices}
	console.log ("C_SHOW: " + arg1 + " " + arg2);
}

// int nx, int ny, int nz, int num_of_fragments, int max_iter
atoms.c_print_info = (nx, ny, nz, num_of_fragments, max_iter) => {
	// var nx = args[0];
	// var ny = args[1];
	// var nz = args[2];
	// var num_of_fragments = args[3];
	// var max_iter = args[4];

	var output = "Size: " + nx + " " + ny + " " + nz + "\n";
	output += "Fragments: " + num_of_fragments + "\n";
	output += "Iterations: " + max_iter;

	console.log ("C_PRINT_INFO: " + output);
}


// int id, InputDF &df1, InputDF &df2, InputDF &df3, OutputDF &df4, int L 
atoms.c_calc_maxdiff = (id, df1, df2, df3, df4, L) => {
	// ARGS
	// var id = args[0];
	// var df1 = args[1];
	// var df2 = args[2];
	// var df3 = args[3];
	// var df4 = args[4];
	// var L = args[5];

    const jn = atoms.NY;
    const kn = atoms.NZ;

    const dft = df1;//df1.getData<int>();
    const sft = df2;//df2.getData<int>();
    const F = df3;//df3.getData<double>();
            
    const dt = atoms.calc_div(id, atoms.SIZE, sft);

    var mx = 0.0;
    for (var i = 1; i < dft[id] - 1; i++) {
        for (var j = 1; j < jn - 1; j++) {
            for (var k = 1; k < kn-1; k++) {
            	var defF = F[(dft[id]*jn*kn) * L + i*jn*kn + j*kn + k];
                const F1 = /*atoms.fabs*/Math.abs(defF - atoms.Fresh((i + dt)*atoms.hx, j*atoms.hy, k*atoms.hz));
                if (F1 > mx) {
                    mx = F1;
                }
    	    }
        }
    }

    df4.value = mx;
}


// int n, int size, OutputDF &df3, OutputDF &df4, OutputDF &df5
atoms.c_init_distribution = (n, size, df3, df4, df5) => {  
	// ARGS
    // var n = args[0];
    // var size = args[1];
    // var df3 = args[2];
    // var df4 = args[3];
    // var df5 = args[4];

    df3.value = [];//df3.create<int>(size);
    
    for (var i = 0; i < size; i++) {
        df3.value[i] = Math.floor(n/size) + (i < n % size);
	}
        
    df4.value = [];//df4.create<int>(size + 1);

    df4.value[0] = 0;
    for (var i = 1; i <= size; i++) {
        df4.value[i] = df4.value[i - 1] + df3.value[i - 1];
    }
        
    df5.value = [];//df5.create<int>(size);
    
    df5.value[0] = df3.value[0] + 1;
    df5.value[size - 1] = df3.value[size - 1] + 1;
    for (var i = 1; i < size - 1; i++)
        df5.value[i] = df3.value[i] + 2;

    if (size == 1) {
        df5.value[0]--;
    }
}


// double *F, int in0, int in1, int fi0, int fi1, int ki, int dt, int jn, int kn
atoms.inic = (F, in0, in1, fi0, fi1, ki, dt, jn, kn) => {	
    // ARGS
    // var F = args[0];
    // var in0 = args[1];
    // var in1 = args[2];
    // var fi0 = args[3];
    // var fi1 = args[4];
    // var ki = args[5];
    // var dt = args[6];
    // var jn = args[7];
    // var kn = args[8];

    for (var i = in0; i < in1; i++) {
        for (var j = 0; j < jn; j++) {
            for (var k = 0; k < kn; k++) {
                if((i != fi0) && (j != 0) && (k != 0) && (k != kn - 1) && (i != fi1) && (j != jn - 1)) {
                    F[i*jn*kn + j*kn + k] = 0.0;
                    F[ki*jn*kn + i*jn*kn + j*kn + k] = 0.0;
                } else {
                    F[i*jn*kn + j*kn + k] = atoms.Fresh((i + dt)*atoms.hx, j*atoms.hy, k*atoms.hz);
                    F[ki*jn*kn + i*jn*kn + j*kn + k] = atoms.Fresh((i + dt)*atoms.hx, j*atoms.hy, k*atoms.hz);
                }
            }
        }
    }
}


// int id, int n, InputDF& df1, InputDF& df2
atoms.c_print_F = (id, n, df1, df2) => {
	// ARGS
	// var id = args[0];
	// var n = args[1];
	// var df1 = args[2];
	// var df2 = args[3];

	
    var totaldim = df1;//df1.getData<int>();
    var df2_data = df2;
    
    var output = "F" + id + "\n";
    
    for(var i =0; i < totaldim[id]; i++) {
		for(var j = 0; j < atoms.NY; j++) {
		    for(var k = 0; k < atoms.NZ; k++) {
				output += df2_data[i*atoms.NY*atoms.NZ+atoms.NZ*j+k] + " ";
		    }
		    output += "\n";
		}
		output += "\n";
	}
	
	output += "-------------\n";
	output += "-------------\n";

    for(var i = 0; i < totaldim[id]; i++) {
		for(var j = 0; j < atoms.NY; j++) {
		    for(var k = 0; k < atoms.NZ; k++) {
				output += df2_data[ (atoms.NY*atoms.NZ*totaldim[id])*1+i*atoms.NZ*atoms.NY+atoms.NZ*j+k] + " ";
		    }
			output += "\n";
		}
		output += "\n";
    }
 
	console.log ("C_PRINT_F: " + output);
}


// int id, int nx, int ny, InputDF &df1
atoms.c_print_border = (id, nx, ny, df1) => {
    // ARGS
    // var id = args[0];
    // var nx = args[1];
    // var ny = args[2];
    // var df1  = args[3];

    var output = "br" + id + "\n";

    if(df1.length != 0) {
        var df1_data = df1;
        for (var j = 1; j < nx - 1; j++) {
            for (var k = 1; k < ny - 1; k++)
                output += df1_data[ny*j + k] + "\n";
            output += "\n";
        }
    } else {
        output += "Buffer is empty\n";
    }

	console.log ("C_PRINT_BORDER: " + output);
}

// int id, int n, int size, InputDF &df_sft, InputDF &df_dim, OutputDF &df_f
atoms.c_init_F = (id, n, size, df_sft, df_dim, df_f) => {
    // ARGS
    // var id = args[0];
    // var n = args[1];
    // var size = args[2];
    // var df_sft = args[3];
    // var df_dim = args[4];
    // var df_f = args[5];

    var sft = df_sft;//.getData<int>();
    var dim = df_dim;//.getData<int>();     
    
    var dt = atoms.calc_div(id, size, sft);
    
    df_f.value = [];
    for (var q = 0; q < 2*atoms.NY*atoms.NZ*dim[id]; q++) {
        df_f.value[q] = 0;
    }
    var F = df_f.value;//df_f.create<double>(2*NY*NZ*dim[id]);
    
    if(id == 0) {
        atoms.inic(F, 0, dim[id], 0, -1, dim[id], dt, atoms.NY, atoms.NZ);
    } else {
        if(id == size - 1)
            atoms.inic(F, 0, dim[id], -1, dim[id]-1, dim[id], dt, atoms.NY, atoms.NZ);
        else
            atoms.inic(F, 0, dim[id], -1, -1, dim[id], dt, atoms.NY, atoms.NZ);
    }
    
}

// OutputDF &df, double val
atoms.c_set_real = (df, val) => {
    df.value = val;
}

// OutputDF &df, int val
atoms.c_set_int = (df, val) => {
    df.value = val;
}

// InputDF &df
atoms.c_print = (df) => {
    var output = /*args[0].globalName + " is " + */df;

	console.log ("C_PRINT: " + output);
}

// int val
atoms.c_print_int = (output) => {
    // var output = args[0];

	console.log ("C_PRINT_INT: " + output);
}

// double val
atoms.c_print_real = (output) => {
    //var output = args[0];

	console.log ("C_PRINT_REAL: " + output);
}

// InputDF &df)
atoms.c_print_as_real = (output) => {
    //var output = args[0];

	console.log ("C_PRINT_AS_REAL: " + output);
}

// double x, double y, OutputDF &df
atoms.c_rmax = (x, y, df) => {
	// ARGS
	// var x = args[0];
	// var y = args[1];
	// var df = args[2];
    var toPass = x > y ? x : y;
    atoms.c_set_real(df, toPass);
}

// const double *up, double *F, int dim, int L
atoms.change_up = (up, F, dim, L) => {
	// ARGS
	// var up = args[0];
	// var F = args[1];
	// var dim = args[2];
	// var L = args[3];

	
    var i = 0;
    for (var j = 0; j < atoms.NY; j++)
        for (var k = 0; k < atoms.NZ; k++)
            F[(dim*atoms.NZ*atoms.NY)*L + i*atoms.NY*atoms.NZ + j*atoms.NZ + k] = up[j*atoms.NZ + k];
}

// const double *down, double *F, int dim, int L
atoms.change_down = (down, F, dim, L) => {
	// ARGS
	// var down = args[0];
	// var F = args[1];
	// var dim = args[2];
	// var L = args[3];

	
    var i = dim - 1;
    for (var j = 0; j < atoms.NY; j++)
        for (var k = 0; k < atoms.NZ; k++)
            F[(dim*atoms.NY*atoms.NZ)*L + i*atoms.NY*atoms.NZ + j*atoms.NZ + k] = down[j*atoms.NZ + k];
}

/*
int id,
luna::ucenv::InputDF &df_dim,		//dim
luna::ucenv::InputDF &df_border1,		//border1
luna::ucenv::InputDF &df_border2,		//border2
luna::ucenv::InputDF &df_f,		//F
luna::ucenv::OutputDF &df_ff, 		//FF
int L
*/
atoms.c_change_border = (id, df_dim, df_border1, df_border2, df_f, df_ff, L) => {
	// ARGS
	// var id = args[0];
	// var df_dim = args[1];
	// var df_border1 = args[2];
	// var df_border2 = args[3];
	// var df_f = args[4];
	// var df_ff = args[5];
    // var L = args[6];

    var dim = df_dim;//.getData<int>();
    var border1 = df_border1;//.getData<double>();
    var border2 = df_border2;//.getData<double>();
    
    //df_ff.copy(df_f);
    df_ff.value = [];
   	for (var i = 0; i < df_f.length; i++) {
   		df_ff.value[i] = df_f[i];
   	}

    var F = df_ff.value;//getData<double>();
        
    if(border2 != null)
        atoms.change_down(border2, F, dim[id], L);
    
    if(border1!= null)
        atoms.change_up(border1, F, dim[id], L);
}

/*
int id,
luna::ucenv::InputDF &df_sft,		//sft
luna::ucenv::InputDF &df_dim,		//dim
luna::ucenv::InputDF &df3,		//F[it]
luna::ucenv::OutputDF &df4,		//F[it+1]
luna::ucenv::OutputDF &df_down,		//down[it+1][i-1]
luna::ucenv::OutputDF &df_up,		//up[it+1][i+1]
luna::ucenv::OutputDF &df_max, 	//max
int iter
*/
atoms.c_ps = (id, df_sft, df_dim, df3, df4, df_down, df_up, df_max, iter) => {
	// ARGS
	// var id = args[0];
	// var df_sft = args[1];		//sft
	// var df_dim = args[2];		//dim
	// var df3 = args[3];		//F[it]
	// var df4 = args[4];		//F[it+1]
	// var df_down = args[5];		//down[it+1][i-1]
	// var df_up = args[6];		//up[it+1][i+1]
	// var df_max = args[7]; 	//max
	// var iter = args[8];

	
    const jn = atoms.NY;
    const kn = atoms.NZ;

    var sft = df_sft;//.getData<int>();
    var dim = df_dim;//.getData<int>();
        
    var dt = atoms.calc_div(id, atoms.SIZE, sft);
            
    const L0 = iter % 2;
    const L1 = 1 - L0;

    //df4.copy(df3);
    df4.value = [];
   	for (var i = 0; i < df3.length; i++) {
   		df4.value[i] = df3[i];
   	}

    var F = df4.value;//.getData<double>();

    //printf("%s %s %s %s\n", df_sft.getCName(), df_dim.getCName(), df3.getCName(), df4.getCName());

//#define F(l,i,j,k) F[(dim[id]*jn*kn)*l+i*jn*kn+j*kn+k]

    var mx = 0.0;
    for (var i = 1; i <= dim[id] - 2; i++) {
        for (var j = 1; j < jn - 1; j++) {
            for (var k = 1; k < kn - 1; k++) {

            	var deff1 = F[(dim[id]*jn*kn)*L0 + (i+1)*jn*kn + j*kn + k];
            	var deff2 = F[(dim[id]*jn*kn)*L0 + (i-1)*jn*kn + j*kn + k];
                const Fi = (deff1 + deff2) / atoms.owx;//(F(L0, (i+1), j, k) + F(L0, (i-1), j, k)) / owx;
                
                deff1 = F[(dim[id]*jn*kn)*L0 + i*jn*kn + (j+1)*kn + k];
            	deff2 = F[(dim[id]*jn*kn)*L0 + i*jn*kn + (j-1)*kn + k];
            	const Fj = (deff1 + deff2) / atoms.owy; //(F(L0, i, (j+1), k) + F(L0, i, (j-1), k)) / owy;
                
                deff1 = F[(dim[id]*jn*kn)*L0 + i*jn*kn + j*kn + (k+1)];
            	deff2 = F[(dim[id]*jn*kn)*L0 + i*jn*kn + j*kn + (k-1)];
                const Fk = (deff1 + deff2) / atoms.owz; //(F(L0, i, j, (k+1)) + F(L0, i, j, (k-1))) / owz;
	    	     
                const Ra = atoms.Ro((i + dt)*atoms.hx, j*atoms.hy, k*atoms.hz);
         
                //F(L1, i, j, k) = (Fi + Fj + Fk - Ra) / c;
                F[(dim[id]*jn*kn)*L1 + i*jn*kn + j*kn+k] = (Fi + Fj + Fk - Ra) / atoms.c;

                deff1 = F[(dim[id]*jn*kn)*L1 + i*jn*kn + j*kn + k];
            	deff2 = F[(dim[id]*jn*kn)*L0 + i*jn*kn + j*kn + k];
                const Fa = Math.abs(deff1-deff2);//fabs(F(L1, i, j, k) - F(L0, i, j, k));
                if(Fa > mx) {
                    mx = Fa;
                }
            }
        }
    }
    df_max.value = mx;

    //df4 = df3;
            
    if(id != 0) {
        df_down.value = [];//.create(jn*kn*sizeof(double));
        var up = df_down.value;//.getData<double>();
        var i = 1;
        for (var j = 0; j < jn; j++)
            for (var k = 0; k < kn; k++) {
                up[j*kn + k] = F[(dim[id]*jn*kn)*L1 + i*jn*kn + j*kn + k];//F(L1, i, j, k);
            }
    }
    else {
		df_down.value = null;//.create(0);
        /*for (var q = 0; q < jn*kn; q++) {
            df_down.value[q] = 0;
        }*/
    }
            
    if(id != atoms.SIZE-1) {
        df_up.value = [];//.create(jn*kn*sizeof(double));
        var down = df_up.value;//.getData<double>();
        var i = dim[id]-2;
        for (var j = 0; j < jn; j++)
            for (var k = 0; k < kn; k++) {
                down[j*kn + k] = F[(dim[id]*jn*kn)*L1 + i*jn*kn + j*kn + k];//F(L1, i, j, k);
            }
    }
    else {
    	df_up.value = null;//.create(0);
        // for (var q = 0; q < jn*kn; q++) {
        //     df_up.value[q] = 0;
        // }
    }
}

// double x, double y, double z
atoms.Ro = (x, y, z) => {
    return -atoms.a*(x + y + z);
}

// double x, double y, double z
atoms.Fresh = (x, y, z) => {
    return x + y + z;
}

// int r, int m, const int *sft
atoms.calc_div = (r, m, sft) => {
	// ARGS
	// var r = args[0];
	// var m = args[1];
	// var sft = args[2];
    return (m != 1) ? sft[r] - (r != 0) : 0;
}

({
	...atoms
})