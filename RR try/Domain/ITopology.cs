using System;
using System.Collections.Generic;
using System.Linq;
using LuNA1.Domain;
using Newtonsoft.Json;

namespace LuNA1.Domain 
{
	public enum MailToType { Sender, All, Others, List };

	public class MessageTarget 
	{
		public MailToType MailTo { get; private set; }
		public string Sender { get; private set;  }
		public IEnumerable<string> MailToList { get; private set; }

		private MessageTarget () {}

		public static MessageTarget CreateAnswerToSender(string sender) // TODO get rid of sender
		{
			if (string.IsNullOrEmpty(sender)) throw new ArgumentException(nameof(sender));
			return new MessageTarget()
			{
				MailTo = MailToType.Sender,
				Sender = sender
			};
		}

		public static MessageTarget CreateAnswerToAll()
		{
			return new MessageTarget()
			{
				MailTo = MailToType.All
			};
		}

		public static MessageTarget CreateAnswerToOthers(string sender)
		{
			if (string.IsNullOrEmpty(sender)) throw new ArgumentException(nameof(sender));
			return new MessageTarget()
			{
				MailTo = MailToType.Others,
				Sender = sender
			};
		}

		public static MessageTarget CreateAnswerToList(string sender, IEnumerable<string> mailToList)
		{
			if (string.IsNullOrEmpty(sender)) throw new ArgumentException(nameof(sender));
			if (mailToList == null) throw new ArgumentException(nameof(mailToList));
			return new MessageTarget()
			{
				MailTo = MailToType.Others,
				MailToList = mailToList
			};
		}

		public static MessageTarget CreateAnswer(string sender, string target)
		{
			if (string.IsNullOrEmpty(sender)) throw new ArgumentException(nameof(sender));
			if (string.IsNullOrEmpty(target)) throw new ArgumentException(nameof(target));
			return new MessageTarget()
			{
				MailTo = MailToType.Others,
				MailToList = new []{ target }
			};
		}
	}

	public class PeerAddRequest
	{
		public string SenderId { get; set; }
		public string Name { get; set; }
		public dynamic CustomData { get; set; }
	}

	public interface IPeer 
	{
		string Id { get; }
		string Name { get; }
		// IDictionary<string, string> Neighbors { get; }
	}

	public interface IEdge 
	{
		string FromId { get; }
		string FromKey { get; }

		string ToId { get; }
		string ToKey { get; }
	}

	public class Edge : IEdge
	{
		public string FromId { get; set; }
		public string FromKey { get; set; }

		public string ToId { get; set; }
		public string ToKey { get; set; }
	}

	public interface ITopology
	{
		string Key { get; }
		IEnumerable<IPeer> Peers { get; }
		IEnumerable<IEdge> Edges { get; }
		void AddPeer(PeerAddRequest peerAddRequest);
		void RemovePeer(string removedPeerId);

		string Serialize();
	}
}