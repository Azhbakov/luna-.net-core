using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace LuNA1.Domain.LineTopology
{
	[DataContract]
	public class LinePeer : IPeer
	{
		[DataMember]
		public string Id { get; }
		[DataMember]
		public string Name { get; }

		public int GlobalPosition { get; set; }

		public LinePeer Left { get; set; }
		public LinePeer Right { get; set; }

		// [DataMember]
		// public IDictionary<string, string> Neighbors => new Dictionary<string, string> { { "left", Left?.Id }, { "right", Right?.Id }};

		public LinePeer(string id, string name)
		{
			if (string.IsNullOrEmpty(id)) throw new ArgumentException(nameof(id));
			Id = id;

			if (string.IsNullOrEmpty(name)) throw new ArgumentException(nameof(name));
			Name = name;
		}
	}

	public class LineTopology : ITopology
	{
		public string Key => "LineTopology";

		private List<LinePeer> _peers = new List<LinePeer>();
		public IEnumerable<IPeer> Peers => _peers;

		public IEnumerable<IEdge> Edges => _peers.Where(p => p.Right != null).Select(p => new Edge() 
		{
			FromId = p.Id, FromKey = "right", ToId = p.Right.Id, ToKey = "left"
		});

		public void AddPeer(PeerAddRequest peerAddRequest)
		{
			var lastPeer = _peers.LastOrDefault();

			var newPeer = new LinePeer(peerAddRequest.SenderId, peerAddRequest.Name);			
			_peers.Add(newPeer);
			
			if (lastPeer != null) 
			{
				lastPeer.Right = newPeer;
				newPeer.Left = lastPeer;
			}
		}

		public void RemovePeer(string removedPeerId)
		{
			if (string.IsNullOrEmpty(removedPeerId)) throw new ArgumentException(nameof(removedPeerId));

			var removedPeer = _peers.SingleOrDefault(p => p.Id.Equals(removedPeerId));
			if (removedPeer == null) throw new InvalidOperationException("Peer not found.");

			if (!_peers.Remove(removedPeer)) throw new InvalidOperationException("Failed to remove peer from collection.");
			if (removedPeer.Left != null)
			{
				if (removedPeer.Right != null) 
				{
					removedPeer.Left.Right = removedPeer.Right;
					removedPeer.Right.Left = removedPeer.Left;
				} 
				else
				{
					removedPeer.Left.Right = null;
				} 
			}
			else if (removedPeer.Right != null) 
			{
				removedPeer.Right.Left = null;
			}
		}

		public string Serialize()
		{
			throw new NotImplementedException();
			//return JsonConvert.Serialize(new {})
		}
	}
}