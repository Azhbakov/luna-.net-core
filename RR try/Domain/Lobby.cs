﻿using LuNA1.Domain;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LuNA1.Domain
{
    public class Lobby // TODO lock
    {
		public string Id { get; }
		public int ChangeIndex { get; private set; } = 0;
		public string Name { get; }
		public int? MaxUsersCount { get; set; } = null;
		public int UserCount => Topology.Peers.Count();
		public string LeaderId => Topology.Peers.FirstOrDefault()?.Id;
		public LunaTaskDetails Task { get; private set; }
		public ITopology Topology { get; private set; }

		public enum LobbyStatus { NotReady, Ready, Running, Finished }
		public LobbyStatus Status { get; set; }
		public Dictionary<string, bool> PeerReadyState { get; } = new Dictionary<string, bool>();

		public Lobby(string id, string name, LunaTaskDetails task, ITopology topology)
		{
			if (string.IsNullOrEmpty(id)) throw new ArgumentException(nameof(id));
			if (string.IsNullOrEmpty(name)) throw new ArgumentException(nameof(id));

			Id = id;
			Name = name;
			Task = task ?? throw new ArgumentNullException(nameof(task));
			Topology = topology ?? throw new ArgumentNullException(nameof(topology));
		}

		private void LobbyChanged() 
		{
			ChangeIndex++;
			foreach (var k in PeerReadyState.Keys.ToList()) 
			{
				PeerReadyState[k] = false;
			}
			Status = LobbyStatus.NotReady;
		}

		public void AddPeer(string peerId, string peerName) 
		{
			LobbyChanged();
			Topology.AddPeer(new PeerAddRequest() {
				SenderId = peerId,
				Name = peerName
			});
			PeerReadyState[peerId] = false;
		}

		public void RemovePeer(string removePeerId) 
		{
			LobbyChanged();
			Topology.RemovePeer(removePeerId);
			PeerReadyState.Remove(removePeerId);
		}

		public bool TrySetPeerReady(string peerId, int changeIndex, bool isReady) 
		{
			if (changeIndex != ChangeIndex) return false;
			PeerReadyState[peerId] = isReady;
			Status = PeerReadyState.Values.Any(status => status == false) ? LobbyStatus.NotReady : LobbyStatus.Ready;
			return true;
		}

		public bool TryStart() 
		{
			if (Status != LobbyStatus.Ready) return false;
			Status = LobbyStatus.Running;
			return true;
		}
	}


	public class LobbyException : Exception
	{
		public LobbyException() { }
		public LobbyException(string message) : base(message) { }
		public LobbyException(string message, Exception inner) : base(message, inner) { }
	}
}
