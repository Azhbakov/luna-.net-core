﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace LuNA1.Domain
{
    public class LunaTaskDetails
    {
		public string Id { get; set; }
		public string Name { get; set; }
		public string AtomsName { get; set; }
		public string ProgramName { get; set; }
	}
}
