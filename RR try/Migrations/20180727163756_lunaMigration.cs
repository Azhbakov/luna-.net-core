﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LuNA1.Migrations
{
    public partial class lunaMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TaskStorage",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    TaskName = table.Column<string>(nullable: true),
                    AtomsFilename = table.Column<string>(nullable: true),
                    ProgramFilename = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskStorage", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TaskStorage");
        }
    }
}
