﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace LuNA1.Services
{
    public class FileStorageService
    {
		ILogger _logger;

		public readonly string _tasksDirectory = Path.Combine(Directory.GetCurrentDirectory(), "Data", "Tasks");

		public async Task StoreFile (string filepath, Stream content)
		{
			if (string.IsNullOrEmpty(filepath)) throw new ArgumentException(nameof(filepath));
			if (content == null) throw new ArgumentException(nameof(content));

			if (File.Exists(StoragePathCombine(filepath))) throw new FileStorageException($"File {filepath} already exists.");
			var targetFolder = Path.Combine(_tasksDirectory, Path.GetDirectoryName(filepath));
			if (!Directory.Exists(targetFolder)) Directory.CreateDirectory(targetFolder);
			
			using (var stream = new FileStream(StoragePathCombine(filepath), FileMode.Create))
			{
				await content.CopyToAsync(stream);
			}

			return;
		}

		// public IEnumerable<FileInfo> GetFiles ()
		// {
		// 	if (!Directory.Exists(_tasksDirectory)) return Array.Empty<FileInfo>();

		// 	return Directory.GetFiles(_tasksDirectory).Select(filename => new FileInfo(StoragePathCombine(filename)));
		// }

		public Stream GetFile(string filepath)
		{
			if (!File.Exists(StoragePathCombine(filepath))) throw new FileStorageException($"File {filepath} not found.");

			return File.OpenRead(StoragePathCombine(filepath));
		}

		public bool RemoveFile(string filePath)
		{
			if (string.IsNullOrEmpty(filePath)) return false;
			if (!File.Exists(StoragePathCombine(filePath))) return false;
			File.Delete(StoragePathCombine(filePath));

			// Remove folder if empty
			var folder = Path.GetDirectoryName(filePath);
			var targetFolder = Path.Combine(_tasksDirectory, folder);
			if (!string.IsNullOrEmpty(folder) && !Directory.GetFiles(targetFolder).Any()) 
			{
				Directory.Delete(targetFolder);
			}
			return true;
		}

		private string StoragePathCombine (string filepath)
		{
			if (string.IsNullOrEmpty(filepath)) throw new ArgumentException(nameof(filepath));
			return Path.Combine(_tasksDirectory, filepath);
		}
	}

	public class FileStorageException : Exception
	{
		public FileStorageException() { }
		public FileStorageException(string message) : base(message) { }
		public FileStorageException(string message, Exception inner) : base(message, inner) { }
	}
}
