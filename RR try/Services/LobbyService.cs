﻿using LuNA1.Domain;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LuNA1.Services
{
    public class LobbyService
    {
		private List<Lobby> _lobbies = new List<Lobby>();

		public ConcurrentDictionary<string, Lobby> PeersAndLobbies { get; } = new ConcurrentDictionary<string, Lobby>();

		public LobbyService () {}

		public void AddLobby(Lobby lobby)
		{
			if (lobby == null) throw new ArgumentNullException(nameof(lobby));
			if (_lobbies.Any(l => l.Id.Equals(lobby.Id))) throw new LobbyServiceException("Lobby with such ID already exists!"); 
			_lobbies.Add(lobby);
		}

		public IEnumerable<Lobby> GetLobbies()
		{
			return _lobbies;
		}

		public Lobby GetLobby(string id)
		{
			return _lobbies.Single(l => l.Id.Equals(id));
		}

		public void RemoveLobby(string id)
		{
			if (string.IsNullOrEmpty(id)) throw new ArgumentNullException(nameof(id));
			var lobby = _lobbies.SingleOrDefault(l => l.Id.Equals(id));
			if (lobby == null) throw new LobbyServiceException("Lobby doesn't exist.");
			_lobbies.Remove(lobby);
		}

		public class LobbyServiceException : System.Exception
		{
			public LobbyServiceException() { }
			public LobbyServiceException(string message) : base(message) { }
			public LobbyServiceException(string message, System.Exception inner) : base(message, inner) { }
		}
	}
}
