﻿using LuNA1.Storage;
using LuNA1.Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace LuNA1.Services
{
	public class TaskStorage : IEnumerable<LunaTaskDetails>
    {
		private readonly LunaDbContext _context;
		private readonly FileStorageService _fileStorage;
		private readonly ILogger _logger;

		public TaskStorage (LunaDbContext context, FileStorageService fileStorage, ILogger<TaskStorage> logger)
		{ 
			_context = context;
			_fileStorage = fileStorage;
			_logger = logger;
		}

		public async Task AddTask (string taskName, 
			string atomsFilename, Stream atomsContent,
			string programFilename, Stream programContent)
		{
			if (_context.TaskStorage.Any(t => t.TaskName.Equals(taskName))) throw new TaskStorageException("Task with such name already exists.");
			try
			{
				await _fileStorage.StoreFile(Path.Combine(taskName, atomsFilename), atomsContent);
				await _fileStorage.StoreFile(Path.Combine(taskName, programFilename), programContent);
			} 
			catch (FileStorageException ex)
			{
				throw new TaskStorageException($"Failed to save task files: {ex.Message}");
			}

			_context.TaskStorage.Add(new TaskStorageEntry()
			{
				Id = Guid.NewGuid().ToString(),
				TaskName = taskName,
				AtomsFilename = atomsFilename,
				ProgramFilename = programFilename
			});

			await _context.SaveChangesAsync();
		}

		public LunaTaskDetails GetTask(string taskId) 
		{
			if (string.IsNullOrEmpty(taskId)) throw new ArgumentException(nameof(taskId));

			var task = _context.TaskStorage.SingleOrDefault(t => t.Id.Equals(taskId));
			if (task == null) throw new TaskStorageException("Task not found.");
			try
			{
				return new LunaTaskDetails() {
					Id = task.Id,
					Name = task.TaskName,
					AtomsName = task.AtomsFilename,
					ProgramName = task.ProgramFilename
				};
			} catch (FileStorageException ex)
			{
				throw new TaskStorageException("One or more files of task cannot be loaded", ex);
			}
		}

		public IEnumerable<LunaTaskDetails> GetTaskList()
		{
			foreach (var entry in _context.TaskStorage)
			{
				LunaTaskDetails task;
				try {
					task = new LunaTaskDetails()
					{
						Id = entry.Id,
						Name = entry.TaskName,
						AtomsName = entry.AtomsFilename,
						ProgramName = entry.ProgramFilename
					};
				}
				catch (FileStorageException ex) {
					_logger.LogError($"Failed to read task from disk: {ex.Message}");
					continue;
				}
				yield return task;
			}
		}

		public Stream GetAtoms(string taskId)
		{
			if (string.IsNullOrEmpty(taskId)) throw new ArgumentException(nameof(taskId));

			var task = _context.TaskStorage.SingleOrDefault(t => t.Id.Equals(taskId));
			if (task == null) throw new TaskStorageException("Task not found.");
			try
			{
				return _fileStorage.GetFile(Path.Combine(task.TaskName, task.AtomsFilename));
			} catch (FileStorageException ex)
			{
				throw new TaskStorageException("One or more files of task cannot be loaded", ex);
			}
		}

		public Stream GetProgram(string taskId)
		{
			if (string.IsNullOrEmpty(taskId)) throw new ArgumentException(nameof(taskId));

			var task = _context.TaskStorage.SingleOrDefault(t => t.Id.Equals(taskId));
			if (task == null) throw new TaskStorageException("Task not found.");
			try
			{
				return _fileStorage.GetFile(Path.Combine(task.TaskName, task.ProgramFilename));
			} catch (FileStorageException ex)
			{
				throw new TaskStorageException("One or more files of task cannot be loaded", ex);
			}
		}

		public async Task<bool> RemoveTask(string taskId)
		{
			if (string.IsNullOrEmpty(taskId)) throw new ArgumentException(nameof(taskId));

			var task = _context.TaskStorage.SingleOrDefault(t => t.Id.Equals(taskId));
			if (task == null) return false;
			_context.TaskStorage.Remove(task);
			foreach (var filename in new []{ task.AtomsFilename, task.ProgramFilename })
			{
				try
				{
					_fileStorage.RemoveFile(Path.Combine(task.TaskName, task.AtomsFilename));
					_fileStorage.RemoveFile(Path.Combine(task.TaskName, task.ProgramFilename));
				} catch (FileStorageException ex)
				{
					_logger.LogWarning($"An error ocurred when removing task from file storage: {ex.Message}");
				}
			}

			await _context.SaveChangesAsync();
			return true;
		}

        public IEnumerator<LunaTaskDetails> GetEnumerator() => GetTaskList().GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetTaskList().GetEnumerator();
    }


	public class TaskStorageException : Exception
	{
		public TaskStorageException() { }
		public TaskStorageException(string message) : base(message) { }
		public TaskStorageException(string message, Exception inner) : base(message, inner) { }
	}
}
