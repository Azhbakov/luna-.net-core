﻿using Microsoft.EntityFrameworkCore;

namespace LuNA1.Storage
{
    public class LunaDbContext : DbContext
    {
		public DbSet<TaskStorageEntry> TaskStorage { get; set; }

		public LunaDbContext(DbContextOptions<LunaDbContext> options) : base(options)
		{

		}
    }
}
