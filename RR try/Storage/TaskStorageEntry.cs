﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LuNA1.Domain;
using LuNA1.Services;

namespace LuNA1.Storage
{
	public class TaskStorageEntry
	{
		public string Id { get; set; }
		public string TaskName { get; set; }
		public string AtomsFilename { get; set; }
		public string ProgramFilename { get; set; }
	}
}
