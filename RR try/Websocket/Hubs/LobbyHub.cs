using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LuNA1.Domain;
using LuNA1.Services;
using LuNA1.Websocket.Hubs.Messages;
using Microsoft.AspNetCore.SignalR;

namespace LuNA1.Websocket.Hubs
{
	public class LobbyHub : Hub
	{
		private readonly LobbyService _lobbyService;

		public LobbyHub(LobbyService lobbyService)
		{
			_lobbyService = lobbyService;
		}

		public async override Task OnConnectedAsync() 
		{
			await base.OnConnectedAsync();
		}

		public async override Task OnDisconnectedAsync(Exception ex) 
		{
			var id = Context.ConnectionId;
			var lobby = _lobbyService.GetLobbies().SingleOrDefault(l => l.Topology.Peers.Any(p => p.Id.Equals(id)));
			if (lobby == null) return;

			lobby.RemovePeer(id);
			await Clients.Others.SendAsync(OnPeerDisconnectedMethod.MethodName, new OnPeerDisconnectedMethod 
			{ 
				DisconnectedPeerId = id, 
				Lobby = new ViewModels.Lobby(lobby) 
			});
		}

		public async Task<dynamic> JoinLobby(string lobbyId, string peerName)
		{
			var id = Context.ConnectionId;
			var lobby = _lobbyService.GetLobby(lobbyId);
			
			lobby.AddPeer(id, peerName);

			await Clients.Others.SendAsync(OnPeerConnectedMethod.MethodName, new OnPeerConnectedMethod
			{ 
				ConnectedPeerId = id, 
				Lobby = new ViewModels.Lobby(lobby) 
			});

			return new { Id = id, Lobby = new ViewModels.Lobby(lobby) };
		}

		public async Task<dynamic> SetPeerReady(int changeIndex, bool isReady) 
		{
			var id = Context.ConnectionId;
			var lobby = _lobbyService.GetLobbies().SingleOrDefault(l => l.Topology.Peers.Any(p => p.Id.Equals(id)));
			if (lobby == null) return false;

			if (!lobby.TrySetPeerReady(id, changeIndex, isReady)) return new { IsCorrectChangeIndex = false };
			
			await Clients.Others.SendAsync(LobbyUpdateMethod.MethodName, new LobbyUpdateMethod 
			{ 
				Lobby = new ViewModels.Lobby(lobby) 
			});

			return new { IsCorrectChangeIndex = true, Lobby = new ViewModels.Lobby(lobby) };
		}

		public async Task<dynamic> LobbyStart(dynamic settings) 
		{
			var id = Context.ConnectionId;
			var lobby = _lobbyService.GetLobbies().SingleOrDefault(l => l.Topology.Peers.Any(p => p.Id.Equals(id)));
			if (lobby == null) return false;

			if (!lobby.TryStart()) return new { Started = false };
			
			await Clients.Others.SendAsync(LobbyStartedMethod.MethodName, new LobbyStartedMethod
			{ 
				Lobby = new ViewModels.Lobby(lobby), 
				Settings = settings 
			});

			return new { Started = true, Lobby = new ViewModels.Lobby(lobby) };
		}

		public async Task Retranslate(string targetPeerId, object message) 
		{
			if (string.IsNullOrEmpty(targetPeerId)) throw new ArgumentException(nameof(targetPeerId));
			await Clients.Client(targetPeerId).SendAsync(ReceiveMessageMethod.MethodName, new ReceiveMessageMethod 
			{ 
				SourcePeerId = Context.ConnectionId,
				Message = message 
			});
		}
	}
}