namespace LuNA1.Websocket.Hubs.Messages
{
    public class OnPeerConnectedMethod 
    {
        public static string MethodName = "OnPeerConnected";

        public string ConnectedPeerId { get; set; }
        public ViewModels.Lobby Lobby { get; set; }
    }

    public class OnPeerDisconnectedMethod 
    {
        public static string MethodName = "OnPeerDisconnected";

        public string DisconnectedPeerId { get; set; }
        public ViewModels.Lobby Lobby { get; set; }
    }

    public class LobbyUpdateMethod 
    {
        public static string MethodName = "LobbyUpdate";

        public ViewModels.Lobby Lobby { get; set; }
    }

    public class LobbyStartedMethod 
    {
        public static string MethodName = "LobbyStarted";

        public ViewModels.Lobby Lobby { get; set; }
        public object Settings { get; set; }
    }

    public class ReceiveMessageMethod
    {
        public static string MethodName = "ReceiveMessage";
        
        public string SourcePeerId { get; set; }
        public object Message { get; set; }
    }
}