using System.Collections.Generic;
using System.Linq;
using LuNA1.Domain;
using static LuNA1.Domain.Lobby;

namespace LuNA1.Websocket.ViewModels
{
	public class Lobby 
	{
		public string Id;
		public int ChangeIndex;
		public string Name;
		public int? MaxUsersCount = null;
		public int UserCount;
		public string LeaderId;
		public LunaTask Task;
		public Topology Topology;
		public string Status;

		public Lobby(LuNA1.Domain.Lobby lobby) 
		{
			Id = lobby.Id;
			ChangeIndex = lobby.ChangeIndex;
			Name = lobby.Name;
			MaxUsersCount = lobby.MaxUsersCount;
			UserCount = lobby.UserCount;
			LeaderId = lobby.LeaderId;
			Task = new LunaTask(lobby.Task);
			Topology = new Topology(lobby.Topology, lobby.PeerReadyState);
			Status = lobby.Status.ToString();
		}
	}

	public class LunaTask
	{
		public string Id;
		public string Name;
		public string AtomsName;
		public string ProgramName;

		public LunaTask(LuNA1.Domain.LunaTaskDetails task) 
		{
			Id = task.Id;
			Name = task.Name;
			AtomsName = task.AtomsName;
			ProgramName = task.ProgramName;
		}
	}

	public class Topology
	{
		public string Key;

		public IEnumerable<Peer> Peers;

		public IEnumerable<IEdge> Edges;

		public Topology(ITopology topology, IDictionary<string, bool> peerReadyState)
		{
			Key = topology.Key;
			Edges = topology.Edges;
			Peers = topology.Peers.Select(p => new Peer(p, peerReadyState[p.Id]));
		}
	}

	public class Peer
	{
		public string Id;

		public string Name;
		public bool IsReady;

		public Peer(IPeer peer, bool isReady) 
		{
			Id = peer.Id;
			Name = peer.Name;
			IsReady = isReady;
		}
	}
}